import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


 class FastTest {
    public FastTest() { //no final
    }


    public static void main(String args[]) throws Exception {
//        new FastTest();


//      name of variables symbols admitted
        int a2_w$ = 0;

//        Period dates

//        try {
//            System.out.println();
//        } catch (IOException e) { // illegal call
//
//        }


    }
}

interface Pow {
    static void wow() {
        System.out.println("In Pow.wow");
    }
}

abstract class Wow {
    static void wow() {
        System.out.println("In Wow.wow");
    }
}

class Powwow extends Wow implements Pow {
    public static void main(String[] args) {
        Powwow f = new Powwow();
        f.wow(); //Wow.wow
    }
}

class A {
    public int i = 10;
    private int j = 20;
    public int k = 40;

    private final void ceva() {
    }

    ;
}

class B extends A {
    private int i = 30; //1
    public int k = 40;

    final void ceva() {
    }
}

class C extends B {
}

class TestClass {
    public static void main(String args[]) {
        C c = new C();
//        System.out.println(c.i); //2
//        System.out.println(c.j); //3
        System.out.println(c.k); // from B
    }
}



interface House {
    public default String getAddress() {
        return "101 Main Str";
    }
}

interface House2 {
    public String getAddress();
}

interface Bungalow extends House {
    public default String getAddress() {
        return "101 Smart Str";
    }
}

interface Bungalow2 extends House {
}

class MyHouse implements Bungalow, House {
}
//class MyHouse2 implements Bungalow2, House2 { // have to override
//}

class TestClass202 {
    public static void main(String[] args) {
        House ci = new MyHouse();
        System.out.println(ci.getAddress()); // 101 Smart Str
    }
}
class ExAllowsNeglectReturn {

    public float parseFloat(String s) {
        float f = 0.0f;
        try {
            f = Float.valueOf(s).floatValue();
            return f;
        } catch (NumberFormatException nfe) {
            f = Float.NaN;
            return f;
        } finally {
            f = 10.0f;
            return f;
        }

    }
}





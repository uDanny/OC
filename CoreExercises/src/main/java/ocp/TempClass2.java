package ocp;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TempClass2 implements Serializable {
    //write all averages together, review it
    //average.getAsDouble
    //review peek
    //review what is recoursiv Action and recoursive Task on forkjoin
    //question 41
    //CopyOnWriteArrayList
    //ToDoubleFunction??
    //collectors, group by 62 q
    //super T - no
    //mannual commit
    //review nio write, read
    //copyOnWrite.iterator remove, add, ...
    //review generics
    //DataOutputStream
    //todo: collectors
    public static void main(String[] args) throws IOException {
        List<String> vals = Arrays.asList("a", "b");
        BinaryOperator<String> stringBinaryOperator = (a, b) -> a.concat(b);
        String join = vals.parallelStream().reduce("_", stringBinaryOperator);
        System.out.println(join); //_a_b or _ab

        Optional<String> reduce = vals.parallelStream().reduce(stringBinaryOperator);

        String paralel = Stream.of("a", "b")
                .parallel().reduce("_", stringBinaryOperator, (x, y) -> x + "!" + y);


        String nonParalel = Stream.of("a", "b")
                .reduce("_", stringBinaryOperator, (x, y) -> x + "!" + y);

        System.out.println(paralel);
        System.out.println(nonParalel);


        //

        IntStream stream2 = IntStream.rangeClosed(1, 10); //1-10
        IntStream stream222 = IntStream.range(1, 10); //1-9
        stream2.forEach(System.out::print);
        stream222.forEach(System.out::print);

        Stream<Integer> stream = Stream.of(1, 2, 3);
        Stream<? extends Number> stream1 = Stream.of(1, 2.1, 3);
        Stream<Double> stream22 = Stream.of(1.0, 2.1, 3.1);

        OptionalDouble average = stream.mapToInt(x -> x).average();
        OptionalDouble average2 = Stream.of(1, 2, 3).mapToLong(x -> x).average();
        OptionalDouble average3 = Stream.of(1, 2.0, 3).mapToDouble(Number::doubleValue).average();
        System.out.println(average);
        System.out.println(average.getAsDouble());
        average.ifPresent(x -> System.out.println(x));


        Stream<String> ohMy2 = Stream.of("lions", "tigers", "bears");
        Double result2 = ohMy2.collect(Collectors.averagingInt(String::length));
//        Double result3 = ohMy2.collect(Collectors.averagingDouble(String::length)); //exception, can't convert int to double
        Double result4 = Stream.empty().collect(Collectors.averagingInt(x -> 2));
        Double result5 = Stream.of(1.0, 2.1, 3.1).collect(Collectors.averagingDouble(x -> x));

        Integer collect = Stream.of(1, 2).collect(Collectors.summingInt(x -> x));
        //

        //

        System.out.println(result2); //5.33
//        System.out.println(result3);
        System.out.println(result4); //0.0
        System.out.println(result5); //2.06

        //


        Path p1 = Paths.get("c:\\temp\\test.txt");
        Path p2 = Paths.get("report.pdf");
        System.out.println(p1.resolve(p2)); // c:\temp\test.txt/report.pdf
        System.out.println(p2.resolve(p1)); // report.pdf/c:\temp\test.txt


        //

        List<String> list = Arrays.asList("a", "b");
        Collections.sort(list, String::compareTo);
        Collections.sort(list, TempClass2::staticMethod);
        Collections.sort(list, new TempClass2()::nontStaticMethod);

        //
        try {


            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("utf8.txt"), Charset.forName("UTF-8").newEncoder());

            new FileOutputStream("a").write(1); //is overriding if file exists
            new FileOutputStream("a").write(new byte[2]); //not thrown exception even if not buffered stream
            new FileInputStream("a").read(new byte[2], 0, 1); //not thrown exception even if not buffered stream


            try (InputStream is = new FileInputStream("a"); OutputStream os = new FileOutputStream("b")) {
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                    System.out.println("Read and written bytes " + bytesRead);
                }
            }
        } catch (Exception e) {

        }
        //


        Connection c = null;
        try {
            c = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("select * from CUSTOMER_ORDER");
            stmt.close();
            while (rs.next()) { //throws because stmt close is closing rs as well
                System.out.println(rs.getString("AMOUNT")); // even if AMOUNT is float, it is converted into string
            }
            c.close();
            c.setAutoCommit(false); //connection is commiting, not statement
            c.commit(); //connection is commiting, not statement
            c.rollback(); //or with parameter of savepoint


        } catch (Exception e) {
            e.printStackTrace();
        }

        //


        class NonSortedObj {
        }
        Stream.of(new NonSortedObj()).sorted().forEach(System.out::println);//will throw an exception, because not implements comparable

        //


        Object v1 = IntStream.rangeClosed(10, 15).boxed().filter(x -> x > 12).parallel().findAny(); //13, 14, 15
        Object v2 = IntStream.rangeClosed(10, 15).boxed().filter(x -> x > 12).sequential().findAny(); //13, 14, 15 because findAny
        Object v3 = IntStream.rangeClosed(10, 15).boxed().filter(x -> x > 12).sequential().findFirst(); //13
        System.out.println(v1 + ":" + v2);

        //
        try {
            PreparedStatement ps = c.prepareStatement("INSERT INTO STUDENT VALUES (?, ?)"); //This is created only once  Once created, the PreparedStatement is compiled automatically.
            ps.setInt(1, 111);
            ps.setString(2, "Bob");
            ps.executeUpdate();

            //Now change the parameter values and execute again.   
            ps.setInt(1, 112);
            ps.setString(2, "Cathy");
            ps.executeUpdate();


            //computeMatrixForSales is a stored procedure that has already been created in the database.   
            CallableStatement callableStatement = c.prepareCall("{call computeMatrixForSales(?)}");
            callableStatement.setInt(1, 1000);
            callableStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //

        Integer integer = Stream.of(1, 2).max(Integer::compareTo).get(); //stream.there is no Stream.sum
        int asInt = Stream.of(1, 2).mapToInt(x -> x).max().getAsInt();
        int sum = Stream.of(1, 2).mapToInt(x -> x).sum(); //0 by default

        //

//        Stream.of(1, 2).mapToInt(Function.identity()); //not working because it is returning primitve wrapper
        Stream.of(1, 2).mapToInt(Number::intValue);

        Set<String> s = Collections.synchronizedSet(new HashSet());

        ///

        List<CharSequence> charSequences = null;
        List<? super CharSequence> objects = doIt(charSequences);

        List<String> charSequences2 = null;
        List<? super String> objects2 = doIt(charSequences);
        List<? extends Object> objects4 = null;

        List<? super String> objects3 = objects2;
        List<? extends Object> objects5 = objects4;


        CopyOnWriteArrayList<String> copy = new CopyOnWriteArrayList<>();
        copy.add("A");
//        copy.iterator().remove(); //UnsupportedOperationException no supported through operator

        copy.remove(0); //allowed in concurrent env

        ////

        try {
            Console console = System.console(); //can be null

            char[] chars = console.readPassword();
            String s1 = console.readLine();
            Reader reader = console.reader();
            PrintWriter writer = console.writer();
            Console format = console.format("");

        } catch (Exception e) {
        }

        //

        LocalDateTime ldt = LocalDateTime.of(2017, 12, 02, 6, 0, 0);
        ZonedDateTime nyZdt = ldt.atZone(ZoneId.of("America/New_York"));
        ZonedDateTime laZdt = ldt.atZone(ZoneId.of("America/Los_Angeles"));
        Duration d = Duration.between(nyZdt, laZdt);
        System.out.println(d);

        //
try{
    DataOutputStream outputStream = new DataOutputStream(null);
    outputStream.write(0);
    outputStream.writeInt(1);

    DataInputStream dataInputStream = new DataInputStream(null);
    int read = dataInputStream.read();
    char readc = dataInputStream.readChar();

}catch (Exception e){}

        //



//        Stream<String> ohMy5 = Stream.of("aa", "a", "ss");
//        Map<Integer, String> map5 = ohMy5.collect(Collectors.toMap( String::length, k -> k)); //error, duplicate key

        Set<String> set = Stream.of("aa", "a", "a").collect(Collectors.toSet()); //just overriding it
    }

    List<String> someMethod() {
        return new ArrayList(); //allowed
    }

    static int staticMethod(String a, String b) {
        return a.compareTo(b);
    }

    int nontStaticMethod(String a, String b) {
        return a.compareTo(b);
    }

    //    customize the behavior of class serialization,
    private void writeObject(ObjectOutputStream out) throws IOException {
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    }


    public static <E extends CharSequence> List<? super E> doIt(List<E> nums) {
        return null;
    }

}

class TestClass implements Runnable {
    volatile int x = 0, y = 0;

    public void run() {
        while (x < 100) {
            x++;
            y++;
            System.out.println(" x = " + x + " , y = " + y);
        }
    }

    public static void main(String[] args) {
        TestClass tc = new TestClass();
        new Thread(tc).start();
        new Thread(tc).start();
    }
}

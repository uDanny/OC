package ocp;

public class FinalTemp2 {
}
interface I1{
    int i = 1;
    static int getI(){
        return i;
    }
    default int getDefault(){
        return i;
    }
}
interface I2{
    int i = 1;
    static int getI(){
        return i;
    }
    default int getDefault(){
        return i;
    }
}
interface I1I2 extends I1, I2{
    default void method(){
        System.out.println(I1.i);

    }

    static int getI(){
        return I2.getI();
    }

    @Override
    default int getDefault() {
        return I1.super.getDefault();
    }
}

class OO implements I1{
    public static void main(String[] args) {
        System.out.println(i);
        I1.getI();


    }
}

package ocp.io;

import java.io.*;

public class IoArchitecture {
    public static void main(String[] args) throws IOException {
        InputStream in = new FileInputStream("stringName"); //is2.read() -> int, casted to char (-1 int if end of file)
        in = new FileInputStream(new File("stringName"));

//        in = new FilterInputStream(in); //protected constructor
        in = new BufferedInputStream(in);//read byte[]
        boolean trueBool = in instanceof FilterInputStream;
        in = new DataInputStream(in);//readInt()
        trueBool = in instanceof FilterInputStream;
        in = new ObjectInputStream(in); //readObject();

        //
        OutputStream out = new FileOutputStream("stringName"); //write int (char)
        out = new FileOutputStream(new File("stringName")); out.write(null); out.write(1);

        out = new FilterOutputStream(out);
        out = new BufferedOutputStream(out); //write byte[]
        trueBool = out instanceof FilterOutputStream;
        out = new DataOutputStream(out); //writeInt
        trueBool = out instanceof FilterOutputStream;
        out = new ObjectOutputStream(out); //writeObject
        out = new PrintStream("stringName");  new PrintStream("stringName").printf("a");
        trueBool = out instanceof FilterOutputStream;
        out = new PrintStream(new File("stringName"));
        out = new PrintStream(out); //all prints, and append char sequence, formats

        //

        Reader reader = new InputStreamReader(in);  while(reader.ready()) {int i  =  reader.read();};
        reader = new BufferedReader(reader); //readLine, supports mark
        reader = new FileReader("stringName"); int stringName = new FileReader("stringName").read();
        reader = new FileReader(new File("stringName"));
        trueBool = reader instanceof InputStreamReader;

        //

        Writer writer = new OutputStreamWriter(out);
        writer = new BufferedWriter(writer); //newLine and write(String)
        writer = new FileWriter("stringName"); //newLine and write(String)
        writer = new FileWriter(new File("stringName"));
        trueBool = writer instanceof OutputStreamWriter;
        writer = new PrintWriter("stringName"); new PrintWriter("stringName").println("a");
        writer = new PrintWriter(new File("stringName"));
        writer = new PrintWriter(out);
        writer = new PrintWriter(writer); //.append(char sequence, format

    }
}

package ocp.io;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class IoPractice {
    public static void main(String[] args) {
        System.out.println(System.getProperty("file.separator"));
        System.out.println(java.io.File.separator);


        File file = new File("/Users/danielursachi/Documents/javaFiles/zoo.txt");
        System.out.println(file.exists());

        File parent = new File("/Users/danielursachi");
        File child = new File(parent, "Documents/javaFiles/zoo.txt"); //parent could be null

        file.exists(); //Returns true if the file or directory exists.
        file.getName(); //Returns the name of the file or directory denoted by this path.
        file.getAbsolutePath(); //Returns the absolute pathname string of this path.
        file.isDirectory(); //Returns true if the file denoted by this path is a directory.
        file.isFile(); //Returns true if the file denoted by this path is a file.
        file.length(); //Returns the number of bytes in the file. For performance reasons, the file system may allocate more bytes on disk than the file actually uses.
        file.lastModified(); //Returns the number of milliseconds since the epoch when the file was last modified.
        file.delete(); //Deletes the file or directory. If this pathname denotes a directory, then the directory must be empty in order to be deleted.
        file.renameTo(null);//Renames the file denoted by this path.
        file.mkdir();// Creates the directory named by this path.
        file.mkdirs(); //Creates the directory named by this path including any nonexistent parent directories.
        file.getParent();//Returns the abstract pathname of this abstract pathname’s parent or null if this pathname does not name a parent directory.
        file.listFiles();//Returns a File[] array denoting the files in the directory.

        //p.469 ex

    }
}

class ReadFileInformation {
    public static void main(String[] args) {
        File file = new File("/Users/danielursachi/Documents/javaFiles/zoo2.txt");
        file.mkdirs();// Creates the directory named by this path.

        System.out.println("File Exists: " + file.exists());
        if (file.exists()) {
            System.out.println("Absolute Path: " + file.getAbsolutePath());
            System.out.println("Is Directory: " + file.isDirectory());
            System.out.println("Parent Path: " + file.getParent());
            if (file.isFile()) {
                System.out.println("File size: " + file.length());
                System.out.println("File LastModified: " + file.lastModified());
            } else {
                for (File subfile : file.listFiles()) {
                    System.out.println("\t" + subfile.getName());
                }
            }
        }
    }
}

class MarkExample {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream(new File("/Users/danielursachi/Documents/javaFiles/untitled"));
        System.out.print((char) is.read());
        if (is.markSupported()) {
            is.mark(100);
            System.out.print((char) is.read());
            System.out.print((char) is.read());
            is.reset();
        }
        System.out.print((char) is.read());
        System.out.print((char) is.read());
        System.out.print((char) is.read());


        InputStream is2 = new FileInputStream(new File("/Users/danielursachi/Documents/javaFiles/untitled"));
        System.out.print((char) is2.read());
        is2.skip(2);
        is2.read();
        System.out.print((char) is2.read());
        System.out.print((char) is2.read());

    }

}

class CopyFileSample {
    public static void copy(File source, File destination) throws IOException {
        try (InputStream in = new FileInputStream(source);
             OutputStream out = new FileOutputStream(destination)) {
            int b;
            while ((b = in.read()) != -1) {
                out.write(b);
            }
        }
    }

    public static void copyStream(File source, File destination) throws IOException {
        try (InputStream in = new BufferedInputStream(new FileInputStream(source));
             OutputStream out = new BufferedOutputStream(
                     new FileOutputStream(destination))) {
            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
//                out.flush(); //is optional
            }
        }
    }

    public static void main(String[] args) throws IOException {
        File source = new File("/Users/danielursachi/Documents/javaFiles/untitled");
        File destination = new File("/Users/danielursachi/Documents/javaFiles/untitled2.txt");
        copy(source, destination);
        copyStream(source, destination);
    }
}


class CopyTextFileSample {
    public static List<String> readFile(File source) throws IOException {
        List<String> data = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(source))) {
            String s;
            while ((s = reader.readLine()) != null) {
                data.add(s);
            }
        }
        return data;
    }

    public static void writeFile(List<String> data, File destination) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(destination))) {
            for (String s : data) {
                writer.write(s);
                writer.newLine();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        File source = new File("/Users/danielursachi/Documents/javaFiles/untitled");
        File destination = new File("untitledNew.txt");
        List<String> data = readFile(source);
        for (String record : data) {
            System.out.println(record);
        }
        writeFile(data, destination);


        Charset usAsciiCharset = Charset.forName("US-ASCII");
        Charset utf8Charset = Charset.forName("UTF-8");
        Charset utf16Charset = Charset.forName("UTF-16");

    }
}


class Animal implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private int age;
    private char type;

    public Animal() {
    }

    public Animal(String name, int age, char type) { //constructor, initialize blocks and setters, getters are ignored by serialization, even if only private constructor
        this.name = name;
        this.age = age;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public char getType() {
        return type;
    }

    public String toString() {
        return "Animal [name=" + name + ", age=" + age + ", type=" + type + "]";
    }
}


class ObjectStreamSample {
    public static List<Animal> getAnimals(File dataFile) throws IOException, ClassNotFoundException {
        List<Animal> animals = new ArrayList<>();
        try (ObjectInputStream in = new ObjectInputStream(
                new BufferedInputStream(new FileInputStream(dataFile)))) {
            while (true) {
                Object object = in.readObject();
                if (object instanceof Animal)
                    animals.add((Animal) object);
            }
        } catch (EOFException e) { // File end reached
        }
        return animals;
    }

    public static void createAnimalsFile(List<Animal> animals, File dataFile) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(dataFile)))) {
            for (Animal animal : animals)
                out.writeObject(animal);
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("Tommy Tiger", 5, 'T'));
        animals.add(new Animal("Peter Penguin", 8, 'P'));
        File dataFile = new File("animal.data");
        createAnimalsFile(animals, dataFile);
        System.out.println(getAnimals(dataFile));
    }
}

package ocp.io;

import java.io.*;

public class PrintsPractice {
    public static void main(String[] args) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter("zoo.txt")) { //try or use out.flash()
            out.print(5); // PrintWriter method
            out.write(String.valueOf(5)); // Writer method
            out.print(2.0); // PrintWriter method
            out.write(String.valueOf(2.0)); // Writer method
            Animal animal = new Animal();
            out.print(animal); // PrintWriter method
            out.write(animal == null ? "null" : animal.toString()); // Writer method

            out.println(System.getProperty("line.separator") + "print with manual ln ");
            out.println("println");
            out.format("%s", "s"); //no diff with printf
            out.printf("%s", "s1"); //no diff with format

        }

        try (PrintWriter out = new PrintWriter(
                new BufferedWriter(new FileWriter(new File("zoo.log"))))) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

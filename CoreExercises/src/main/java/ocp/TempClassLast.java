package ocp;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TempClassLast {
    //todo: when is created
    //todo: play with Files.renameTo
    //todo: review interfaces static, and defaults inheritance, inclusive multiple inheritance
    //todo: review from oca: casting order, strings api, oop
    //todo: q71
    public static void main(String[] args) {


        LocalTime now = LocalTime.now();
        LocalTime gameStart = LocalTime.of(10, 15);
        long timeConsumed = 0;
        long timeToStart = 0;
        if (now.isAfter(gameStart)) {
            timeConsumed = gameStart.until(now, ChronoUnit.HOURS);
        } else {
            timeToStart = now.until(gameStart, ChronoUnit.HOURS);
        }
        System.out.println(timeToStart + " " + timeConsumed);

        //


        Path path = Paths.get("OC/a");
        Path path1 = Paths.get("OC/b");
        Path path2 = Paths.get("/Users/danielursachi/IdeaProjects/Ex/Java/OC/a");
        Path path3 = Paths.get("/Users/danielursachi/IdeaProjects/Ex/Java/OC/b");

        Path relativize = path.relativize(path1); // ../b
        Path relativize1 = path2.relativize(path3); // ../c

//        path2.relativize(path); //illegal arg exception
//        path1.relativize(path2); //illegal arg exception


        // it starts with \\ -> returns arg (second param)
        Path resolve = path.resolve(path1); //OC/A/OC/B
        Path resolve1 = path1.resolve(path); //OC/B/OC/A
        Path resolve2 = path2.resolve(path3); // second param /Users/danielursachi/IdeaProjects/Ex/Java/OC/b
        Path resolve3 = path3.resolve(path2); //   second param /Users/danielursachi/IdeaProjects/Ex/Java/OC/a

        Path resolve4 = path.resolve(path2); // returns second param /Users/danielursachi/IdeaProjects/Ex/Java/OC/a
        Path resolve5 = path2.resolve(path); // /Users/danielursachi/IdeaProjects/Ex/Java/OC/a/OC/a

        //


        Path p1 = Paths.get("photos/../beaches/./calangute/a.txt");
        Path p2 = p1.normalize(); //beaches/calangute/a.txt
        Path p3 = p1.relativize(p2);
        Path p4 = p2.relativize(p1);
        System.out.println(p1.getNameCount() + " " + p2.getNameCount() + " " + p3.getNameCount() + " " + p4.getNameCount());

        //
        long count = Stream.empty().count(); //returns long
        long count2 = Stream.of("a", "b", "c").filter((str) -> str.compareTo("c") < 0).count(); //all elements before "c" in a sorted stream

        int i = ThreadLocalRandom.current().nextInt();
        int i2 = ThreadLocalRandom.current().nextInt(1, 4); //inclusive, exclusive


        Stream<String> ss = Stream.of("a", "b", "c");
        String str = ss.collect(Collectors.joining(",", "-", "+"));
//        str = ss.collect(Collectors.joining());
//        str = ss.collect(Collectors.joining(","));
        System.out.println(str); //-a,b,c+
//
        boolean trueBool = new Thread() instanceof Runnable;
//
        HashMap<?, List<String>> box = new HashMap<Object, List<String>>();
        HashMap<? extends List, List<String>> box2 = new HashMap<ArrayList, List<String>>();

//        System.out.println(0%0); // / by zero ArithmeticException


        List<String> names = Arrays.asList("charles", "chuk", "cynthia", "cho", "cici");
        long x = names.stream().filter(name -> name.length() > 4).collect(Collectors.counting());
//        double y = IntStream.rangeClosed(0, 3).collect(Collectors.summingInt(x->x));
        System.out.println(x);

        //

        System.out.println(Stream.of(1, 2, 3).collect(Collectors.mapping(xx -> xx, Collectors.summarizingInt(xx -> xx))).getSum());
        IntSummaryStatistics collect1 = Stream.of(1, 2, 3).collect(Collectors.summarizingInt(xx -> xx)); //same as upper
        System.out.println(collect1.getSum());
        System.out.println(collect1.getAverage());
        Double collect = Stream.of(1, 2, 3).collect(Collectors.averagingInt(xx -> xx));
        System.out.println(collect);

        //

        List old = null;
        List<?> wild = null;
        List<? extends List> ex = null;
        List<? super List> su = null;
        List<List> gen = null;

        List<ArrayList> child = null;
        List<Collection> parent = null;

        old = old;
        old = wild;
        old = ex;
        old = su;
        old = gen;
        old = child;
        old = parent;

        wild = old;
        wild = wild;
        wild = ex;
        wild = su;
        wild = gen;
        wild = child;
        wild = parent;

        ex = old;
        ex = ex;
        ex = gen;
        ex = child;


        su = old;
        su = su;
        su = gen;
        su = parent;

        gen = old;
        gen = gen;

        //


        gen.addAll(child); //valid

        List lg = null;
        ArrayList lchild = null;
        Collection lparent = null;
        su.add(lg); //valid
        su.add(lchild); //valid
//        su.add(lparent); // not valid


        //


        String[] sa = {"charlie", "bob", "andy", "dave"};
        Collections.sort(Arrays.asList(sa), null);
        System.out.println(sa[0]); // andy , if null comparator, it uses default


        //


        someMethodWithPrimitiveStreams();


        //


//        1. The time zone of America / New York is normally 5 hours behind UTC. 2. Day light saving is ON during June in
//        New York.(This means, clocks in New York are 1 hour ahead than when day light saving is OFF.)What will
//        the following code print when compiled and run?
        LocalDateTime ldt = LocalDateTime.of(2017, 06, 02, 6, 0, 0);  //Jun 2nd, 6AM.
        ZoneOffset nyOffset = ZoneOffset.ofHoursMinutes(-5, 0);
        ZoneId nyZone = ZoneId.of("America/New_York");
        OffsetDateTime nyOdt = ldt.atOffset(nyOffset);
        ZonedDateTime nyZdt = ldt.atZone(nyZone);
        Duration d = Duration.between(nyOdt, nyZdt);
        System.out.println(d); //P-1H
    }

    private static void someMethodWithPrimitiveStreams() {
        IntStream is1 = IntStream.of(1, 3, 5);  //1
        OptionalDouble x = is1.filter(i -> i % 2 == 0).average(); //returns optional as well as max, min, finds
        System.out.println(x); //3
        IntStream is2 = IntStream.of(2, 4, 6); //4
        int y = is2.filter(i -> i % 2 != 0).sum(); //returns 0 if empty
        System.out.println(y); //6


    }

    public List<? extends Shape> m4(List<? extends Shape> strList) {
        List<Shape> list = new ArrayList<>();
        Shape e = null;
        list.add(e);
        list.addAll(strList);
        return list;
    }

    static List old = null;
    static List<?> wild = null;
    static List<? extends List> ex = null;
    static List<? super List> su = null;
    static List<List> gen = null;
    static List<ArrayList> child = null;
    static List<Collection> parent = null;


    static List old() {
//        return old;
//        return wild;
//        return ex;
//        return su;
//        return gen;
//        return child;
        return parent;
    }

    static List<?> wild() {
//        return old;
//        return wild;
//        return ex;
//        return su;
//        return gen;
//        return child;
        return parent;
    }

    static List<? extends List> ex() {
//        return old;
//        return ex;
//        return gen;
        return child;
    }

    static List<? super List> su() {
//        return old;
//        return su;
//        return gen;
        return parent;
    }

    static List<List> col() {
//        return old;
        return gen;
    }

}


interface Eatable {
    int types = 10;

    static int getTypes() {
        return types;
    }

    default int get() {
        return types;
    }
}

class Food implements Eatable {
    public static int types = 20;

    static int getTypes() {
        return types;
    }

    @Override
    public int get() { //autoresolved
        return types;
    }
}

class Fruit extends Food implements Eatable { //LINE1
    public static void main(String[] args) {
//        types = 30; // ambiguous call
//        System.out.println(types); // ambiguous call
        System.out.println(Food.types); //LINE 3
        System.out.println(Eatable.types); //LINE 3

        int types = getTypes(); //compiles ok
        int types1 = Eatable.getTypes(); //compiles ok

    }

    void mainInstance() {
        int i = get();
    }
}

class Fruit2 extends Food { //LINE1
    public static void main(String[] args) {
        types = 30; // not an ambiguous call
        System.out.println(types); // not an ambiguous call
    }
}


class Calculator {
    public static void main(String[] args) {
        double principle = 100;
        int interestrate = 5;
//        double amount = compute(principle, x -> x * interestrate);
    }

    //return type is the problem
    //    public static double compute(double base, Function<Double, Integer> func) {
//        return func.apply(base);
//    }
    //return type is the problem
//    public static double compute(double base, Function<Integer, Double> func) {
//        return func.apply((int) base);
//    }

//    public static Double getDouble() {
//        return 1;
//    }

}

class Boo {
    int boo = 10;

    public Boo(int k) {
        System.out.println("In Boo k = " + k);
        boo = k;
    }
}

class BooBoo extends Boo {
    public BooBoo() { //required no-args constructor for highest level of non-serializable class
        super(0);
    }

    public BooBoo(int k) {
        super(k);
        System.out.println("In BooBoo k = " + k);
    }
}

class Moo extends BooBoo implements Serializable {
    int moo = 10;

    public Moo() {
        super(5);
        System.out.println("In Moo");
    }
}

class SerializeTest {
    public static void main(String[] args) throws Exception {
        Moo moo = new Moo();
        FileOutputStream fos = new FileOutputStream("moo1.ser");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(moo);
        os.close();
        FileInputStream fis = new FileInputStream("moo1.ser");
        ObjectInputStream is = new ObjectInputStream(fis);
        moo = (Moo) is.readObject();
        is.close();
    }
}


class MySecureClass {
    public synchronized void doALotOfStuff() {
        try {
            Thread.sleep(10000); //locks the full object, so it will not be able to perform any other methods
        } catch (Exception e) {
        }
    }

    public synchronized void doSmallStuff() {
        System.out.println("done");
    }

    public static void main(String[] args) {
        MySecureClass m = new MySecureClass();
        new Thread() {
            @Override
            public void run() {
                m.doALotOfStuff();
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                m.doSmallStuff();
            }
        }.start();

        //if one of the methods would been not synchronized, it done was printed immediately, otherwise it is waiting
    }
}


class MySecureClass2 {
    public synchronized void doALotOfStuff() {
        for (int i = 0; i < 990000; i++) {
            print("doALotOfStuff");

        }
    }

    String s = new String();

    public synchronized void doSmallStuff() {

        print("doSmallStuff ----- "); //displayed at the end, after first is finished

//        for (int i = 0; i < 100; i++) {

//        synchronized (s){
//            print("doSmallStuff ----- "); //it is displayed at the start if method is not synchronized, and sync block is by a field
//        }
//        }


    }

    void print(String s) {
        System.out.println(s);
    }

    public static void main(String[] args) {
        MySecureClass2 m = new MySecureClass2();
        Thread thread = new Thread(() -> m.doALotOfStuff());

        Thread thread1 = new Thread(() -> m.doSmallStuff());
        thread.start();
        thread1.start();

        //if one of the methods would been not synchronized, it done was printed immediately, otherwise it is waiting
    }
}

class ScrollDb {

    private void testRS() throws Exception {
        final String query = "select id, name, zip from customer where zip='11111'";
        Connection connection = DriverManager.getConnection("a");
        Statement stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        Statement stmt2 = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs1 = stmt1.executeQuery(query);
        rs1.next();
        ResultSet rs2 = stmt2.executeQuery(query);
        rs2.next();
        rs2.updateString("zip", "22222");
        rs2.updateRow(); //1
        System.out.println("Zip : " + rs1.getString("zip")); //2
    }


}


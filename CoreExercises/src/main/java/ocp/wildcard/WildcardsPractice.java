package ocp.wildcard;

import com.sun.org.apache.xerces.internal.xs.StringList;

import java.util.ArrayList;
import java.util.List;

public class WildcardsPractice {
    public static void printList(List<Object> list) {
        for (Object x : list) System.out.println(x);
    }

    public static List<?> bound(List<?> list) {
        for (Object x : list)
            System.out.println(x);
        Object o = list.get(0);
        Object o2 = list.remove(0);
//        list.add(new Object()); // no compile


//        return new ArrayList<>();
        return new ArrayList<String>();
    }

    public static List<? extends B> upperBound(List<? extends B> list) {
        for (B x : list) System.out.println(x);
        list.remove(new B()); //because it requires an obj parameter
        list.remove(null); //because it requires an obj parameter
        B b = list.get(0);
        A a = list.get(0);
//        list.add(new B()); //no compile, because can't access methods with ? parameters type

//        return new ArrayList();
//        return new ArrayList<B>();
        return new ArrayList<C>(); //no <A>
    }

    public static List<? super B>  lowerBound(List<? super B> list) {
//        for (A x : list) System.out.println(x);
        list.remove(new B());//because it requires an obj parameter
        list.add(new B());
        list.add(new C());
        Object object = list.get(0);
//        B b = list.get(0);

//        return new ArrayList();
//        return new ArrayList<B>();
//        return new ArrayList<A>();
        return new ArrayList<Object>();//no <C>
         }

    public static void main(String[] args) {
        List<String> keywords = new ArrayList<>();
        keywords.add("java");
//        printList(keywords); // DOES NOT COMPILE
//        List<Object> objects = keywords; // DOES NOT COMPILE

        List<?> bound = bound(keywords);
        List bound2 = bound(keywords); //no <Object>
        arraysException();

        ArrayList w = new ArrayList<>();
        ArrayList<Object> os = new ArrayList<>();
        ArrayList<A> as = new ArrayList<>();
        ArrayList<B> bs = new ArrayList<>();
        ArrayList<C> cs = new ArrayList<>();

        //upperBound
        List<? extends B> b = new ArrayList<>();
        List list1 = upperBound(w);
        List<? extends B> bs1 = upperBound(b);
        upperBound(bs);
        upperBound(cs);

//        b.add(new B()); // DOES NOT COMPILE
//        b.add(new C()); // DOES NOT COMPILE
        B be = b.get(0);

        //lower bound
        List<? super B> b2 = new ArrayList<>();
        b2.add(new B());
        b2.add(new C());
        Object bo = b2.get(0);

        List<? super B> objects = lowerBound(os);
        List list = lowerBound(w); // not A, B, C, Object
        lowerBound(b2);
        lowerBound(as);
        lowerBound(bs);

        b2.add(new B());
        b2.add(new C());

//        b = new ArrayList<A>();
        b = new ArrayList<B>();
        b = new ArrayList<C>();

        b2 = new ArrayList<A>();
        b2 = new ArrayList<B>();
//        b2 = new ArrayList<C>();

    }

    private static void arraysException() {
        Object[] o = new String[0];
        Integer[] numbers = {new Integer(42)};
        Object[] objects = numbers;
//        objects[0] = "forty two"; // throws ArrayStoreException
    }
}

class WildCardInheritance{
    void w(List<?> list){}
    void ex(List<? extends B> list){}
    void su(List<? super B> list){}
}
class WildCardInheritanceChild extends WildCardInheritance{
    //the only way to override with changed type
    @Override
    void w(List list) {
        super.w(list);
    }

    @Override
    void ex(List list) {
        super.ex(list);
    }

    @Override
    void su(List list) {
        super.su(list);
    }
}

class A {
}

class B extends A {
}

class C extends B {
}

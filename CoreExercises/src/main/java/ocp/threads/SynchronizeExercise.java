package ocp.threads;

import java.util.ArrayList;

public class SynchronizeExercise {
    /**
     * if is used by different SynchronizeExercise instances,
     * \it runs in parallel even if with synchronized method
     * if same, it block this method of obj
     */
    synchronized void count(String source) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            System.out.println(source + " " + i);
            Thread.sleep(500);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        SynchronizeExercise s = new SynchronizeExercise();
        Thread a = new Thread(() -> {
            try {
                s.count("a");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread b = new Thread(() -> {
            try {
                s.count("b");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        a.start();
        b.start();

    }
}

//todo: review how is that working
class VolatileExercise {

    volatile int i = 0;

    void count(String source) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            System.out.println(source + " " + this.i++);
            Thread.sleep(500);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        VolatileExercise s = new VolatileExercise();
        Thread a = new Thread(() -> {
            try {
                s.count("a");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread b = new Thread(() -> {
            try {
                s.count("b");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        a.start();
        b.start();

    }
}

//todo: add same ex with volatile

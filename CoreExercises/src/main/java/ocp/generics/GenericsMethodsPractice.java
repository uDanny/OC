package ocp.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsMethodsPractice {
    public static void main(String[] args) {
        List<String> a = GenericsMethodsPractice.ship("A");
        List<Integer> ship = GenericsMethodsPractice.<Integer>ship(2);

        List nonStringTyped = new ArrayList();
        nonStringTyped.add(2);

        List<String> strongTyped = new ArrayList();

        strongTyped(strongTyped);
        strongTyped(nonStringTyped);
        nonStrongTyped(strongTyped);
        nonStrongTyped(nonStringTyped);
//        String s = strongTyped.get(0);//ClassCastException
    }

    public static <T> List<T> ship(T t) {
        System.out.println("Preparing " + t);
        ArrayList<T> list = new ArrayList<>();
        return list;
    }

    public static List strongTyped(List<String> ss) {
        return null;
        //could be class exception if is called with nonStringTyped if String s = ss.get(0);
    }

    public static List<String> nonStrongTyped(List ss) {
        //could be class exception if is called with strongTyped<String> param, and inside is added an Integer
        //it fails on consuming data from the list
        ss.add(2);
        return ss;
    }
//    public static void strongTyped(List<Integer> ss) {} // can't use different generic type of parameter as param polymorphism
}


class LegacyAutoboxing {
    public static void main(String[] args) {
        java.util.List numbers = new java.util.ArrayList();
        numbers.add(5);
        Object o = numbers.get(0);// DOES NOT COMPILE

    }
}

package ocp.generics;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * cant:
 * Call the constructor. new T() is not allowed because at runtime it would be new Object().
 * Create an array of that static type. This one is the most annoying, but it makes sense because you’d be creating an array of Objects.
 * Call instanceof. This is not allowed because at runtime List<Integer> and List<String> look the same to Java thanks to type erasure.
 * Use a primitive type as a generic type parameter. This isn’t a big deal because you can use the wrapper class instead. If you want a type of int, just use Integer.
 * Create a static variable as a generic type parameter. This is not allowed because the type is linked to the instance of the class.
 */
public class GenericsPractice {
    public static void main(String[] args) {

        String t = new StringImpl().t;
        String[] ts = new StringImpl().ts;
        List<String> list = new StringImpl().list;


        StringImpl stringImpl = new StringImpl();
        stringImpl.t = "someString";
        stringImpl.method();

    }
}

abstract class AbsClass<T> {
//    static T st; //not allowed static
    T t;
    T[] ts;
    List<T> list;
    void method(){
//        T t = new T(); //no instantiation
//        T[] ts1 = new T[2]; //no array instantiation
        List<T> ts = new ArrayList<>(); //allowed

        if (t instanceof String){ //true
            System.out.println("isString");
        }
    }
    void notAllowedMethod(Object j){
//        if(j instanceof T){}
    }

}
class StringImpl extends AbsClass<String> {

}

abstract class AbsClassConstructor<T> {
    public AbsClassConstructor(T t) {
    }
}

class ImplClass2<T> extends AbsClassConstructor<T> {

    public ImplClass2(T t) {
        super(t);
    }
}

class ImplClass3 extends AbsClassConstructor<String> {

    public ImplClass3(String s) {
        super(s);
    }
}


//---

interface Shippable<T> {
    void ship(T t);
}

class ShippableRobotCrate implements Shippable<String> {
    public void ship(String t) {
    }
}

class ShippableAbstractCrate<U> implements Shippable<U> {
    public void ship(U t) {
    }
}

class ShippableCrate implements Shippable {
    public void ship(Object t) {
    }
}

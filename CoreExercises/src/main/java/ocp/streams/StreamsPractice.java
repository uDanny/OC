package ocp.streams;


import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class StreamsPractice {
    public static void main(String[] args) {
        Stream<String> stringStream = Stream.empty();

        Stream<Integer> singleElement = Stream.of(1);// count = 1
        Stream<Integer> fromArray = Stream.of(1, 2, 3); // count = 2


        List<String> list = Arrays.asList("a", "b", "c");
        Stream<String> fromList = list.stream();
        Stream<String> fromListParallel = list.parallelStream();

        Stream<Double> randoms = Stream.generate(Math::random);
        Stream<Double> randomManual = Stream.generate(()->2.0);
        Stream<Integer> oddNumbers = Stream.iterate(1, n -> n + 2);

        //operations
        long count = fromArray.count();

        //min, max
        Stream<String> s = Stream.of("monkey", "ape", "bonobo");
        Optional<String> min = s.min((s1, s2) -> s1.length() - s2.length());
        min.ifPresent(System.out::println); // ape

        //findAny findFirst
        //findAny() is useful when you are working with a parallel stream. It gives Java the flexibility to return to you the first element it comes by rather than the one that needs to be first in the stream based on the intermediate operations.
        Stream<String> s1 = Stream.of("monkey", "gorilla", "bonobo");
        Stream<String> infinite = Stream.generate(() -> "chimp");
        s1.findAny().ifPresent(System.out::println); // monkey
        infinite.findAny().ifPresent(System.out::println); // chimp

//        allMatch(), anyMatch() and noneMatch()

        List<String> list1 = Arrays.asList("monkey", "2", "chimp");
        Stream<String> infinite1 = Stream.generate(() -> "chimp");
        Predicate<String> pred = x -> Character.isLetter(x.charAt(0));
        System.out.println(list1.stream().anyMatch(pred)); // true
        System.out.println(list1.stream().allMatch(pred)); // false
        System.out.println(list1.stream().noneMatch(pred)); // false
        System.out.println(infinite1.anyMatch(pred)); // true


        Stream<String> s2 = Stream.of("Monkey", "Gorilla", "Bonobo");
        s2.forEach(System.out::print); // MonkeyGorillaBonobo


        //reduce
        Stream<String> stream = Stream.of("w", "o", "l", "f");
        String word = stream.reduce("", (ss, c) -> ss + c);
        System.out.println(word); // wolf


        Stream<Integer> empty = Stream.empty();
        Stream<Integer> oneElement = Stream.of(3);
        Stream<Integer> threeElements = Stream.of(3, 5, 6);

        BinaryOperator<Integer> op = (a, b) -> a * b;
        empty.reduce(op).ifPresent(System.out::print); // no output
        oneElement.reduce(op).ifPresent(System.out::print); // 3
        threeElements.reduce(op).ifPresent(System.out::print); // 90


        empty = Stream.empty();
        oneElement = Stream.of(3);
        threeElements = Stream.of(3, 5, 6);

        System.out.println(empty.reduce(1, op)); // 1
        System.out.println(oneElement.reduce(1, op)); // 3
        System.out.println(threeElements.reduce(2, op)); // 180

        //collect
        Stream<String> streamc = Stream.of("w", "o", "l", "f");
        StringBuilder wordc2 = streamc.collect(StringBuilder::new, StringBuilder::append, (x, y) -> x.append(y));
//        StringBuilder wordc = streamc.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append); //equivalent to (x, y) -> x.append(y)
//        StringBuilder wordc22 = streamc.collect(StringBuilder::new, StringBuilder::append, (x, y) -> TempFunctInterface.doMake(x, y));
//        StringBuilder wordc222 = streamc.collect(StringBuilder::new, StringBuilder::append, TempFunctInterface::doMake);  //equivalent to (x, y) -> TempFunctInterface.doMake(x, y)
        // the second BiConsumer is for merging streams processed in parallel


        Stream<String> streamc1 = Stream.of("w", "o", "l", "f");
        TreeSet<String> set = streamc1.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);
//        TreeSet<String> set2 = streamc1.collect(() -> new TreeSet<>(), (strings, e) -> strings.add(e), (strings, c) -> strings.addAll(c));
        System.out.println(set); // [f, l, o, w]


        Stream<String> streamc3 = Stream.of("w", "o", "l", "f");
        TreeSet<String> setc3 = streamc3.collect(Collectors.toCollection(TreeSet::new));
        System.out.println(setc3); // [f, l, o, w]

        Set<String> collect = Stream.of("w", "o", "l", "f").collect(Collectors.toSet());


        // INTERMEDIATE OPERATORS

        //filter
        Stream<String> sf = Stream.of("monkey", "gorilla", "bonobo");
        sf.filter(x -> x.startsWith("m")).forEach(System.out::print); // monkey

        //distinct
        Stream<String> sd = Stream.of("duck", "duck", "duck", "goose");
        sd.distinct().forEach(System.out::print); // duckgoose

        //skip, limit
        Stream<Integer> ssl = Stream.iterate(1, n -> n + 1);
        ssl.skip(5).limit(2).forEach(System.out::print); // 67

        //map
        Stream<String> sm = Stream.of("monkey", "gorilla", "bonobo");
        sm.map(String::length).forEach(System.out::print); // 676

        //flatmap
        List<String> zero = Arrays.asList();
        List<String> one = Arrays.asList("Bonobo");
        List<String> two = Arrays.asList("Mama Gorilla", "Baby Gorilla");
        Stream<List<String>> animals = Stream.of(zero, one, two);
        animals.flatMap(l -> l.stream()).forEach(System.out::println);

        //sorted
        Stream<String> ss = Stream.of("brown-", "bear-");
        ss.sorted().forEach(System.out::print); // bear-brown-

        Stream<String> ss1 = Stream.of("brown bear-", "grizzly-");
        ss1.sorted(Comparator.reverseOrder())
                .forEach(System.out::print);

        Stream.of("brown bear-", "grizzly-").sorted(Comparator.reverseOrder());

        //peek
        Stream<String> streamp = Stream.of("black bear", "brown bear", "grizzly");
        long countp = streamp.filter(sss -> sss.startsWith("g"))
                .peek(x -> System.out.println(x)).count(); // grizzly
        System.out.println(countp); // 1

        //mapToInt
        primitiveStreams();

        streams2();


    }

    private static void primitiveStreamsFunctions() {
        IntSupplier is = () -> 1;
        int asInt = is.getAsInt();
        LongSupplier ls = () -> (long) 1;
        DoubleSupplier ds = () -> 0.2;

        IntConsumer ic = (x) -> System.out.println(x);
        LongConsumer lc = (x) -> System.out.println(x);
        DoubleConsumer da = (x) -> System.out.println(x);

        IntPredicate ip = (x) -> x < 0;
        LongPredicate lp = (x) -> x < 0;
        DoublePredicate dp = (x) -> x < 0;

        IntFunction<String> iF = x -> String.valueOf(x);
        LongFunction<String> lf = x -> String.valueOf(x);
        DoubleFunction<String> df = x -> String.valueOf(x);

        IntUnaryOperator iuo = (x) -> x + 1;
        LongUnaryOperator luo = (x) -> x + 1;
        DoubleUnaryOperator duo = (x) -> x + 1;

        DoubleBinaryOperator dbo = (x, y) -> x + y;
        IntBinaryOperator ibo = (x, y) -> x + y;
        LongBinaryOperator lbo = (x, y) -> x + y;
    }

    private static void primitiveStreams() {
        DoubleStream doubleStream = DoubleStream.of(2.0);
        IntStream intStream = IntStream.of(2);
        LongStream longStream = LongStream.of(1);
//
//        //to-delete
//        intStream.reduce(1, (x,y)->x+y);


        Stream<Integer> stream = Stream.of(1, 2, 3);
        IntStream intStream2 = stream.mapToInt(x -> x);
        System.out.println(intStream2.sum());
        System.out.println(Stream.of(1, 2, 3).mapToInt(x -> x).average().getAsDouble());


        DoubleStream random = DoubleStream.generate(Math::random);
        DoubleStream fractions = DoubleStream.iterate(.5, d -> d / 2);
        random.limit(3).forEach(System.out::println);
        System.out.println();
        fractions.limit(3).forEach(System.out::println);

        IntStream.range(1, 6);

        IntStream ints = Arrays.asList(1, 2).stream().flatMapToInt(x -> IntStream.of(x));
        IntStream ints2 = Arrays.asList(1, 2).stream().mapToInt(x -> x);

        //Optional for primitives
        IntStream stream2 = IntStream.rangeClosed(1, 10);
        OptionalDouble optional = stream2.average();
        optional.ifPresent(System.out::println);
        System.out.println(optional.getAsDouble());
        System.out.println(optional.orElseGet(() -> Double.NaN));

        LongStream longs = LongStream.of(5, 10);
        long sum = longs.sum();
        System.out.println(sum); // 15
        DoubleStream doubles = DoubleStream.generate(() -> Math.PI);
//        OptionalDouble min = doubles.min(); // runs infinitely

        IntSummaryStatistics stats = ints.summaryStatistics();
        if (stats.getCount() == 0) throw new RuntimeException();
        int i = stats.getMax() - stats.getMin();
        long l = stats.getSum();
        double ii = stats.getAverage();


        BooleanSupplier b1 = () -> true;
        BooleanSupplier b2 = () -> Math.random() > .5;
        System.out.println(b1.getAsBoolean());
        System.out.println(b2.getAsBoolean());


    }


    private static void streams2() {

        Stream<String> ohMy = Stream.of("lions", "tigers", "bears");
        String result = ohMy.collect(Collectors.joining(", "));
        System.out.println(result); // lions, tigers, bears

        Stream<String> ohMy2 = Stream.of("lions", "tigers", "bears");
        Double result2 = ohMy2.collect(Collectors.averagingInt(String::length));
        System.out.println(result2); // 5.333333333333333

        Stream<String> ohMy3 = Stream.of("lions", "tigers", "bears");
        TreeSet<String> result3 = ohMy3.filter(s -> s.startsWith("t")).collect(Collectors.toCollection(TreeSet::new));
        System.out.println(result3); // [tigers]


        //toMap
        Stream<String> ohMy4 = Stream.of("lions", "tigers", "bears");
        Map<String, Integer> map = ohMy4.collect(
                Collectors.toMap(s->s, String::length));
        ohMy4.collect(
                Collectors.toMap(Function.identity(), String::length)); // key(s->s) and value functions
        System.out.println(map); // {lions=5, bears=5, tigers=6}

        Stream<String> ohMy5 = Stream.of("lions", "tigers", "bears");
        Map<Integer, String> map5 = ohMy5.collect(Collectors.toMap(
                String::length, k -> k, (s1, s2) -> s1 + "," + s2));
        System.out.println(map5); // {5=lions,bears, 6=tigers}
        System.out.println(map5.getClass()); // class. java.util.HashMap

        Stream<String> ohMy6 = Stream.of("lions", "tigers", "bears");
        TreeMap<Integer, String> map6 = ohMy6.collect(Collectors.toMap(
                String::length, k -> k, (s1, s2) -> s1 + "," + s2, TreeMap::new));
        System.out.println(map6); // // {5=lions,bears, 6=tigers}
        System.out.println(map.getClass()); // class. java.util.TreeMap

//        Grouping

        Stream<String> ohMy7 = Stream.of("lions", "tigers", "bears");
        Map<Integer, List<String>> map7 = ohMy7.collect(
                Collectors.groupingBy(String::length));
        System.out.println(map7); // {5=[lions, bears], 6=[tigers]}

        Stream<String> ohMy8 = Stream.of("lions", "tigers", "bears");
        Map<Integer, Set<String>> map8 = ohMy8.collect(
                Collectors.groupingBy(String::length, Collectors.toSet()));
        System.out.println(map8); // {5=[lions, bears], 6=[tigers]}

        Stream<String> ohMy9 = Stream.of("lions", "tigers", "bears");
        TreeMap<Integer, Set<String>> map9 = ohMy9.collect(
                Collectors.groupingBy(String::length, TreeMap::new, Collectors.toSet()));
        System.out.println(map9); // {5=[lions, bears], 6=[tigers]}


        Stream<String> ohMy1 = Stream.of("lions", "tigers", "bears");
        TreeMap<Integer, List<String>> map1 = ohMy1.collect(
                Collectors.groupingBy(String::length, TreeMap::new, Collectors.toList()));
        System.out.println(map1);

//      Partitioning

        Stream<String> ohMy11 = Stream.of("lions", "tigers", "bears");
        Map<Boolean, List<String>> map11 = ohMy11.collect(
                Collectors.partitioningBy(s -> s.length() <= 5));
        System.out.println(map11); // {false=[tigers], true=[lions, bears]}


        Stream<String> ohMy12 = Stream.of("lions", "tigers", "bears");
        Map<Boolean, List<String>> map12 = ohMy12.collect(
                Collectors.partitioningBy(s -> s.length() <= 7));
        System.out.println(map12); // {false=[], true=[lions, tigers, bears]}

        Stream<String> ohMy13 = Stream.of("lions", "tigers", "bears");
        Map<Boolean, Set<String>> map13 = ohMy13.collect(
                Collectors.partitioningBy(s -> s.length() <= 7, Collectors.toSet()));
        System.out.println(map13);// {false=[], true=[lions, tigers, bears]}

        Stream<String> ohMy14 = Stream.of("lions", "tigers", "bears");
        Map<Integer, Long> map14 = ohMy14.collect(Collectors.groupingBy(
                String::length, Collectors.counting()));
        System.out.println(map14); // {5=2, 6=1}

        //mapping
        Set<String> collect = Stream.of("lions", "tigers", "bears")
                .collect(Collectors.mapping(x -> x + 2, Collectors.toSet()));

        //todo: reivew one more time
//        Stream<String> ohMy15 = Stream.of("lions", "tigers", "bears");
//        Map<Integer, Optional<Character>> map15 = ohMy15.collect(Collectors.groupingBy(
//                String::length,
//                Collectors.mapping(s -> s.charAt(0),
//                        Collectors.minBy(Comparator.naturalOrder()))));
//        System.out.println(map15); // {5=Optional[b], 6=Optional[t]}

    }
}

//@FunctionalInterface
interface TempFunctInterface {
    static StringBuilder doMake(StringBuilder s1, StringBuilder s2) {
        return null;
    }

}

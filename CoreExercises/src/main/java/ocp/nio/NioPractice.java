package ocp.nio;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.*;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.*;

public class NioPractice {
    public static void main(String[] args) throws URISyntaxException {
        try {


            Path path1 = Paths.get("pandas/cuddly.png");
            Path path2 = Paths.get("c:\\zooinfo\\November\\employees.txt");
            Path path3 = Paths.get("/home/zoodirector");

            Path path11 = Paths.get("pandas", "cuddly.png");
            Path path22 = Paths.get("c:", "zooinfo", "November", "employees.txt");
            Path path33 = Paths.get("/", "home", "zoodirector");

            Path path111 = Paths.get(new URI("file://pandas/cuddly.png")); // THROWS EXCEPTION // AT RUNTIME
            Path path222 = Paths.get(new URI("file:///c:/zoo-info/November/employees.txt"));
            Path path333 = Paths.get(new URI("file:///home/zoodirectory"));

            Path path4 = Paths.get(new URI("http://www.wiley.com"));
            Path path5 = Paths.get(new URI("ftp://username:password@ftp.the-ftp-server.com"));

            Path path44 = Paths.get(new URI("http://www.wiley.com"));
            URI uri44 = path4.toUri();
        } catch (Exception e) {

        }
        try {

            Path path1 = FileSystems.getDefault().getPath("pandas/cuddly.png");
            Path path2 = FileSystems.getDefault().getPath("c:", "zooinfo", "November", "employees.txt");
            Path path3 = FileSystems.getDefault().getPath("/home/zoodirector");
            FileSystem fileSystem = FileSystems.getFileSystem(new URI("http://www.selikoff.net"));
            Path path = fileSystem.getPath("duck.txt");


            File file = new File("pandas/cuddly.png");
            Path path11 = file.toPath();
            Path path12 = Paths.get("cuddly.png");
            File file12 = path.toFile();
        } catch (Exception e) {

        }

        try {
            Path path = FileSystems.getDefault().getPath("pandas/cuddly.png");
            path.toRealPath();
        } catch (IOException e) { //this operation requires the file to exist
            e.printStackTrace();
        }


        LinkOption nofollowLinks = NOFOLLOW_LINKS;
        FileVisitOption followLinks = FOLLOW_LINKS;
        StandardCopyOption copyAttributes = COPY_ATTRIBUTES;
        StandardCopyOption replaceExisting = REPLACE_EXISTING;
        StandardCopyOption atomicMove = ATOMIC_MOVE;


        Paths.get("/zoo/../home").getParent().normalize().toAbsolutePath();

        Path path0 = Paths.get("/land/hippo/harry.happy");
        System.out.println("The Path Name is: " + path0);
        for (int i = 0; i < path0.getNameCount(); i++) {
            System.out.println(" Element " + i + " is: " + path0.getName(i));
        }
    }
}

class PathFilePathTest {
    public static void printPathInformation(Path path) {
        System.out.println("Filename is: " + path.getFileName()); //shells.txt
        System.out.println("Root is: " + path.getRoot()); // Root is: / | Root is: null if relative
        Path currentParent = path;
        while ((currentParent = currentParent.getParent()) != null) {
            System.out.println(" Current parent is: " + currentParent);
//            Current parent is: /zoo/armadillo
//            Current parent is: /zoo
//            Current parent is: /

//            Current parent is: armadillo
        }
    }

    public static void main(String[] args) {
        printPathInformation(Paths.get("/zoo/armadillo/shells.txt"));
        System.out.println();
        printPathInformation(Paths.get("armadillo/shells.txt"));
    }
}

class ToAbsolute {
    public static void main(String[] args) {

        Path path1 = Paths.get("C:\\birds\\egret.txt");
        System.out.println("Path1 is Absolute? " + path1.isAbsolute()); //Path1 is Absolute? true
        System.out.println("Absolute Path1: " + path1.toAbsolutePath()); //Absolute Path1: C:\birds\egret.txt
        Path path2 = Paths.get("birds/condor.txt");
        System.out.println("Path2 is Absolute? " + path2.isAbsolute()); //Path2 is Absolute? false
        System.out.println("Absolute Path2 " + path2.toAbsolutePath()); //Absolute Path2 /home/birds/condor.txt

    }
}

class SubPaths {
    public static void main(String[] args) {
        Path path = Paths.get("/mammal/carnivore/raccoon.image");
        System.out.println("Path is: " + path);
        System.out.println("Subpath from 0 to 3 is: " + path.subpath(0, 3)); //mammal/carnivore/raccoon.image
        System.out.println("Subpath from 1 to 3 is: " + path.subpath(1, 3)); // carnivore/raccoon.image
        System.out.println("Subpath from 1 to 2 is: " + path.subpath(1, 2)); //carnivore

//        System.out.println("Subpath from 0 to 4 is: "+path.subpath(0,4)); // THROWS // EXCEPTION AT RUNTIME
//        System.out.println("Subpath from 1 to 1 is: "+path.subpath(1,1)); // THROWS // EXCEPTION AT RUNTIME
    }
}

class Relativise {
    public static void main(String[] args) {


        Path path1 = Paths.get("fish.txt");
        Path path2 = Paths.get("birds.txt");
        Path path33 = Paths.get("birdsFolder");
        Path path44 = Paths.get("birdsFolder/inside");
        Path path55 = Paths.get("birdsFolder/inside2");
        System.out.println(path1.relativize(path2)); // ..\birds.txt
        System.out.println(path2.relativize(path1)); // ..\fish.txt
        System.out.println(path33.relativize(path44)); // inside
        System.out.println(path44.relativize(path33)); // ..
        System.out.println(path44.relativize(path55)); // ../inside2

        Path path3 = Paths.get("E:\\habitat");
        Path path4 = Paths.get("E:\\sanctuary\\raven");
        System.out.println(path3.relativize(path4)); // ..\sanctuary\raven
        System.out.println(path4.relativize(path3)); // ..\..\habitat

        //both paths should be relative or both absolute
//        Path path1 = Paths.get("/primate/chimpanzee");
//        Path path2 = Paths.get("bananas.txt");
//        path1.relativize(path3); // THROWS EXCEPTION AT RUNTIME
    }
}

class Resolve {
    public static void main(String[] args) {
        final Path path1 = Paths.get("/cats/../panther");
        final Path path2 = Paths.get("food");
        System.out.println(path1.resolve(path2)); // /cats/../panther/food
        System.out.println(path1.normalize().resolve(path2)); // /panther/food


        final Path path11 = Paths.get("/turkey/food");
        final Path path22 = Paths.get("/tiger/cage");
        System.out.println(path11.resolve(path22)); // /tiger/cage , because is absolute


        Path path3 = Paths.get("E:\\data");
        Path path4 = Paths.get("E:\\user\\home");
        Path relativePath = path3.relativize(path4);
        System.out.println(path3.resolve(relativePath)); // E : \ data \ . . \ user \ home
        System.out.println(path3.resolve(relativePath).normalize()); // E : \ user \ home
//        path3.toRealPath(); //checks if file exists

        path3.toAbsolutePath();
        // method in that it can convert a relative path to an absolute path,
        // it also verifies that the file referenced by the path actually exists, and thus it throws a checked IOException at runtime if the file cannot be located.
        // It is also the only Path method to support the NOFOLLOW_LINKS option.
        // it is automatically normalized
        // it also convert to absolute path


        //assume we are in the path: /horse/schedule
        try {
            System.out.println(Paths.get("/zebra/food.source").toRealPath());
            System.out.println(Paths.get(".././food.txt").toRealPath());

            System.out.println(Paths.get(".").toRealPath()); //get curent path

        } catch (IOException e) {
            // Handle file I/O exception...
        }

    }
}

class ExistCheck {
    public static void main(String[] args) {
        Files.exists(Paths.get("/ostrich/feathers.png"));
        Files.exists(Paths.get("/ostrich"));
    }
}

class IsSame {
    public static void main(String[] args) {
        try {
            //is same checks if path is the same, and checks in fileSystem, not the content itself
            System.out.println(Files.isSameFile(Paths.get("/user/home/cobra"), Paths.get("/user/home/snake"))); //reuw
            System.out.println(Files.isSameFile(Paths.get("/user/tree/../monkey"), Paths.get("/user/monkey")));
            System.out.println(Files.isSameFile(Paths.get("/leaves/./giraffe.exe"), Paths.get("/leaves/giraffe.exe")));
            System.out.println(Files.isSameFile(Paths.get("/flamingo/tail.data"), Paths.get("/cardinal/tail.data")));
        } catch (IOException e) { //throws if doesnt exist one of files
// Handle file I/O exception...
        }
    }
}

class CreateDirectory {
    public static void main(String[] args) {
        try {
            Files.createDirectory(Paths.get("/bison/field")); //throws if not exist the whole path
            Files.createDirectories(Paths.get("/bison/field/pasture/green")); //creates whole path, throws if already exist
        } catch (IOException e) {
            // Handle file I/O exception...
        }

    }
}

class Copy {
    public static void main(String[] args) {
        try {
            //default is NOFOLLOW_LINKS
            Files.copy(Paths.get("/panda"), Paths.get("/panda-save"));
            Files.copy(Paths.get("/panda/bamboo.txt"), Paths.get("/panda-save/bamboo.txt"));
        } catch (IOException e) {
            // Handle file I/O exception...
        }


        try (InputStream is = new FileInputStream("source-data.txt");
             OutputStream out = new FileOutputStream("output-data.txt")) { // Copy stream data to file
            Files.copy(is, Paths.get("c:\\mammals\\wolf.txt"));
            Files.copy(is, Paths.get("c:\\mammals\\wolf.txt"), NOFOLLOW_LINKS);
            // Copy file data to stream
            Files.copy(Paths.get("c:\\fish\\clown.xsl"), out);
        } catch (IOException e) {
            // Handle file I/O exception... }

        }
    }
}

class Move {
    public static void main(String[] args) {
        try {
            //default: exception if already exists, copy all data
            //also can't copy non empty dir to other drive
            Files.move(Paths.get("c:\\zoo"), Paths.get("c:\\zoo-new"));
            Files.move(Paths.get("c:\\user\\addresses.txt"), Paths.get("c:\\zoo-new\\addresses.txt"));
        } catch (IOException e) {
// Handle file I/O exception...
        }
    }
}

class Delete {
    public static void main(String[] args) {
        try {
            Files.delete(Paths.get("/vulture/feathers.txt")); //if is a nonempty path, DirectoryNotEmptyException
            Files.deleteIfExists(Paths.get("/pigeon")); //false if not exists or emtpy
        } catch (IOException e) {
// Handle file I/O exception...
        }
    }
}

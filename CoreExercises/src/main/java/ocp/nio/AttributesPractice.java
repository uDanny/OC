package ocp.nio;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.UserPrincipal;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AttributesPractice {
    public static void main(String[] args) {
        //if no file, exception is not thrown
        System.out.println(Files.isDirectory(Paths.get("/canine/coyote/fur.jpg")));
        System.out.println(Files.isRegularFile(Paths.get("/canine/types.txt")));
        System.out.println(Files.isSymbolicLink(Paths.get("/canine/coyote")));
    }
}

class Access {
    public static void main(String[] args) {
        try {
            System.out.println(Files.isHidden(Paths.get("/walrus.txt")));
        } catch (IOException e) {
            // Handle file I/O exception...
        }


        //isReadable, isExecutable
        System.out.println(Files.isReadable(Paths.get("/seal/baby.png")));
        System.out.println(Files.isExecutable(Paths.get("/seal/baby.png")));

        //size
        try {
            System.out.println(Files.size(Paths.get("/zoo/c/animals.txt")));
        } catch (IOException e) {

            // Handle file I/O exception...
        }
    }
}

class ModifyDate {
    public static void main(String[] args) {

        try {
            final Path path = Paths.get("/rabbit/food.jpg");
            FileTime lastModifiedTime = Files.getLastModifiedTime(path);
            System.out.println(lastModifiedTime.toMillis());
            Files.setLastModifiedTime(path, FileTime.fromMillis(System.currentTimeMillis()));
            System.out.println(Files.getLastModifiedTime(path).toMillis());
        } catch (IOException e) {

            // Handle file I/O exception...
        }

    }
}

class Owner {

    public static void main(String[] args) {
        try {
            UserPrincipal owner = FileSystems.getDefault().getUserPrincipalLookupService().lookupPrincipalByName("jane");
            Path path = null;
            UserPrincipal owner2 = path.getFileSystem().getUserPrincipalLookupService()
                    .lookupPrincipalByName("jane");
        } catch (IOException e) {

        }


        try {
            // Read owner of file
            Path path = Paths.get("/chicken/feathers.txt");
            System.out.println(Files.getOwner(path).getName());
            // Change owner of file
            UserPrincipal owner = path.getFileSystem()
                    .getUserPrincipalLookupService().lookupPrincipalByName("jane");
            Files.setOwner(path, owner);
            // Output the updated owner information
            System.out.println(Files.getOwner(path).getName());
        } catch (IOException e) {
            // Handle file I/O exception...
        }
    }
}

class BasicFileAttributesSample {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/turtles/sea.txt");
        BasicFileAttributes data = Files.readAttributes(path, BasicFileAttributes.class);
        System.out.println("Is path a directory? " + data.isDirectory());
        System.out.println("Is path a regular file? " + data.isRegularFile());
        System.out.println("Is path a symbolic link? " + data.isSymbolicLink());
        System.out.println("Path not a file, directory, nor symbolic link? " + data.isOther());
        System.out.println("Size (in bytes): " + data.size());
        System.out.println("Creation date/time: " + data.creationTime());
        System.out.println("Last modified date/time: " + data.lastModifiedTime());
        System.out.println("Last accessed date/time: " + data.lastAccessTime());
        System.out.println("Unique file identifier (if available): " + data.fileKey());
    }
}

class BasicFileAttributeViewSample {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/turtles/sea.txt");
        BasicFileAttributeView view = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        BasicFileAttributes data = view.readAttributes();
        FileTime lastModifiedTime = FileTime.fromMillis(data.lastModifiedTime().toMillis() + 10_000);
        view.setTimes(lastModifiedTime, null, null); //null == no modify
    }
}

class Walk {
    public static void main(String[] args) {
        Path path = Paths.get("/Users/danielursachi/IdeaProjects/Ex/Java/OC/CoreExercises/src/main/java");
        try {
            Files.walk(path) // can modified maxFiles.walk(path) the default Integer.MAX_VALUE, or FOLLOW_LINKS for symbolic links, but FileSystemLoopException can be thrown

                    .filter(p -> p.toString()
                            .endsWith(".java"))
                    .forEach(System.out::println);
        } catch (IOException e) {
            // Handle file I/O exception...
        }
    }
}

class Find {
    public static void main(String[] args) {
        Path path = Paths.get("/Users/danielursachi/IdeaProjects/Ex/Java/OC/CoreExercises/src/main/java");
        long dateFilter = 1420070400000l;
        try {
            Stream<Path> stream = Files.find(path, 10, (p, a) -> p.toString().endsWith(".java")
                    && a.lastModifiedTime().toMillis() > dateFilter);
            stream.forEach(System.out::println);
        } catch (Exception e) {
            // Handle file I/O exception...
        }
    }
}

class List {
    public static void main(String[] args) {
        try {
            Path path = Paths.get("/Users/danielursachi/IdeaProjects/Ex/Java/OC/CoreExercises/src/main/java");
            Files.list(path)  //java.io.File.listFiles
                    .filter(p -> !Files.isDirectory(p))
                    .map(p -> p.toAbsolutePath())
                    .forEach(System.out::println);
        } catch (IOException e) {
// Handle file I/O exception...
        }
    }

}

class LazyEvaluation {
    public static void main(String[] args) {

        try {
            java.util.List<String> allList = Files.readAllLines(Paths.get("birds.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path path = Paths.get("/fish/sharks.log");
        try {
            Files.lines(path).forEach(System.out::println);
        } catch (IOException e) {
            // Handle file I/O exception...
        }


        try {
            System.out.println(Files.lines(path)
                    .filter(s -> s.startsWith("WARN "))
                    .map(s -> s.substring(5))
                    .collect(Collectors.toList()));
        } catch (IOException e) {
            // Handle file I/O exception...
        }
    }
}

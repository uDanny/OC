package ocp;

public class FinalTemp {

    //todo: hierarchy
    //todo: ioArch
    //todo: reduce, collectors?
    public static void main(String[] args) {

    }
}

interface Fa {
    int i = 2;

    static int getI() {
        return i;
    }

    int ii = 2;

    static int getII() {
        return ii;
    }
}

interface Fb {
    int i = 2;

    static int getI() {
        return i;
    }
}

class FC implements Fa, Fb {
    public static void main(String[] args) {
        System.out.println(ii); //can access from interface because there is no conflict
//        System.out.println(getII()); //can NOT access because there is no conflict

        System.out.println(Fa.i);
        System.out.println(Fa.getI());


    }
}

class FPc {
    static int i = 2;

    static int getI() {
        return i;
    }

    static int iii = 2;

    static int getIII() {
        return iii;
    }
}

class FC2 extends FPc {
    public static void main(String[] args) {
        System.out.println(i);
        System.out.println(getI());
    }

}

class FC3 extends FPc implements Fa, Fb {
    public static void main(String[] args) {

        System.out.println(ii); //can access from interface because there is no conflict
        System.out.println(iii); //can access from class because there is no conflict
//        System.out.println(getII()); //can NOT access because there is no conflict
        System.out.println(getIII()); //can NOT access because there is no conflict

        System.out.println(Fa.i);
        System.out.println(Fa.getI());
    }
}


interface RandI{
    int i = 2;

    static int getI(){return i;}
}
interface RandII extends RandI{
    default void met(){
        System.out.println(i);
        RandI.getI();
    }
    static void met2(){
        System.out.println(i);
        RandI.getI();
    }
}

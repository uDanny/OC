package ocp.collections;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * void clear() - Removes all keys and values from the map.
 * boolean isEmpty() - Returns whether the map is empty.
 * int size() - Returns the number of entries (key/value pairs) in the map.
 * V get(Object key) - Returns the value mapped by key or null if none is mapped.
 * V put(K key, V value) V - Adds or replaces key/value pair. Returns previous value or null.
 * remove(Object key) - Removes and returns value mapped to key. Returns null if none.
 * boolean containsKey(Object key) - Returns whether key is in map.
 * boolean containsValue(Object) - Returns value is in map.
 * Set<K> keySet() Collection<V> values() - Returns set of all keys.
 */
public class MapPractice {
    public static void main(String[] args) {
        Map<String, String> map = new LinkedHashMap<>();

        Collection<String> values = map.values();


        TreeMap<String, String> treeMap = new TreeMap<>();//not allows nulls
        new Hashtable<String, String>(); //not allows nulls

        System.out.println(treeMap.put("a", "av")); //returns null
        System.out.println(treeMap.put("a", "av2")); //returns av


        Map<String, String> favorites = new HashMap<>();
        favorites.put("Jenny", "BusTour");
        favorites.put("Tom", null);

        String s = favorites.putIfAbsent("Jenny", "Tram"); // BusTour , map not changed
        String s1 = favorites.putIfAbsent("Sam", "Tram"); // null, added
        String s2 = favorites.putIfAbsent("Tom", "Tram"); //null, added
        System.out.println(favorites); // {Tom=Tram, Jenny=BusTour, Sam=Tram}

        merge();
        computeIfPresent();
        computeIfAbsent();
        compute();
    }

    public static void computeIfPresent() {
        System.out.println("computeIfPresent");
        // it runs only when the key isn’t present or is null:
        // the return value is the result of what changed in the map or null if that doesn’t apply.
        Map<String, Integer> counts = new HashMap<>();
        counts.put("Jenny", 1);
        counts.put("Tom", null);
        BiFunction<String, Integer, Integer> nullMap = (k, v) -> null;
        BiFunction<String, Integer, Integer> notNullMap = (k, v) -> v + 1;

        Integer jenny = counts.computeIfPresent("Jenny", notNullMap); // returns 2, changed value
        Integer jennyNull = counts.computeIfPresent(null, notNullMap); // returns null, no changes value
        Integer tom = counts.computeIfPresent("Tom", notNullMap); // null, map not changed
        Integer sam = counts.computeIfPresent("Sam", notNullMap); // null, map not changed

        counts = new HashMap<>();
        counts.put("Jenny", 1);
        counts.put("Tom", null);

        Integer jenny2 = counts.computeIfPresent("Jenny", nullMap); // returns null, removes key
        Integer sam2 = counts.computeIfPresent("Sam", nullMap); // null, map not changed
        Integer tom2 = counts.computeIfPresent("Tom", nullMap); // null, map not changed
        Integer tom3 = counts.computeIfPresent(null, nullMap); // null, map not changed


//nullPractice
//        {Jenny=2}

        counts.computeIfPresent("Jenny", (k, v) -> null);


    }

    public static void computeIfAbsent() {
        Map<String, Integer> counts = new HashMap<>();
        counts.put("Jenny", 15);
        counts.put("Tom", null);
        Function<String, Integer> notNullMapper = (k) -> 1;
        Function<String, Integer> nullMapper = (k) -> null;

        Integer Jenny = counts.computeIfAbsent("Jenny", notNullMapper); // returns 15, no change
//        Integer JennyNull = counts.computeIfAbsent(null, notNullMapper); // 1, it changes first, and can add duplicate keys
        Integer sam = counts.computeIfAbsent("Sam", notNullMapper); // 1, add key
        Integer tom = counts.computeIfAbsent("Tom", notNullMapper); // 1, changes value


        counts = new HashMap<>();
        counts.put("Jenny", 15);
        counts.put("Tom", null);

        Integer Jenny1 = counts.computeIfAbsent("Jenny", nullMapper); //returns 15, map no change
        Integer sam2 = counts.computeIfAbsent("Sam", nullMapper); //returns null, map no change
        Integer tom2 = counts.computeIfAbsent("Tom", nullMapper); //returns null, map no change
        Integer n = counts.computeIfAbsent(null, nullMapper); //returns null, map no change


    }

    public static void compute() {
        System.out.println("compute");

        Map<String, Integer> counts = new HashMap<>();
        counts.put("Jenny", 1);
        counts.put("Tom", null);
        BiFunction<String, Integer, Integer> nullMap = (k, v) -> null;
        BiFunction<String, Integer, Integer> notNullMap = (k, v) -> 2;

        Integer jenny = counts.compute("Jenny", notNullMap); // returns 2, changed value
        Integer jennyNull = counts.compute(null, notNullMap); // 2, add key with null
        Integer tom = counts.compute("Tom", notNullMap); // 2, value changed to 2
        Integer sam = counts.compute("Sam", notNullMap); // 2, value entry added

        counts = new HashMap<>();
        counts.put("PreJenny", 1);
        counts.put("Jenny", 1);
        counts.put("Tom", null);

        Integer jenny2 = counts.compute("Jenny", nullMap); // returns null, removes key
        Integer sam2 = counts.compute("Sam", nullMap); // null, map not changed
        Integer tom2 = counts.compute("Tom", nullMap); // null, removes key
        Integer tom3 = counts.compute(null, nullMap); // null, map not changed


    }

    public static void merge() {
        System.out.println("merge");

        BiFunction<String, String, String> mapper = (v1, v2)
                -> v1.length() > v2.length() ? v1 : v2;
        Map<String, String> favorites = new HashMap<>();
        favorites.put("Jenny", "Bus Tour");
        favorites.put("Tom", "Tram");
        //merge returns the chosen value
        String jenny = favorites.merge("Jenny", "Skyride", mapper); //Bus Tour because is longer
        String tom = favorites.merge("Tom", "Skyride", mapper); //Skyride because is longer
        String tom2 = favorites.merge("Tom2", "Skyride", mapper); //just adds the new one

        System.out.println(favorites); // {Tom=Skyride, Jenny=Bus Tour}
        System.out.println(jenny); // Bus Tour
        System.out.println(tom); // Skyride

        ////
        System.out.println("null examples");

        Map<String, String> favoritesWithNull = new HashMap<>();
        favoritesWithNull.put("Sam", null);
        favoritesWithNull.merge("Sam", "Skyride", mapper); //BiFunction is not called, is set by default the new value (not allowed null as a new value, will throw an exception)
        System.out.println(favoritesWithNull);
//        favorites.merge("Sam", null, mapper); // throws an exception
//        favorites.merge("Sam", null, (v1, v2)-> v1); // throws an exception
//        favorites.merge("a", null, (v1, v2)-> v2); // throws an exception

        //null function
        BiFunction<String, String, String> nullFunction = (s, s2) -> null;
        BiFunction<String, String, String> notNullFunction = (s, s2) -> "a";
        favoritesWithNull = new HashMap<>();
        favoritesWithNull.put("Sam", "Skyride0");
        favoritesWithNull.put("Tom", null);

        String merge0 = favoritesWithNull.merge("Sam", "Skyride", notNullFunction);// returns a, change value to a
        String merge02 = favoritesWithNull.merge("Tom", "Skyride", notNullFunction);// returns Skyride function not evaluated
        String merge03 = favoritesWithNull.merge("Sam2", "Skyride", notNullFunction);// returns Skyride, add key Sam2 Skyride, because key is not found
        String merge04 = favoritesWithNull.merge(null, "Skyride", notNullFunction);// returns Skyride, duplicates key of tom->Skyride


        favoritesWithNull = new HashMap<>();
        favoritesWithNull.put("Sam", "Skyride1");
        favoritesWithNull.put("Tom", null);
        String merge = favoritesWithNull.merge("Sam", "Skyride", nullFunction);// returns null, remove map entry, because key is found
        String merge2 = favoritesWithNull.merge("Tom", "Skyride", nullFunction);// returns Skyride, add value, function not evaluated
        String merge3 = favoritesWithNull.merge("Sam2", "Skyride", nullFunction);// returns Skyride, add key Sam2 Skyride, because key is not found and function not evaluated
        String merge4 = favoritesWithNull.merge(null, "Skyride", nullFunction);// returns Skyride, duplicates key of tom->Skyride
        System.out.println(favoritesWithNull);


    }
}

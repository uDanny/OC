package ocp.collections;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * There are four formats for method references:
 * Static methods
 * Instance methods on a particular instance
 * Instance methods on an instance to be determined at runtime Constructors
 */
public class FunctionalCollectionsPracticeWithMethodReference {
    public static void main(String[] args) {
        Consumer<List<Integer>> methodRef1 = Collections::sort;
        Consumer<List<Integer>> lambda1 = l -> Collections.sort(l);
        methodRef1.accept(new ArrayList<>());

        String str = "abc";
        Predicate<String> methodRef2 = str::startsWith;
        Predicate<String> lambda2 = s -> str.startsWith(s);
        Predicate<String> lambda22 = s -> test(s);
        Predicate<String> lambda23 = FunctionalCollectionsPracticeWithMethodReference::test;
        BiPredicate<String, String> lambda233 = FunctionalCollectionsPracticeWithMethodReference::test;
        boolean isStringStartsWithO = methodRef2.test("o");


        Supplier<ArrayList<String>> methodRef4 = ArrayList::new;
        Supplier<ArrayList> methodRef42 = ArrayList<String>::new;
        Supplier<ArrayList> lambda4 = () -> new ArrayList<String>();
        ArrayList<String> strings = methodRef4.get();
        ArrayList arrayList = methodRef42.get();


        List<String> list = new ArrayList<>();
        list.add("Magician");
        list.add("Assistant");
        System.out.println(list); // [Magician, Assistant]
        list.removeIf(s -> s.startsWith("A"));
        System.out.println(list); // [Magician]


        List<Integer> list2 = Arrays.asList(1, 2, 3);
        list2.replaceAll(x -> x * 2);
        System.out.println(list); // [2, 4, 6]

        List<String> cats = Arrays.asList("i");
        cats.forEach(c -> System.out.println(c));
        cats.forEach(System.out::println);

        //
        Stream<SomeClass> stream = Stream.of(new SomeClass());
        //these lines are equivalent, but none of them are not compilable because ambiguous add static and nonstatic methods add in SomeClass
//        stream.collect(SomeClass::new, SomeClass::add, (strings1, c) -> strings1.add(c));
//        stream.collect(SomeClass::new, SomeClass::add, SomeClass::add);


    }

    private static boolean test(String s) {
        return s.startsWith(s);
    }

    private static boolean test(String s, String ss) {
        return s.startsWith(s);
    }
}

class SomeClass {

    public static SomeClass append(SomeClass someClass) {
        return null;
    }

    public static void append(ArrayList<Object> objects, SomeClass someClass) {

    }

    public void add(SomeClass e) {

    }

    public static void add(SomeClass e, SomeClass e2) {

    }
}

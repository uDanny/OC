package ocp.collections;

import java.util.*;

public class ListPractice {
    /**
     * void add(E element) - Adds element to end
     * void add(int index, E element) - Adds element at index and moves the rest toward the end
     * E get(int index) - Returns element at index
     * int indexOf(Object o) - Returns first matching index or -1 if not found
     * int lastIndexOf(Object o) - Returns last matching index or -1 if not found
     * void remove(int index) - Removes element at index and moves the rest toward the front
     * E set(int index, E e) - Replaces element at index and returns original
     */
    public static void main(String[] args) {
        //add
        List<String> list = new ArrayList<>();

        boolean a = list.add("a");//requires E, returns true
        boolean b = list.add("b");//requires E, returns true

        Set<String> set = new HashSet<>();

        System.out.println(set.add("A")); //true
        System.out.println(set.add("A")); //false

        //remove
        boolean a1 = list.remove("a");//requires Object, return true if found, false is not
        String remove = list.remove(0);//by index, returns E

        //isEmpty
        boolean empty = list.isEmpty();
        //size
        int size = list.size();

        //clear
        list.clear(); //void

        //contains
        boolean a2 = list.contains("A");// requires obj

        boolean c = list.add("c");//requires E, returns true

        iterators(list);

        //old
        Vector<String> strings = new Vector<>(); // ArrayList equivalent, but thread safe and old
        Stack<String> strings1 = new Stack<>(); //like deque, but old
        strings1.peek();
        strings1.pop();


    }

    private static void iterators(List<String> list) {
        Iterator iter = list.iterator();
        while (iter.hasNext()) {
            String string = (String) iter.next();
            System.out.println(string);
        }

        Iterator<String> iter2 = list.iterator();
        while (iter2.hasNext()) {
            String string = iter2.next();
            System.out.println(string);
        }

//        Iterator<String> iter3 = list.iterator();
//        String string;
//        while ((string = iter3.next()) != null) { //  throws exception
//            System.out.println(string);
//        }
    }

}

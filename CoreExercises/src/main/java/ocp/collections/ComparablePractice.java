package ocp.collections;

import javafx.collections.transformation.SortedList;

import java.util.*;

public class ComparablePractice {
    public static void main(String[] args) {

        SortedSet<Animal> animals = new TreeSet<>();
        animals.add(new Animal()); //throws exception if Animal doesn't implement Comparable
        //can use objects that doesn't extends comparable
        SortedSet<Animal> animalsWithComparator = new TreeSet<>((o1, o2) -> 0);

        List<Animal> list = new ArrayList<>();
        list.add(new Animal());
        Collections.sort(list); //no compile if it doesn't extend Comparable


        Comparator<Animal> animalComparator = new Comparator<Animal>() {
            @Override
            public int compare(Animal o1, Animal o2) {
                return o1.id - o2.id;
            }
        };
        Comparator<Animal> animalLambda = (o1, o2) -> o1.id - o2.id;
        Comparator<Animal> animalLambda2 = Comparator.comparingInt(o -> o.id);
        Comparator<Animal> animalLambda3 = Comparator.comparing(o -> o.id);
        animalLambda3 = animalLambda3.thenComparing(o -> o.id);
        Comparator<Animal> animalLambda4 = Comparator.reverseOrder();
        Collections.sort(list, animalComparator);
        list.add(null);

        Comparator<Animal> animalLambdaNulls = (o1, o2) -> o1 == null || o2 == null ? 1 :o1.id - o2.id;

        Collections.sort(list, animalLambdaNulls); //support comparing nulls if comparator is handling it

    }
}


class Animal implements Comparable<Animal> {
    Integer id;

    public int compareTo(Animal a) {
        return this.id - a.id;
    }
}

class LegacyDuck implements Comparable {
    private String name;

    public int compareTo(Object obj) { //because no generics
        LegacyDuck d = (LegacyDuck) obj;
        return name.compareTo(d.name);
    }
}

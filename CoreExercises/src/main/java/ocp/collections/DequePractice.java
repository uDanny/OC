package ocp.collections;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequePractice {
    public static void main(String[] args) {
        //todo: add pollFirst and last

        //like LinkedList
        Deque<String> d = new ArrayDeque<>();
        //no adding null values

        //add at the end
        boolean a = d.add("a"); //true, at the end
        boolean a2 = d.add("b"); //true
        boolean a3 = d.add("a"); //true

        boolean c = d.offer("c"); //true, at the end
        boolean c1 = d.offer("c"); //true

        //add in front
        d.push("x"); //void, in the front, before a

        //returns from front
        String element = d.element(); //x
//        String element2 = new ArrayDeque<String>().element(); //throws exception

        //returns from front
        String peek = d.peek(); //x, from the front
        String peek2 = new ArrayDeque<String>().peek(); //null

        //remove and returns from front
        String remove = d.remove(); //x removes only first found
//        String remove2 = new ArrayDeque<String>().remove(); //noSuchElementException

        //remove and returns from the front
        String pop = d.pop();
//        String pop2 = new ArrayDeque<String>().pop(); //noSuchElementException

        //remove and returns from front
        String poll = d.poll(); //x, from the front
        String poll2 = new ArrayDeque<String>().poll(); //null



        int i = 0;

        //todo: play with Stack
    }
}

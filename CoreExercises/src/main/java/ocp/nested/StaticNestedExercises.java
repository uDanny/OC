package ocp.nested;

/**
 * It is like a regular class except for the following:
 * The nesting creates a namespace because the enclosing class name must be used to refer to it.
 * It can be made private or use one of the other access modifiers to encapsulate it.
 * The enclosing class can refer to the fields and methods of the static nested class.
 * could be imported static like : import static bird.Toucan.Beak;
 */
//todo: exercise with importing
public class StaticNestedExercises {
    int i = 0;
    static int si = 0;
    static int si2 = 0;

    static class SomeStaticClass { //can extend a class
        int instanceVar;
        static int si = 0;


        public SomeStaticClass(int instanceVar) {
            this.instanceVar = instanceVar;
        }

        void someMethod() {
//            System.out.println(i);
            System.out.println(instanceVar);
            System.out.println(si);
            System.out.println(this.si); //example we can access static by this
            System.out.println(StaticNestedExercises.si);
            System.out.println(si2);
//            System.out.println(StaticNestedExercises.this.si);
            }

        //can declare static methods
        static void  someStaticMethod() {
//            System.out.println(instanceVar);
            System.out.println(si);
//            System.out.println(this.si); //example we can access static by this
            System.out.println(StaticNestedExercises.si);
//            System.out.println(StaticNestedExercises.this.si);
        }
        class InnerLvl2{}
        static class InnerLvl2Static{}

    }
}


class Enclosing {
    static class Nested {
        private int price = 6;
    }

    public static void main(String[] args) {
        Nested nested = new Nested();
        System.out.println(nested.price);


        StaticNestedExercises.SomeStaticClass someStaticClass = new StaticNestedExercises.SomeStaticClass(0);
        someStaticClass.someMethod();
        someStaticClass.someStaticMethod();
        StaticNestedExercises.SomeStaticClass.someStaticMethod();




        StaticNestedExercises staticNestedExercises = new StaticNestedExercises();
//        staticNestedExercises.new SomeStaticClass();


    }
}

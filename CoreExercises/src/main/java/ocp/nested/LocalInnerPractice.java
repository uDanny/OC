package ocp.nested;

public class LocalInnerPractice {

    public static void main(String[] args) {

    }

    //final practice
    public void isItFinal() {
        int one = 20;
        int two = one; //no final
        two++;
        int three; //final because is assigned only once, even if inside branches
        if (one == 4) three = 3;
        else three = 4;
        int four = 4; // not final, assigned  twice
        class Inner {

            void a() {
                System.out.println(three);
                System.out.println(one);
//            System.out.println(two);
//            System.out.println(four);
            }
        }
        four = 5;
    }
}

/**
 * They do not have an access specifier.
 * They cannot be declared static and cannot declare static fields or methods.
 * They have access to all fields and methods of the enclosing class.
 * They do not have access to local variables of a method unless those variables are final or effectively final. More on this shortly.
 */
class Outer {
    private int length = 5;
    private static int lengthS = 5;

    public void calculate() {
        final int width = 20;
        int width2 = 20;
        int width3;
        width3 = 20;
        length = 5;
//        Inner inner = new Inner();

        StringBuilder s = new StringBuilder("a");
        s.append("a");
//        s = new StringBuilder(); //wont be accessible from Inner

        class Inner { //can extend a class, can be declared in static method as well
            int a = 0;

            {
                a = 2;
            }

            final static int a2 = 0;
            //            static int a2 = 0; //no static
//            static{
//
//            }
            static final int a3 = 0;
            public void multiply() {
                System.out.println(length * width); //width should be final
                System.out.println(width2); //technically, compile treat it as final
                System.out.println(width3); //also final
                System.out.println(lengthS); //also final
                System.out.println(a3); //static final, cannot have only static or static blocks
//                System.out.println(postFactum); should be defined first
            }

            public void otherMethod() {
                s.append("a");
            }
        }
        int postFactum = 2;
        Inner inner = new Inner();
        inner.multiply();
        inner.otherMethod();
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.calculate();
    }
}





package ocp.nested;

public class MemberInnerPractice {

    public static void main(String[] args) {
        new Outer2("fs").new Inner(); //creates Outer$Inner.class
    }
}

class Outer2 {
    private String s;
    private final String fs;
    private static String ss;

    public Outer2(String fs) {
        this.fs = fs;
        new Inner(); //can be called at in constructor
    }

    void callInner() {
        Inner inner = new Inner();
    }


    /**
     * member inner
     * public, private, or protected or use default access
     * extend any class and implement interfaces
     * Can be abstract or final
     * Cannot declare static fields or methods
     * Can access members of the outer class including private members
     */
    class Inner { //, because non static

        private String s;
        static final int a = 2;
//        static int a2 = 2;
//        static {
//
//}

        //        private static String ss;
        void method() {
            ss = fs;

        }
//        static void staticMethod() {
//            ss = fs;
//
//        }
//        static class StaticClass{}

    }

    interface InnerI {
        int i = 0;
    }


    private class InnerPeace {
        private String reason = "none";
    }

    void m() {
        InnerPeace i = new InnerPeace();
        System.out.println(i.reason); //access private member
    }

}


class A {
    private int x = 10;

    class B {
        private int x = 20;

        class C {
            private int x = 30;

            public void allTheX() {
                System.out.println(x); //30
                System.out.println(this.x); //30
                System.out.println(B.this.x); //20
                System.out.println(A.this.x); //10

            }
        }
    }

    public static void main(String[] args) {
        A a = new A();
        A.B b = a.new B();
        A.B.C c = b.new C();
        c.allTheX();
    }
}

class CaseOfThePrivateInterface {
    private interface Secret { //yes, private interface
        public void shh();
    }

    class DontTell implements Secret {
        public void shh() {
        }
    }
}

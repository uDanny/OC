package ocp.nested;

/**
 * anonymous inner class is just an unnamed local inner class.
 */
public class AnonymousExercise {
    public static void main(String[] args) {

    }
}

//it could be an interface as well
class AnonInner {
    abstract class SaleTodayOnly {
        abstract int dollarsOff();
    }

    public int admission(int basePrice) {
        SaleTodayOnly sale = new SaleTodayOnly() {
//            public SaleTodayOnly(){ } cannot set constructor
            @Override
            int dollarsOff() {
                return 3;
            }
        };
        return basePrice - sale.dollarsOff();
    }
}

//-----
class AnonInner1 {
    interface SaleTodayOnly {
        int dollarsOff();
    }

    public int pay() {
        int o = 9; //also works with static
        int admission = admission(5, new SaleTodayOnly() {
            final static int ai = 2;
              int a2i = 2;
            public int dollarsOff() {
//                o = 2; is not effectively final
                return o;
            }
        });
        System.out.println(o);
        return admission;
    }

    public int admission(int basePrice, SaleTodayOnly sale) {
        return basePrice - sale.dollarsOff();
    }
}

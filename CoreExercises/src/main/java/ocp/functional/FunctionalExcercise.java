package ocp.functional;

import java.util.function.Predicate;

public class FunctionalExcercise {
    public static void main(String[] args) {

        FunctionalExcercise f = new FunctionalExcercise();
//        f.someClass(()-> {defPrint()});

    }

    void someClass(F f){
        f.print();
    }
    private static void print(String animal, Predicate trait) {
        if(trait.test(animal)) System.out.println(animal);
    }
}
@FunctionalInterface
interface F{
    void print();
    default void defPrint(){
        System.out.println("fron def");
    }
}



package ocp.functional;

import java.util.function.*;

public class FunctionalPractice {
    public static void main(String[] args) {

        Supplier<String> s = () -> "newValue";
        String s1 = s.get();


        Consumer<String> c = (String x) -> System.out.println(x);
        c.accept("string");

        BiConsumer<String, Integer> bc = (x, y) -> System.out.println(x + y);
        bc.accept("string", 2);


        Predicate<String> p = (String x) -> x != null;
        boolean a1 = p.test("a");

        BiPredicate<String, Integer> bp = (x, y) -> x + y != null;
        boolean a = bp.test("A", 2);

        Function<String, Integer> f = (String x) -> Integer.valueOf(x);
        Integer apply1 = f.apply("2");


        BiFunction<String, Integer, Integer> bf = (x, y) -> Integer.valueOf(x) + y;
        Integer apply = bf.apply("2", 3);


        UnaryOperator<String> u = (String x) -> x + "a";
        String u1 = u.apply("u");

        BinaryOperator<String> bo = (x, y) -> x + y;
        bo.apply("a", "b");
    }
}

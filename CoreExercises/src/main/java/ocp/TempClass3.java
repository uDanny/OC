package ocp;

import com.sun.org.apache.xerces.internal.xs.StringList;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TempClass3 {
    //review ? super ..  get object?
    //todo: review tricky hierarchy package access
    //todo: Files.newBufferReader
    //Q7 savePoint
    //supplier.getAsDouble
    //atomic get
    //order of aa, a, a1
    //Q27
    //Q33 comparing by method(book::getName)
    //Collectors.tomap vs toMap
    //Stream of multiple members
    //for(;;);
    //NavigableMap
    //ReadWriteLock
    //collectors to map duplicate review one more time
    //PrintWriter
    //.foreachOrdered
    //Math round, in which direction -> to up, check with  negative cases
    //tomap, groupby, groupbyboolean
    //todo: where both path should be relative or both absolute - in relativize, and resolve (recheck)


    public static void main(String[] args) throws SQLException, IOException {


        Path p1 = Paths.get("c:\\temp\\test.txt");
        Path p2 = Paths.get("c:\\temp\\report.pdf");
        System.out.println(p1.resolve(p2)); //c:\temp\report.pdf When the argument to resolve starts with the root (such as c: or, on *nix, a /), the result is same as the argument.

        //

        try {
            Connection c = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
            try (Statement stmt = c.createStatement();) {
                ResultSet rs = stmt.executeQuery("select * from STUDENT");
                ResultSetMetaData rsmd = rs.getMetaData();
                int cc = rsmd.getColumnCount();
                while (rs.next()) {
                    for (int i = 0; i < cc; i++) { // should be from 0
                        System.out.print(rsmd.getColumnName(i) + " = " + rs.getObject(i + 1) + ", ");
                    }
                    System.out.println();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }

        //


        try (Connection c = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");) {
            c.setAutoCommit(false);
            Statement stmt = c.createStatement();
            int a1 = stmt.executeUpdate("insert into STUDENT2 values (1, 'aaa', 1.1)");
            Savepoint sp1 = c.setSavepoint();
            int a2 = stmt.executeUpdate("insert into STUDENT2 values (2, 'bbb', 2.1)");
            c.rollback(); //savepoint is ignored, because not passed as an param, so it is all rollbacked
//            c.rollback(sp1); //should be  like this
            int a3 = stmt.executeUpdate("insert into STUDENT2 values (3, 'ccc', 3.1)"); //transaction created
            c.setAutoCommit(true); //row inseted
            int a4 = stmt.executeUpdate("insert into STUDENT2 values (4, 'ddd', 4.1)"); //auto inserted
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //

        try {
            Files.isSameFile(null, null); // checks paths, not files itself

        } catch (Exception e) {
        }


        //

        boolean b = new AtomicInteger(9).compareAndSet(8, 2);// remains 9, returns false


        //

        Stream.of("a", "aa", "0", "A").sorted().forEach(System.out::println); // 0, A, a, aa

        //
        try {
            Properties p = new Properties();
            p.setProperty("user", "user");
            p.setProperty("password", "password");
            Connection c = DriverManager.getConnection("url", p);

        } catch (Exception e) {
        }

        //
        Map<String, Boolean> collect3 = Stream.of("a", "bb", "ccc").collect(Collectors.toMap(x -> x, y -> y.length() % 2 == 0));
        Map<String, Boolean> collect4 = Stream.of("a", "bb", "ccc").collect(Collectors.toMap(x -> x, y -> y.length() % 2 == 0, (x, y) -> false));
        TreeMap<String, Boolean> collect5 = Stream.of("a", "bb", "ccc").collect(Collectors.toMap(x -> x, y -> y.length() % 2 == 0, (x, y) -> false, TreeMap::new));


        Map<Boolean, List<String>> collect = Stream.of("a", "bb", "ccc").collect(Collectors.groupingBy(y -> y.length() % 2 == 0));
        Map<Boolean, Set<String>> collect1 = Stream.of("a", "bb", "ccc").collect(Collectors.groupingBy(y -> y.length() % 2 == 0, Collectors.toSet()));
        TreeMap<Boolean, Set<String>> collect2 = Stream.of("a", "bb", "ccc").collect(Collectors.groupingBy(y -> y.length() % 2 == 0, TreeMap::new, Collectors.toSet()));

        Set<String> collect6 = Stream.of("a", "a").collect(Collectors.toSet()); //just overrides
        //
        class A {
        }

        List<A> as = Arrays.asList(new A());
        List as2 = Arrays.asList(new A());
//        Collections.sort(as); //noCompile
//        Collections.sort(as2); //ClassCastException
        as.stream().sorted(); //no exception, just ignore

        //

//        for (;;); //infinite loop

//        List<Object> a = new ArrayList<String>(); //no compile

        //atomic move doesn't throw exception if file exists

        //
        NavigableMap<String, String> navigableMap = new TreeMap<>();
        navigableMap.put("a", "a");
        navigableMap.put("b", "b");
        navigableMap.put("c", "c");

        Map.Entry<String, String> stringStringEntry = navigableMap.pollFirstEntry(); //returns and removes from map a, null if empty set
        Map.Entry<String, String> stringStringEntry1 = navigableMap.pollLastEntry(); //returns and removes from map c, null if empty set


        NavigableMap<String, String> navigableMap1 = navigableMap.tailMap("b", true);//returns part of map, where value is greater or equal to b

        //
        //if one thread is reading, other threads can read, but no thread can write. If one thread is writing, no other thread can read or write.
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        readWriteLock.readLock().lock();


        //
        double random = Math.random(); // 0 to 1
        int i = ThreadLocalRandom.current().nextInt(1, 11); // 1 to 10

        //

        List<?> l = new ArrayList<String>(Arrays.asList("a", "b"));
        List<?> l22 = new ArrayList(Arrays.asList("a", "b"));
        Object o = l.get(0);
        List<? extends Object> l2 = new ArrayList<String>();
        List<? extends List> l3 = new ArrayList<List>();
        List<? extends List> l4 = new ArrayList<ArrayList>();
        List<? extends List> l5 = new ArrayList<ArrayList>();
        List<? super List> l6 = new ArrayList<List>();
        List<? super ArrayList> l7 = new ArrayList<List>();
        List<? super ArrayList> l8 = new ArrayList<Object>();

try {

    Paths.get("c:\\code\\java\\PathTest.java");
    p1.getName(0); // is code
    p1.getName(1); // is java
    p1.getName(2); //is PathTest.
//         p1.getName(3)  //will cause IllegalArgumentException to be thrown.
         }catch (Exception e){}


        assert true;
        assert false : "kk";

        EnumA.A.ordinal(); //not index
        EnumA.A.name(); //not index

        try {

            Stream<String> lines = Files.lines(Paths.get("test.txt"));
            lines.forEach(System.out::println);
            Stream<String> lines2 = Files.lines(Paths.get("test.txt"), Charset.defaultCharset());
            lines2.forEach(s -> System.out.println(s));

        } catch (Exception e){}      //

        p1.resolveSibling("text2.txt"); // returns abs, path from the same parent folder as p1

        //

        System.out.println(Math.round(3.5)); //4
        System.out.println(Math.round(-3.5)); //-3

        //

        Comparator<String> compareTo = String::compareTo;
        Stream.of("a","b","c").sorted(compareTo.thenComparing(String::length));
        Stream.of("a","b","c").sorted(Comparator.comparing(String::length));


        //
        Stream.of("a","b","c").parallel().forEachOrdered(System.out::println);


        Stream<String> lines1 = Files.lines(Paths.get("/"));
        Stream<String> lines = Files.lines(Paths.get("/"),  Charset.defaultCharset());
        List<String> list1 = Files.readAllLines(Paths.get("/"));
        List<String> list = Files.readAllLines(Paths.get("/"),  Charset.defaultCharset());
        BufferedReader bufferedReader = Files.newBufferedReader(Paths.get("/"));
        BufferedReader bufferedReader2 = Files.newBufferedReader(Paths.get("/"), Charset.defaultCharset());
        BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get("/"));
        BufferedWriter bufferedWriter2 = Files.newBufferedWriter(Paths.get("/"), Charset.defaultCharset());
        InputStream inputStream = Files.newInputStream(Paths.get("/"));
        OutputStream outputStream = Files.newOutputStream(Paths.get("/"));

    }

    public void usePrintWriter(PrintWriter pw) {

        boolean bval = true;
        pw.print(bval); // .write() cant wtite boolean
    }

    enum EnumA implements Serializable { //cant be inner, can extends interfaces
        A;
//        @Override
//        public A clone() {return null;} //in enums, clone method is final
    }


}

class Student {
    private Map<String, Integer> marksObtained = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void setMarksInSubject(String subject, Integer marks) {
        lock.writeLock().lock(); //1
        try {
            marksObtained.put(subject, marks);
        } finally {
            lock.writeLock().unlock(); //2
        }
    }

    public double getAverageMarks() {
        lock.readLock().lock(); //3
        double sum = 0.0;
        try {
            for (Integer mark : marksObtained.values()) {
                sum = sum + mark;
            }
            return sum / marksObtained.size();
        } finally {
            lock.readLock().unlock();//4
        }
    }

    public static void main(String[] args) {

        final Student s = new Student();

        //create one thread that keeps adding marks
        new Thread() {
            public void run() {
                int x = 0;
                while (true) {
                    int m = (int) (Math.random() * 100);
                    s.setMarksInSubject("Sub " + x, m);
                    x++;
                }
            }
        }.start();

        //create 5 threads that get average marks
        for (int i = 0; i < 5; i++) {
            new Thread() {
                public void run() {
                    while (true) {
                        double av = s.getAverageMarks();
                        System.out.println(av);
                    }
                }
            }.start();
        }
    }

}



package ocp;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TempClass4 {
    public static void main(String[] args) {
//Enumeration
//todo: defaultlocale
//todo: short review string
//todo: numberformat and dateformat locales revie
//todo: Console.io direct interact methods
//todo: final static review (in inner classes)

        try {
            ResultSet rs = null;
            rs.getInt(0);
            rs.getInt("columnLabel");
            rs.getString(0);
            rs.getString("columnLabel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //RowSet ??


        try {
            BufferedWriter br = null;
            br.append("a");
            br.append('a'); // works like write
            br.close(); //includes flush as well
        } catch (Exception e) {
            e.printStackTrace();
        }

        //
//        Enumeration<String> e
//        Arrays.asList()

        Stream<Integer> stream = Stream.of(10);
        stream.collect(Collectors.partitioningBy(z -> z < 10, Collectors.counting())).values().forEach(obj -> System.out.print(obj));

        LocalDateTime.ofInstant(Instant.now(), ZoneId.of("GMT+2"));

        //
        try {
            ResourceBundle rs = null;
            rs.getString("a");
        } catch (Exception e) {
        }

        //

        List<String> ls = Arrays.asList("Tom Cruise", "Tom Hart", "Tom Hanks", "Tom Brady");
        Predicate<String> p = str -> {
            System.out.println("Looking...");
            return str.indexOf("Tom") > -1;
        };
        boolean flag = ls.stream().filter(str -> str.length() > 8).allMatch(p);
        //

        try (FileOutputStream outputStream = new FileOutputStream("ss");) {
//             outputStream = new FileOutputStream("ss"); //try-resources are final

        } catch (Exception e) {
        }

        TreeSet<String> strings = new TreeSet<>();
        strings.add("a");
        strings.add("b");
        strings.add("c");
        System.out.println(strings.subSet("b", "c")); // [b]
        NavigableSet<String> x = strings.subSet("b", true, "c", true);
        System.out.println(x); //[b, c]
        x.remove("c"); //subset is backed by original set, and it deletes from original as well, works in the same manner with inserts
        x.add("b0"); //key is in range of subset na is added successfully
//        x.add("d");// key is out of range, IllegalArgumentException
        System.out.println(strings); //[a, b, b0]

        //

        ArrayList<String> strings1 = new ArrayList<>();
        strings1.add("a");
        strings1.add("b");
        strings1.add("c");

        List<String> list = strings1.subList(0, 2);
        list.remove(0); //also removes from parrent list
        list.add("a"); //also adds into parent list

        String[] as = "aa".split("a");

        //


        try {
            Path p1 = Paths.get("c:\\company\\records\\customers.dat");
//            Path p2 = p1.resolveSibling("clients.dat");
            Path p2 = Paths.get("c:", p1.subpath(0, 2).toString(), "clients.dat");
        } catch (Exception e) {
        }

        //

        try {
            ObjectInputStream in = null;
            in.defaultReadObject(); //To serialize the object using the default behavior, you must call objectOutputStream.defaultWriteObject(); or objectOutputStream.writeFields();. This will ensure that instance fields of Portfolio object are serialized.
            // To deserialize the object using the default behavior, you must call objectInputStream.defaultReadObject(); or objectInputStream.readFields();. This will ensure that instance fields of Portfolio object are deserialized.

            String s = (String) in.readObject();
            double v = in.readDouble();
        } catch (Exception e) {
        }

        ///
        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap();
        concurrentHashMap.putIfAbsent("a", "aa");

        //
        Instant parse = Instant.parse("2015-06-26T02:43:30Z");

        //
        try {
            PrintWriter pw = new PrintWriter("/");
            pw.write("aa"); //return void, and no exception thrown
            pw.printf("hh").print("success");
            pw.println("hh"); //return void
            if (pw.checkError()){
                System.out.println("error");
            }; //no exception thrown
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        "k".toUpperCase(new Locale("en", "US"));

    }

    enum Pets {

        DOG("D"), CAT("C"), //fields should be declared first

        FISH("F");
        static String prefix = "I am ";
        String name;

        Pets(String s) {
//            name = prefix + s; //illegat to access static fields in constructor
        }

        public String getData() {
            return name + prefix;
        }
    }

    static enum A {}

    public void assertTest(List v) {
        assert v != null : v = new ArrayList();//Valid because <boolean_expression> : <any_expression_but_void> is satisfied., attribution but not void  method }


    }

    interface Measurement {

        public default int getLength() {
            return 0;
        }

        public static int getBreadth() {
            return 0;
        }
    }

    interface Size extends Measurement {
        public static final int UNIT = 100;

//    public static int getLength() {
//        return 10;
//    }
    }

    class RepeatA {
        RepeatA a(RepeatA a) {
            return a;
        }
    }

    class RepeatB extends RepeatA {
        @Override
        RepeatB a(RepeatA a) { //no RepeatB param
            return null;
        }
    }
}

;

class Data implements java.io.Serializable {
    public static String f1; //not serializable because static
    public static transient int f2; //skipped because transient
    public transient boolean f3;//not serializable because static
    public final static String f4 = "4"; //no serialized but initialized with the same value
    public String f5 = "5"; //serialized

    public static void main(String[] args) {
        Data d = new Data();
        d.f1 = "f1";
        d.f2 = 2;
        d.f3 = true;
        //only f4, f5 are equivalent across jvms
    }
}

class PathTest {
    static Path p1 = Paths.get("c:\\a\\b\\c");

    public static String getValue() {
        String x = p1.getName(1).toString();
        String y = p1.subpath(1, 2).toString();
        return x + " : " + y;
    }

    public static void main(String[] args) {
        System.out.println(getValue()); // b:b
    }
}

class Test extends Thread {
    static int x, y;
    String s;

    public Test(String s) {
        this.s = s;
    }

    public synchronized void run() {
        for (; ; ) {
            x++;
            y++;
            System.out.println(s + " " + x + " " + y);
        }
    }

    public static void main(String[] args) {
        new Test("A").start();
        new Test("A").start();
        new Test("B").start();
    }
}


class TestClass2 {
    public static void main(String[] args) {
        TreeSet<Integer> s = new TreeSet<Integer>();
        TreeSet<Integer> subs = new TreeSet<Integer>();
        for (int i = 324; i <= 328; i++) {
            s.add(i);
        }
        subs = (TreeSet) s.subSet(326, true, 328, true);
        subs.add(329); //key is out of range
        System.out.println(s + " " + subs);
    }
}


class TestOuter {
    public void myOuterMethod() {
        new TestInner();
        new TestOuter().new TestInner();
    }

    public class TestInner {
    }

    public static void main(String[] args) {
        TestOuter to = new TestOuter();
        to.new TestInner();
    }
}

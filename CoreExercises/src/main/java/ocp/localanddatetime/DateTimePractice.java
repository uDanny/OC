package ocp.localanddatetime;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class DateTimePractice {

    public static void main(String[] args) {

        LocalTime time1 = LocalTime.of(6, 15);
        LocalTime time2 = LocalTime.of(6, 15, 30);
        LocalTime time3 = LocalTime.of(6, 15, 30, 200);


        LocalDate date1 = LocalDate.of(2015, Month.JANUARY, 20);
        LocalDate date2 = LocalDate.of(2015, 1, 20);


        LocalDateTime dateTime1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6, 15, 30);
        LocalDateTime dateTime2 = LocalDateTime.of(2015, Month.JANUARY, 20, 6, 15, 30, 15);
        LocalDateTime dateTime3 = LocalDateTime.of(date1, time1);

        ZoneId zone = ZoneId.of("US/Eastern");
        ZonedDateTime zoned1 = ZonedDateTime.of(2015, 1, 20,
                6, 15, 30, 200, zone);
        ZonedDateTime zoned2 = ZonedDateTime.of(date1, time1, zone);
        ZonedDateTime zoned3 = ZonedDateTime.of(dateTime1, zone);


        ZoneId.getAvailableZoneIds().stream()
                .filter(z -> z.contains("US") || z.contains("America")).sorted().forEach(System.out::println);


        date2.toEpochDay();
        dateTime2.toEpochSecond(ZoneOffset.UTC);


        //Period

        Period annually = Period.ofYears(1); // every 1 year
        Period quarterly = Period.ofMonths(3); // every 3 months
        Period everyThreeWeeks = Period.ofWeeks(3); // every 3 weeks
        Period everyOtherDay = Period.ofDays(2); // every 2 days
        Period everyYearAndAWeek = Period.of(1, 0, 7); // every year, 0 months and 7 days

        System.out.println(everyYearAndAWeek); //P1Y7D
        System.out.println(Period.ofWeeks(6)); //P42D

        //Duration

        Duration daily = Duration.ofDays(1); // PT24H
        Duration hourly = Duration.ofHours(1); // PT1H
        Duration everyMinute = Duration.ofMinutes(1); // PT1M
        Duration everyTenSeconds = Duration.ofSeconds(10); // PT10S
        Duration everyMilli = Duration.ofMillis(1); // PT0.001S
        Duration everyNano = Duration.ofNanos(1); // PT0.000000001S


        Duration daily2 = Duration.of(1, ChronoUnit.DAYS);
        Duration halfDaily2 = Duration.of(1, ChronoUnit.HALF_DAYS);
        Duration hourly2 = Duration.of(1, ChronoUnit.HOURS);
        Duration everyMinute2 = Duration.of(1, ChronoUnit.MINUTES);
        Duration everyTenSeconds2 = Duration.of(10, ChronoUnit.SECONDS);
        Duration everyMilli2 = Duration.of(1, ChronoUnit.MILLIS);
        Duration everyNano2 = Duration.of(1, ChronoUnit.NANOS);


        LocalTime one = LocalTime.of(5, 15);
        LocalTime two = LocalTime.of(6, 30);
        LocalDate date = LocalDate.of(2016, 1, 20);
        System.out.println(ChronoUnit.HOURS.between(one, two)); // 1
        System.out.println(ChronoUnit.MINUTES.between(one, two)); // 75
//        System.out.println(ChronoUnit.MINUTES.between(one, date)); // DateTimeException


        Instant now = Instant.now(); // do something time consuming
        Instant later = Instant.now();
        Duration duration = Duration.between(now, later);
        System.out.println(duration.toMillis());
        System.out.println(Duration.between(now, later)); //will be printed with minus, like  PT-1..


        LocalDate date3 = LocalDate.of(2015, 5, 25);
        LocalTime time33 = LocalTime.of(11, 55, 0);
        ZoneId zone3 = ZoneId.of("US/Eastern");
        ZonedDateTime zonedDateTime3 = ZonedDateTime.of(date3, time33, zone3);
        Instant instant3 = zonedDateTime3.toInstant(); // 2015–05–25T15:55:00Z
        System.out.println(zonedDateTime3); // 2015–05–25T11:55–04:00[US/Eastern]
        System.out.println(instant3); // 2015–05–25T15:55:00Z


        Instant instant = Instant.ofEpochSecond(2);
        System.out.println(instant); // 2015–05–25T15:55:00Z


        Instant nextDay = instant.plus(1, ChronoUnit.DAYS);
        System.out.println(nextDay); // 2015–05–26T15:55:00Z
        Instant nextHour = instant.plus(1, ChronoUnit.HOURS);
        System.out.println(nextHour); // 2015–05–25T16:55:00Z
        instant.plus(Duration.of(2, ChronoUnit.DAYS));
        //        Instant nextWeek = instant.plus(1, ChronoUnit.WEEKS); // exception, it allows operations with day or smaller units of time
//        instant.plus(Duration.of(2, ChronoUnit.WEEKS)); //exception
//        instant.plus(duration); // exception



    }
}

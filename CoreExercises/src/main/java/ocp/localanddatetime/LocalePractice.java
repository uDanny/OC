package ocp.localanddatetime;

import java.util.*;

public class LocalePractice {

    public static void localization() {

        Locale locale = Locale.getDefault();
        System.out.println(locale);
        System.out.println(Locale.GERMAN); // de
        System.out.println(Locale.GERMANY); // de_DE


        System.out.println(new Locale("fr")); // fr
        System.out.println(new Locale("hi", "IN")); // hi_IN

        Locale l1 = new Locale.Builder()
                .setLanguage("en")
                .setRegion("US")
                .build();
        Locale l2 = new Locale.Builder()
                .setRegion("US")
                .setLanguage("en")
                .build();

        System.out.println(Locale.getDefault()); // en_US
        Locale locale2 = new Locale("fr");
        Locale.setDefault(locale2); // change the default
        System.out.println(Locale.getDefault()); // fr
    }

    public static void main(String[] args) {
        Locale us = new Locale("en", "US");
        Locale france = new Locale("fr", "FR");
        printProperties(us);
        System.out.println();
        printProperties(france);
    }

    public static void RbPractice() {

        Locale us = new Locale("en", "US");
        ResourceBundle rb = ResourceBundle.getBundle("Zoo", us);
        Set<String> keys = rb.keySet();
        keys.stream().map(k -> k + " " + rb.getString(k)).forEach(System.out::println);


        Properties props = new Properties();
        rb.keySet().stream()
                .forEach(k -> props.put(k, rb.getString(k)));


        String notReallyAProperty1 = props.getProperty("notReallyAProperty");//null, because doesn't exist
        System.out.println(notReallyAProperty1);
        System.out.println(props.getProperty("notReallyAProperty", "123")); //123, because it's default

        Object notReallyAProperty = props.get("notReallyAProperty");
        System.out.println(notReallyAProperty); //null
    }

    public static void printProperties(Locale locale) {
        ResourceBundle rb = ResourceBundle.getBundle("Zoo", locale);
        System.out.println(rb.getString("hello"));
        System.out.println(rb.getString("open"));


        Enumeration<String> keys = rb.getKeys();
        while (keys.hasMoreElements()){
            String s = keys.nextElement();

        }

    }


}


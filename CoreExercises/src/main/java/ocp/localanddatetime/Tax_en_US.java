package ocp.localanddatetime;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class Tax_en_US extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return new Object[][]{{"tax", new UsTaxCode()}};
    }

    public static void main(String[] args) {

        new Tax_en_US().someMethod();


    }

    void someMethod() {
        ResourceBundle rb = ResourceBundle.getBundle("ocp.datetime.Tax", Locale.US); //package resourcebundles;
        System.out.println(rb.getObject("tax"));


        String tax = ((UsTaxCode) rb.getObject("tax")).helloByName;
        System.out.println(java.text.MessageFormat.format(tax, "name"));
    }


}

class UsTaxCode {
    public String helloByName = "Hello, {0}";
}

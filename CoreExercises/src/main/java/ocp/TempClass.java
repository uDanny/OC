package ocp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.*;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class TempClass {
    public static void main(String[] args) throws IOException, SQLException {
        //check SCJP threads stuff
        //concurrent collections

        //review volatile
        //review comparator comparing
        //when we should Load the driver class using Class.forName and when not
        //static inner enum ?? - default is static
        //review generics, not in methods, but in creation and ussage itself
        //all match, nonematch
        //files, copyOptions
        //period, duration short review
        //question 31, concurency cyclic barrier
        //review interface, class multiple inheritance conflict scenarios
        //review copying files io
        //try with resources with no body? - NO
        //review static calls to super vars
        //collection to map stream
        //localepractice review
        //small compute review

        method();


    }


    public static final void method() throws IOException, SQLException {

        List list = Arrays.asList("a, b");
        Map<String, List<String>> map = new HashMap<>();
        map.put("", list);


        List<String> list1 = Arrays.asList("a", "bc", "abc");
        list1.stream().sorted();
        //or
        list1.stream().sorted((x, y) -> x.compareTo(y));
        list1.stream().sorted(Comparator.comparing(x -> x));
        list1.stream().sorted(Comparator.comparingInt(x -> x.length()));

        //functions for primitives

        IntSupplier is = () -> 1;
        int asInt = is.getAsInt();
        LongSupplier ls = () -> (long) 1;
        DoubleSupplier ds = () -> 0.2;

        IntConsumer ic = (x) -> System.out.println(x);
        LongConsumer lc = (x) -> System.out.println(x);
        DoubleConsumer da = (x) -> System.out.println(x);

        IntPredicate ip = (x) -> x < 0;
        LongPredicate lp = (x) -> x < 0;
        DoublePredicate dp = (x) -> x < 0;

        IntFunction<String> iF = x -> String.valueOf(x);
        LongFunction<String> lf = x -> String.valueOf(x);
        DoubleFunction<String> df = x -> String.valueOf(x);

        IntUnaryOperator iuo = (x) -> x + 1;
        LongUnaryOperator luo = (x) -> x + 1;
        DoubleUnaryOperator duo = (x) -> x + 1;

        DoubleBinaryOperator dbo = (x, y) -> x + y;
        IntBinaryOperator ibo = (x, y) -> x + y;
        LongBinaryOperator lbo = (x, y) -> x + y;

        AtomicInteger ai = new AtomicInteger(2);
        AtomicLong al = new AtomicLong(2);
        AtomicBoolean ab = new AtomicBoolean(true);

        al.addAndGet(1);
        al.incrementAndGet();
        al.decrementAndGet();

        Double d = null;
        DoubleSupplier doubleSupplier = () -> d; //d == null
//        System.out.println(d.doubleValue()); //nullPointer

        List<Integer> integers = Arrays.asList(1, 2);
        boolean bTrue = integers.stream().distinct().anyMatch(x -> x == 2);
        boolean bTrue2 = integers.stream().distinct().noneMatch(x -> x == 3);


        Queue<String> container = new LinkedList<>();
        container.peek();
        container.element();
        container.poll();
        container.remove();
        container.add("a");
        container.offer("a");

        Deque<String> dequeue = new LinkedList<>();
        dequeue.peek();
        dequeue.peekFirst();
        dequeue.peekLast();
        dequeue.element();
        dequeue.poll();
        dequeue.pollFirst();
        dequeue.pollLast();
        dequeue.remove();
        dequeue.removeFirst();
        dequeue.removeLast();
        dequeue.pop();
        dequeue.offer("a");
        dequeue.offerFirst("a");
        dequeue.offerLast("a");
        dequeue.add("a");
        dequeue.addFirst("a");
        dequeue.addLast("a");


        Queue<String> container2 = new PriorityQueue<>(Comparator.naturalOrder());


        Path p1 = Paths.get("c:\\temp\\test1.txt");
        Path p2 = Paths.get("c:\\temp\\test2.txt");
//        Which of the following code fragments moves the file test1.txt to test2.txt, even if test2.txt exists?
        Files.move(p1, p2);  //This will throw a java.nio.file.FileAlreadyExistsException if the file already exists.
        Files.move(p1, p2, StandardCopyOption.REPLACE_EXISTING);  //it works, default is NOFOLLOW_LINKS

//        REPLACE_EXISTING     If the target file exists, then the target file is replaced if it is not a non-empty directory. If the target file exists and is a symbolic link, then the symbolic link itself, not the target of the link, is replaced.
//        COPY_ATTRIBUTES     Attempts to copy the file attributes associated with this file to the target file. The exact file attributes that are copied is platform and file system dependent and therefore unspecified. Minimally, the last-modified-time is copied to the target file if supported by both the source and target file store. Copying of file timestamps may result in precision loss.
//        NOFOLLOW_LINKS     Symbolic links are not followed. If the file is a symbolic link, then the symbolic link itself, not the target of the link, is copied. It is implementation specific if file attributes can be copied to the new link. In other words, the COPY_ATTRIBUTES option may be ignored when copying a symbolic link. An implementation of this interface may support additional implementation specific options.


        Path p11 = Paths.get("x\\y");
        Path p22 = Paths.get("z");
        Path p33 = p1.relativize(p22); // ../../z


        Path p1111 = Paths.get("c:\\..\\temp\\test.txt");
        System.out.println(p1111.normalize().toUri()); //file:///c:/temp/test.txt , because it is already the root

        A[] values = A.values();


        List<Integer> ls1 = Arrays.asList(3, 4, 6, 9, 2, 5, 7);
        System.out.println(ls1.stream().reduce(Integer.MIN_VALUE, (a, b) -> a > b ? a : b)); //1 it requires binaryOperator
        System.out.println(ls1.stream().max(Integer::max).get()); //2
        BinaryOperator<Integer> integerComparator2 = (a1, b1) -> Integer.max(a1, b1); //cant be added to max
        Comparator<Integer> integerComparator = (a1, b1) -> Integer.max(a1, b1);
        System.out.println(ls1.stream().max(integerComparator).get()); //2 Integer.max returns max number, not difference as comparator does (Integer.compare)
        System.out.println(ls1.stream().max(Integer::compare).get()); //3
        System.out.println(ls1.stream().max((a, b) -> a > b ? a : b)); //4 same problem as Integer.max,

        System.out.println(ls1.stream().mapToInt(Integer::valueOf).max().getAsInt()); // no need max comparator


        Connection c = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        Statement stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from STUDENT");

        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();

        p1 = Paths.get("c:\\code\\java\\PathTest.java");
        p1.getRoot(); //is c:\  ((For Unix based environments, the root is usually / ).
        p1.getName(0); //is code
        p1.getName(1); //is java
        p1.getName(2); //is PathTest.java
//        p1.getName(3); // will cause IllegalArgumentException to be thrown.


        DoubleStream iss = DoubleStream.of(0, 2, 4);
//        Note that this is unlike the average method which always returns an OptionalDouble for all numeric streams. If the stream has no elements, average returns an empty OptionalDouble and not a null.
        double sum = iss.filter(i -> i % 2 != 0).sum(); // 0, because if  empty elements list, returns 0
    }

    void met() {
        Stream.empty().filter(new TempClass.Inn(null)::innMethod);
        Stream.empty().filter(new Inn(null)::innMethod);
        Stream.empty().map(TempClass::new);
        Stream.empty().map(TempClass.Inn::new);
    }

    static enum A{ //by default is static
        a, v
    }

    TempClass(Object o) {
    }

    class Inn {
        Inn(Object o) {
        }

        public boolean innMethod(Object o) {
            return false;
        }
    }

    static class StaticInn {
        StaticInn(Object o) {
        }

        public boolean innMethod(Object o) {
            return false;
        }

        public static boolean innStaticMethod(Object o) {
            return false;
        }
    }
}

class Helper {
    void met() {
//        Stream.empty().filter(new TempClass.Inn(null)::innMethod);
//        Stream.empty().filter(new TempClass.Inn(null)::innMethod);
        Stream.empty().map(TempClass::new);
//        Stream.empty().map(new TempClass(null).Inn::new);
        Stream.empty().map(TempClass.StaticInn::innStaticMethod);
        Stream.empty().map(TempClass.StaticInn::new);


        Stream.empty().filter(new TempClass(null).new Inn(null)::innMethod);

        TempClass tempClass = new TempClass(null);
        Stream.empty().map(x -> tempClass.new Inn(x)); //no pointer
//        Stream.empty().map(tempClass.Inn::new); //::new not working with non static embedded


    }
}


class MyCache {
    static class InnerObject {
        public InnerObject(int i, int j) {
            this.i = i;
            this.j = j;
        }

        int i;
        int j;
    }

    private CopyOnWriteArrayList<InnerObject> cal = new CopyOnWriteArrayList<>();

    public void addData(List<InnerObject> list) {
        cal.addAll(list);
    }

    public Iterator<InnerObject> getIterator() {
        return cal.iterator();
    }

    public static void main(String[] args) throws InterruptedException {

        MyCache cache = new MyCache();
        cache.addData(Arrays.asList(new InnerObject(0, 0)));

        Thread t = new Thread(() -> {
            try {
                Iterator<InnerObject> iterator = cache.getIterator();
                iterator.next().i = 2;
                System.out.println("i");
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                Iterator<InnerObject> iterator = cache.getIterator();
                iterator.next().j = 2;
                System.out.println("j");
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t.start();
        t2.start();

        t.join();
        t2.join();
        System.out.println(cache.cal.toString());


    }
}

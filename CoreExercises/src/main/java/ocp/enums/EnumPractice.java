package ocp.enums;
//1. It implements java.lang.Comparable (thus, an enum can be added to sorted collections such as SortedSet, TreeSet, and TreeMap).
//2. It has a method ordinal(), which returns the index (starting with 0) of that constant i.e. the position of that constant in its enum declaration.
//3. It has a method name(), which returns the name of this enum constant, exactly as declared in its enum declaration.
public class EnumPractice {
    //it is final, also you cannot extend an enum from another enum or class because an enum implicitly extends java.lang.Enum. But an enum can implement interfaces.
    public static void main(String[] args) {
        E[] values = E.values();
        for (E value : E.values()) {
            System.out.println(value.name() + " " + value.ordinal());
        }

        E fromString = E.valueOf("A");
//        E fromString2 = E.valueOf("a"); //IllegalArgException

        switch (fromString) {
            case A://rather than no E.A
            case B:
        }

        //


        E1 a = E1.A; //on first usage, are invoked both enums(constructor is twice one for A, one for B)
        a.print();

    }
}

enum E { //no inheritance
    A, B
}

enum E1 { //no inheritance
    A("a"), B("b");

    String s;

    E1(String s) { //private by default, cant be non-private
        System.out.println("enumConstructor");
        this.s = s;
    }

    void print() {
        System.out.println("print: " + s);
    }

}

enum E2 {
    A {
        @Override
        void print() { // override
            System.out.println("printA");
        }
    }, B {
        @Override
        void print() {
            System.out.println("printB");
        }

        void print2() { //compile, can't access
            System.out.println("printB");
        }
    }, C;


    void print() { //can be abstract, enum remains as it is (no abstract tag near name)
        System.out.println("printDefault(C)");
    }

}

enum Exp{
    a{},b(),c
}

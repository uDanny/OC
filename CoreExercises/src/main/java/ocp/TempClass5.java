package ocp;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class TempClass5 {

    //review new Inputstream(Bufferstream(Inputstream))
    //todo: review which methods throws explicit NotFoundFIle instead of IO Exception
    //resource bundle default vs not specified
    public static void main(String[] args) {

        Thread thread = new Thread();
        thread.run(); //no exceptions thrown

        //
        List<ComparaBleClass> list = Arrays.asList(new ComparaBleClass(2), new ComparaBleClass(1));
        Collections.sort(list); //can sort even list operated by an array
        Collections.sort(list, (x, y) -> x.i - y.i); //sort in the same way

        TreeSet<NonComparableCass> objects = new TreeSet<>();
//        objects.add(new NonComparableCass()); //throws exception on adding first element

        //

        Path p1 = Paths.get("/personal/./photos/../readme.txt");
        Path p2 = Paths.get("/personal/index.html");
        Path p3 = p1.relativize(p2);
        System.out.println(p3);


        Path p11 = Paths.get("\\photos\\vacation"); // it starts with \\ -> returns arg
        Path p22 = Paths.get("\\yellowstone");
        System.out.println(p11.resolve(p22) + "  " + p11.relativize(p22));

        //
        IntStream.concat(IntStream.of(1), IntStream.of(2)); //[1,2]

        //


        try {
            ResultSet rs = null;
            rs.moveToInsertRow();
            rs.updateString(1, "111");
            rs.updateString(2, "Java in 24 hours");  //INSERT CODE HERE  Assuming that rs is a valid reference to an updatable ResultSet and that the result set contains only two String columns, what can be inserted in the code so that a new row is inserted in the database?
            rs.insertRow();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Instant now = Instant.now();
            Instant now2 = now.truncatedTo(ChronoUnit.DAYS);  // sets all values up to days, as 00
            System.out.println(now2);

        } catch (Exception e) {
        }

        IntFunction<IntUnaryOperator> aa = (x)->(y->x-y); // x the int,  y->x-y forms the method body of the IntFunction that should be returned
        IntUnaryOperator apply = aa.apply(1);
        System.out.println(apply.applyAsInt(2)); //-1  because 1(x) - 2(y)


        IntUnaryOperator a = x->x;
        a.applyAsInt(2);
    }


    static class NonComparableCass {
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
    }

    static class ComparaBleClass implements Comparable<ComparaBleClass> {
        Integer i;

        public ComparaBleClass(Integer i) {
            this.i = i;
        }

        @Override
        public int compareTo(ComparaBleClass o) {
            return i.compareTo(o.i);
        }
    }

    <K> void someClass(K k) {
        boolean equals = k.equals(null);
        int i = k.hashCode(); // Integer's hashCode simply returns the int value contained in the Integer object.
    }

    private class Inner1 {
        class Inner11 {

        }
    }

    class Inner2 {
        public void main(String[] args) {
            Inner1 inner1 = new Inner1();
            Inner1.Inner11 inner11 = inner1.new Inner11();

        }


    }
}


interface Classic {
    int version = 1;

    public static void readStatic() {
    }
}

class ClassicClass {
    static int version = 2;

    public static void readStatic() {
    }

}

class MediaReader extends ClassicClass implements Classic {

    public static void main(String[] args) {
        new MediaReader().read();
    }

    public void read() {
        System.out.println(((Classic) this).version);
//        System.out.println(version); //ambiguos
        System.out.println(super.version);

        readStatic(); // only from class can be called directly
    }
}

class ReaderTest {
    public static void main(String[] args) {
        MediaReader mr = new MediaReader();
        mr.read(); //
    }
}


class Boo2 implements Serializable {
    transient int ti = 10;
    static int si = 20;
}

class SerializeTransientClass {
    public static void main(String[] args) throws Exception {
        Boo2 boo = new Boo2();
        boo.si++;
        System.out.println(boo.ti + " " + boo.si);
        FileOutputStream fos = new FileOutputStream("c:\\temp\\boo.ser");
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(boo);
        os.close();
        FileInputStream fis = new FileInputStream("c:\\temp\\boo.ser");
        ObjectInputStream is = new ObjectInputStream(fis);
        boo = (Boo2) is.readObject();
        is.close();
        System.out.println(boo.ti + " " + boo.si); //transient is set to 0
    }
}

package ocp.jdbc;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public class JdbcPractice {
}

class MyFirstDatabaseConnection {
    public static void main(String[] args) throws SQLException {
        //connection example
//        jdbc:postgresql://localhost/zoo

        String url = "jdbc:derby:zoo";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select name from animal")) {
            while (rs.next()) System.out.println(rs.getString(1));
        }
    }
}

class TestConnect {
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1234");
        System.out.println(conn);
    }
}

class GetConnect {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver"); //required for JDBC <= 3.0 Driver, if there is no metadata file
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ocp-book", "username",
                "password");
    }
}

class StatementPractice {
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1234");
        System.out.println(conn);
        java.sql.Statement statement = conn.createStatement();

        Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        stmt.setMaxRows(2); //throws sqlException, sets limit programatically, (siliently ignoring extras)

    }

}

class StatementPractice2 {
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1234");
        Statement stmt = conn.createStatement();
        int result = stmt.executeUpdate(
                "insert into species values(10, 'Deer', 3)");
        System.out.println(result); // 1 rows affected
        result = stmt.executeUpdate(
                "update species set name = '' where name = 'None'");
        System.out.println(result); // 0
        result = stmt.executeUpdate(
                "delete from species where id = 10");
        System.out.println(result); // 1


        ResultSet rs = stmt.executeQuery("select * from species");

        execute(stmt);

        Map<Integer, String> idToNameMap = new HashMap<>();
        ResultSet rs2 = stmt.executeQuery("select id, name from species");
        while (rs.next()) {
            int id = rs2.getInt("id");
            String name = rs2.getString("name");
            idToNameMap.put(id, name);
        }
        System.out.println(idToNameMap); // {1=African Elephant, 2=Zebra}


        //second approach
        int id = rs.getInt(1); //starts with 1
        String name = rs.getString(2);
        idToNameMap.put(id, name);

        System.out.println(idToNameMap); // {1=African Elephant, 2=Zebra}

        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
    }

    private static void execute(Statement stmt) throws SQLException {
        boolean isResultSet = stmt.execute("select * from species");
        if (isResultSet) {
            ResultSet rs = stmt.getResultSet();
            System.out.println("ran a query");
        } else {
            int result = stmt.getUpdateCount();
            System.out.println("ran an update");
        }
    }

}

class Retrieving {
    public static void main(String[] args) throws SQLException {

        ResultSet rs = null;

        boolean aBoolean = rs.getBoolean(0);//BOOLEAN
        Date date = rs.getDate(0);//DATE
        double aDouble = rs.getDouble(0);//DOUBLE
        int anInt = rs.getInt(0);//INTEGER
        long aLong = rs.getLong(0);//BIGINT
        Object object = rs.getObject(0);//AnyType
        String string = rs.getString(0);//CHAR, VARCHAR
        rs.getTime(0); //TIME
        Timestamp timestamp = rs.getTimestamp(0);//TIMESTAMP


    }
}

class GetDate {
    public static void main(String[] args) throws SQLException {
        Statement stmt = null;

        ResultSet rs = stmt.executeQuery("select date_born from animal where name = 'Elsa'");
        if (rs.next()) {
            java.sql.Date sqlDate = rs.getDate(1);
            LocalDate localDate = sqlDate.toLocalDate();
            java.sql.Time sqlTime = rs.getTime(1);
            LocalTime localTime = sqlTime.toLocalTime();
            Timestamp timestamp = rs.getTimestamp(1);
            LocalDateTime sqlDateTime = timestamp.toLocalDateTime();


            System.out.println(localDate); // 2001―05―06
        }
    }
}

class GetObject {
    public static void main(String[] args) throws SQLException {
        Statement stmt = null;

        ResultSet rs = stmt.executeQuery("select id, name from species");
        while (rs.next()) {
            Object idField = rs.getObject("id");
            Object nameField = rs.getObject("name");
            if (idField instanceof Integer) {
                int id = (Integer) idField;
                System.out.println(id);
            }
            if (nameField instanceof String) {
                String name = (String) nameField;
                System.out.println(name);
            }
        }
    }
}

class Scrolling {
    public static void main(String[] args) throws SQLException {
        //all operators except .next(), requires Scrollable ResultSet
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1234");

        Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = stmt.executeQuery("select id from species order by id");
        rs.afterLast();
        System.out.println(rs.previous()); //true
        System.out.println(rs.previous()); //System.out.println(rs.getInt(1));

        System.out.println(rs.getInt(1)); // 2

        System.out.println(rs.last()); //true (is false if no result)
        System.out.println(rs.getInt(1)); //1
        System.out.println(rs.first()); //true (is false if no result)
        System.out.println(rs.getInt(1)); //2
        rs.beforeFirst();
        System.out.println(rs.getInt(1)); // throws SQLException

        //absolute
        System.out.println(rs.absolute(0)); // returns false, because index 0 stands for beforeFirst
        System.out.println(rs.absolute(2)); // true, 2nd element
        System.out.println(rs.absolute(-1)); // true, the last element, false if value is equals to beforeFirst it is length of elements *(-1)

        //relative
        System.out.println(rs.relative(2)); // if curent index == 5, returns 7th
        System.out.println(rs.relative(-2)); // if curent index == 5, returns 3rd

    }
}

class ClosingResources {
    public static void main(String[] args) {
        //Closing a Connection also closes the Statement and ResultSet.
        //Closing a Statement also closes the ResultSet.
        String url = "jdbc:derby:zoo";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select count(*) from animal")) {
            if (rs.next())
                System.out.println(rs.getInt(1));
            ResultSet rs2 = stmt.executeQuery("select count(*) from animal"); //rs is closed here
            int num = stmt.executeUpdate(
                    "update animal set name = 'clear' where name = 'other'");
        } catch (SQLException e) {
            // 4 resources are closed() both ResultSets
            e.printStackTrace();

            // error messages
            System.out.println(e.getMessage()); // ERROR: column "not_a_column" does not exist
            System.out.println(e.getSQLState()); // Position: 8
            System.out.println(e.getErrorCode()); // 0

        }
    }
}

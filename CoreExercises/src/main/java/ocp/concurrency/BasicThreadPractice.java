package ocp.concurrency;

public class BasicThreadPractice {

    public static void main(String[] args) {
        int minPriority = Thread.MIN_PRIORITY; //1
        int normPriority = Thread.NORM_PRIORITY; //5, the default value
        int maxPriority = Thread.MAX_PRIORITY; //10


        Runnable runnable = () -> System.out.println("Hello World");
        Runnable runnable0 = () -> {
            int i = 10;
            i++;
        };
        Runnable runnable1 = () -> {
            return;
        };
        Runnable runnable2 = () -> {
        };


        new ReadInventoryThread().start();
        new Thread(new PrintData()).start();
        System.out.println("end");
        //one of the possible
//        Printing zoo inventory
//                end
//        Printing zoo inventory
//        Printing record: 0
//        Printing record: 1
//        Printing record: 2

    }


}

class CalculateAverage implements Runnable {
    private double[] scores;

    CalculateAverage(double[] scores) {
        this.scores = scores;
    }

    @Override
    public void run() {
        // Define work here that uses the scores object
    }

}

class PrintData implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 3; i++) System.out.println("Printing record: " + i);
    }

    public static void main(String[] args) {
        (new Thread(new PrintData())).start();
    }
}

class ReadInventoryThread extends Thread {
    @Override
    public void run() {
        System.out.println("Printing zoo inventory");
    }

    public static void main(String[] args) {
        (new ReadInventoryThread()).start();
    }
}



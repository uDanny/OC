package ocp.concurrency;

import java.util.concurrent.*;

public class CallablePractice {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            Future<Integer> result = service.submit(() -> {
                Thread.sleep(10000);
                return 30 + 11;
            });


//            System.out.println(result.get()); //if it is uncommented, it is blocking main thread and waiting for result,
            //otherwise, it is printing isTerminated ==  false
        } finally {
            if (service != null) service.shutdown();
        }
        if (service != null) {
            service.awaitTermination(50, TimeUnit.MILLISECONDS); // Check whether all tasks are finished
            if (service.isTerminated()) //check if all tasks are done
                System.out.println("All tasks finished");
            else
                System.out.println("At least one task is still running");
        }
    }

}

class AddData {
    public static void main(String[] args) throws InterruptedException,
            ExecutionException {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            Future<Integer> result = service.submit(() -> 30 + 11);

            System.out.println(result.get());
        } finally {
            if (service != null) service.shutdown();
        }
    }
}

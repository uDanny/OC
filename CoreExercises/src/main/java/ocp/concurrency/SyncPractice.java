package ocp.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class SyncPractice {
}

class SheepManager {
    private int sheepCount = 0;
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    private void incrementAndReport() {
        System.out.print((++sheepCount) + " ");
    }

    private void incrementAtomicAndReport() {
        System.out.print((atomicInteger.incrementAndGet()) + " ");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager manager = new SheepManager();
            for (int i = 0; i < 100; i++)
                service.submit(() -> {
//                    try {
//                        Thread.sleep(4000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    manager.incrementAndReport(); //unordered and inconsistent
//                    manager.incrementAtomicAndReport(); //unordered, consistent
                });
        } finally {
            if (service != null) service.shutdown();
        }
    }
}


class SheepManagerSync {
    private int sheepCount = 0;
    private AtomicInteger atomicInteger = new AtomicInteger(0);
    private final Object lock = new Object();

    private synchronized void syncMethodIncrementAndReport() { //A thread is allowed to reacquire a lock that it already has in case of recursive call. So there will not be a deadlock if a method calls itself
        System.out.print((++sheepCount) + " ");
    }

    private void incrementAndReport() {
        synchronized (this) { //could also be: synchronized(lock) {
            System.out.print((++sheepCount) + " ");
        }
    }

    private void incrementAtomicAndReport() {
        System.out.print((atomicInteger.incrementAndGet()) + " ");
    }

    //static examples, sync all instances:
    public static void printDaysWorkStaticV1() {
        synchronized (SheepManager.class) {
            System.out.print("Finished work");
        }
    }

    public static synchronized void printDaysWorkStaticV2() {
        System.out.print("Finished work");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManagerSync manager = new SheepManagerSync();
            for (int i = 0; i < 10; i++) {
//                synchronized (manager) { //not solving the problem because it sync the creation of threads, not the execution
//                    service.submit(() -> manager.incrementAndReport());
//                }

                service.submit(() -> manager.incrementAndReport());
            }
        } finally {
            if (service != null) service.shutdown();
        }
    }
}

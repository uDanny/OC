package ocp.concurrency;

import java.util.*;
import java.util.concurrent.*;

public class ConcurentCollections {

    public static final void main() {
//table on page 512
//        not-ordered  not-sorted non-blocking
        new ConcurrentHashMap();/*ConcurrentMap*/  // //not allows nulls in key or values
//        ordered      not-sorted non-blocking
        new ConcurrentLinkedDeque();/*Deque*/  //
//        ordered      not-sorted non-blocking
        new ConcurrentLinkedQueue();/*Queue*/  //
//        ordered      sorted     non-blocking
        new ConcurrentSkipListMap();/*ConcurrentMap SortedMap NavigableMap*/  //
//        ordered      sorted     non-blocking
        new ConcurrentSkipListSet();/*SortedSet NavigableSet*/  //
//        ordered      not-sorted non-blocking
        new CopyOnWriteArrayList();/*List*/  //
//        not-ordered  not-sorted non-blocking
        new CopyOnWriteArraySet();/*Set*/  //
//        ordered      not-sorted blocking
        new LinkedBlockingDeque();/*BlockingQueue BlockingDeque*/  //
//        ordered      not-sorted blocking
        new LinkedBlockingQueue();/*BlockingQueue*/  //


    }


    public static void concurentExamples() {
        Map<String, Integer> map = new ConcurrentHashMap<>();
        map.put("zebra", 52);
        map.put("elephant", 10);
        System.out.println(map.get("elephant"));
        Queue<Integer> queue = new ConcurrentLinkedQueue<>();
        queue.offer(31);
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        Deque<Integer> deque = new ConcurrentLinkedDeque<>();
        deque.offer(10);
        deque.push(4);
        System.out.println(deque.peek());
        System.out.println(deque.pop());
    }

    public static void main(String[] args) {
        Map<String, Object> foodData = new HashMap<>();
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        try {
            for (String key : foodData.keySet())
                foodData.remove(key);
        } catch (ConcurrentModificationException e) {
            System.out.println("expected exception");
        }

        foodData = new ConcurrentHashMap<>(foodData);
        for (String key : foodData.keySet())
            foodData.remove(key);
        System.out.println(foodData); //{}


    }
}

class ZooManager {
    private Map<String, Object> foodData = new ConcurrentHashMap<>();

    public void put(String key, String value) {
        foodData.put(key, value);
    }

    public Object get(String key) {
        return foodData.get(key);
    }
}

class LinkedBlocking {
    public static void main(String[] args) {
        try {
            BlockingQueue<Integer> blockingQueue = new LinkedBlockingQueue<>();
            boolean offer = blockingQueue.offer(39);
            boolean offer1 = blockingQueue.offer(3, 4, TimeUnit.SECONDS);//false if exceed time
            Integer poll = blockingQueue.poll();
            Integer poll1 = blockingQueue.poll(10, TimeUnit.MILLISECONDS); //null if exceed time
            System.out.println(poll);
            System.out.println(poll1);
        } catch (InterruptedException e) { // Handle interruption
        }


        try {
            BlockingDeque<Integer> blockingDeque = new LinkedBlockingDeque<>();
            boolean offer = blockingDeque.offer(91);
            boolean offer1 = blockingDeque.offer(3, 4, TimeUnit.SECONDS);
            boolean b = blockingDeque.offerFirst(5, 2, TimeUnit.MINUTES); //false if exceed time
            boolean b1 = blockingDeque.offerLast(47, 100, TimeUnit.MICROSECONDS);

            Integer poll = blockingDeque.poll();
            System.out.println(poll);
            System.out.println(blockingDeque.pollLast(1, TimeUnit.SECONDS));
            System.out.println(blockingDeque.poll(950, TimeUnit.MILLISECONDS)); //null if exceed time
            System.out.println(blockingDeque.pollFirst(200, TimeUnit.NANOSECONDS));
        } catch (InterruptedException e) { // Handle interruption
        }
    }
}

class SkipListPractice {
    public static void main(String[] args) {
        NavigableSet concurrentSkipListSet = new ConcurrentSkipListSet();
        SortedMap concurrentSkipListMap = new ConcurrentSkipListMap();
    }

}

class CopyOnWritePractice {
    public static void main(String[] args) {

        List<Integer> list = new CopyOnWriteArrayList<>(Arrays.asList(4, 3, 52));
        for (Integer item : list) {
            System.out.print(item + " "); // ConcurrentModificationException for ArrayList
            list.add(9);
        }
        System.out.println();
        System.out.println("Size: " + list.size());
    }

}

class SyncCollectionPractice {
    public static void main(String[] args) {
        List<Integer> list = Collections.synchronizedList(new ArrayList<>(Arrays.asList(4, 3, 52)));
        synchronized (list) {
            for (int data : list){
                System.out.print(data + " ");
//                list.add(null);//also throws an ConcurrentModificationException
            }
        }
    }
}

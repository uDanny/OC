package ocp.concurrency;

import java.time.Instant;
import java.util.concurrent.*;

public class SchedulingExercise {
    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Runnable task1 = () -> System.out.println("Hello Zoo");
        Callable<String> task2 = () -> "Monkey";
        Future<?> result1 = service.schedule(task1, 10, TimeUnit.SECONDS);
        Future<String> result2 = service.schedule(task2, 8, TimeUnit.MINUTES);


        Future<?> result3 = service.scheduleAtFixedRate(() -> {
            System.out.println("fixedRate starts");
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("fixedRate ends");
        }, 0, 4, TimeUnit.SECONDS);
//        creating a new task every period value that passes.
        //run exactly by cron
        ScheduledFuture<?> result4 = service.scheduleWithFixedDelay(() -> {
            System.out.println("fixedDelay starts");
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("fixedDelay ends");
        }, 0, 4, TimeUnit.SECONDS);
        //Creates and executes a Runnable task after the given initial delay and subsequently with the given delay between the termination of one execution and the com- mencement of the next
        //runs after a period of time after the previous ends
        System.out.println(result4.getDelay(TimeUnit.MILLISECONDS)); //0

//        shut down by the time the scheduled task execution time is reached, they will be discarded.
    }
}

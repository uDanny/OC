package ocp.concurrency;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParallelStreamPractice {
    public static void main(String[] args) {
        Stream<Integer> stream = Arrays.asList(1, 2, 3, 4, 5, 6).stream();
        Stream<Integer> parallelStream = stream.parallel();
        Stream<Integer> parallelStream2 = Arrays.asList(1, 2, 3, 4, 5, 6).parallelStream();

        parallelStream.forEach(System.out::println);
        parallelStream2.forEach(System.out::println);
        Arrays.asList(1, 2, 3, 4, 5, 6).parallelStream().forEachOrdered(System.out::println);
    }

}

class WhaleDataCalculator {
    public int processRecord(int input) {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            // Handle interrupted exception }

        }
        return input + 1;
    }

    public void processAllData(List<Integer> data) {
        data.parallelStream().map(a -> processRecord(a)).count();
    }

    public static void main(String[] args) {
        WhaleDataCalculator calculator = new WhaleDataCalculator();
        // Define the data
        List<Integer> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) data.add(i);
        // Process the data
        long start = System.currentTimeMillis();
        calculator.processAllData(data);
        double time = (System.currentTimeMillis() - start) / 1000.0;
        // Report results
        System.out.println("\nTasks completed in: " + time + " seconds");
        paralelStream();
    }

    public static void paralelStream() {
        System.out.print(Arrays.asList(1, 2, 3, 4, 5, 6).parallelStream().findAny().get()); // unpredictable result
        System.out.print(Arrays.asList(1, 2, 3, 4, 5, 6).parallelStream().skip(1).limit(2).findFirst()); // consistent result

        Arrays.asList(1, 2, 3, 4, 5, 6).stream().unordered().parallel(); //could improve performance, if tell JVM that the order can be ignored
    }
}

class ReducerParallelStreams {
    public static void main(String[] args) {
        System.out.println(Arrays.asList(1, 2, 3, 4, 5, 6)
                .parallelStream()
                .reduce(0, (a, b) -> (a - b))); // NOT AN ASSOCIATIVE ACCUMULATOR

        System.out.println(Arrays.asList("w", "o", "l", "f").parallelStream().reduce("X", String::concat)); //XwXoXlXf
        System.out.println(Arrays.asList("w", "o", "l", "f").parallelStream().reduce("X", String::concat, (s, s2) -> s + " - " + s2)); //Xw - Xo - Xl - Xf

    }
}

class CollectorParallelStreams {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("w", "o", "l", "f").parallel();
        SortedSet<String> set = stream.collect(ConcurrentSkipListSet::new, Set::add, Set::addAll);
        System.out.println(set); // [f, l, o, w]


        //true parallelReduction
        Stream<String> ohMy = Stream.of("lions", "tigers", "bears").parallel();
        ConcurrentMap<Integer, String> map = ohMy
                .collect(Collectors.toConcurrentMap(String::length, k -> k, (s1, s2) -> s1 + "," + s2));
        System.out.println(map); // {5=lions,bears, 6=tigers}
        System.out.println(map.getClass()); // java.util.concurrent.ConcurrentHashMap


        Stream<String> ohMy2 = Stream.of("lions", "tigers", "bears").parallel();
        ConcurrentMap<Integer, List<String>> map2 = ohMy2.collect(
                Collectors.groupingByConcurrent(String::length));
        System.out.println(map); // {5=[lions, bears], 6=[tigers]}
    }
}

package ocp.concurrency;

import java.util.concurrent.*;

public class ExecutorPractice {
}

class ZooInfo {
    public static void main(String[] args) {

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("begin");
            service.execute(() -> System.out.println("Printing zoo inventory"));
            service.execute(() -> {
                for (int i = 0; i < 3; i++)
                    System.out.println("Printing record: " + i);
            });
            service.execute(() -> System.out.println("Printing zoo inventory"));
            System.out.println("end");
        } finally {
            if (service != null) {

                service.shutdown(); //run all registered runnable, after it is shut down
//                List<Runnable> runnables = service.shutdownNow(); //return all not started runnable
//                waiting that run which is in progress
//                dont start the following
//                also doesnt assure non-infinite execution
            }
            assert service.isShutdown(); //shutdown was invoked
            assert service.isTerminated(); //shutdown was terminated


            //shutDown needed because it creates a non-Daemon thread, and program never ends
        }
    }
}

class SubmitExamples {
    public static void main(String[] args) {

        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("begin");
            Future<?> f1 = service.submit(() -> System.out.println("Printing zoo inventory"));
            Future<?> f2 = service.submit(() -> {
                for (int i = 0; i < 3; i++)
                    System.out.println("Printing record: " + i);
            });
            Future<?> f3 = service.submit(() -> System.out.println("Printing zoo inventory"));
            System.out.println("end");
        } finally {
            if (service != null)
                service.shutdown();
            //shutDown needed because it creates a non-Daemon thread, and program never ends
        }
    }
}

class Get {
    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException,
            ExecutionException {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            Future<?> result = service.submit(() -> {
                for (int i = 0; i < 500; i++) {
                    counter++;
                    System.out.println(counter);
                }
            });
            Object o = result.get(1, TimeUnit.SECONDS); //it is not invoked, timeout is optional, o == null
            System.out.println("Reached!");
            boolean done = result.isDone();
            boolean cancelled = result.isCancelled();
            boolean cancelResult = result.cancel(true);
            //Attempts to cancel execution of the task, after it's call, if it is not started, it won't
            //if is started and mayInterrupt is true, is is trying to interrupt thread
            //isDone == true
            //isCancelled == true

        } catch (TimeoutException e) {
            System.out.println("Not reached in time");
        } finally {
            if (service != null) service.shutdown();
        }
    }
}

package ocp.concurrency;

public class OldFashionThreads {

    /**
     * New - This is the state the thread is in after the Thread instance has been created, but the start() method has not been invoked on the thread. It is a live Thread object, but not yet a thread of execution. At this point, the thread is considered not alive.
     * Runnable -  This is the state a thread is in when it's eligible to run, but the scheduler has not selected it to be the running thread. A thread first enters the runnable state when the start() method is invoked, but a thread can also return to the runnable state after either running or coming back from a blocked, waiting, or sleeping state. When the thread is in the runnable state, it is considered alive.
     * Running - This is it. The "big time." Where the action is. This is the state a thread is in when the thread scheduler selects it (from the runnable pool) to be the currently executing process. A thread can transition out of a running state for several reasons, including because "the thread scheduler felt like it." We'll look at those other reasons shortly. Note that in Figure 9-2, there are several ways to get to the runnable state, but only one way to get to the running state: the scheduler chooses a thread from the runnable pool.
     * Waiting/blocked/sleeping - This is the state a thread is in when it's not eligible to run. Okay, so this is really three states combined into one,
     * but they all have one thing in common: the thread is still alive, but is currently not eligible to run. In other words, it is not runnable, but it might return to a runnable state later if a particular event occurs. A thread may be blocked waiting for a resource (like I/O or an object's lock), in which case the event that sends it back to runnable is the availability of the resource—for example, if data comes in through the input stream the thread code is reading from, or if the object's lock suddenly becomes available. A thread may be sleeping because the thread's run code tells it to sleep for some period of time, in which case the event that sends it back to runnable is that it wakes up because its sleep time has expired. Or the thread may be waiting, because the thread's run code causes it to wait, in which case the event that sends it back to runnable is that another thread sends a notification that it may no longer be necessary for the thread to wait. The important point is that one thread does not tell another thread to block. Some methods may look like they tell another thread to block, but they don't. If you have a reference t to another thread, you can write something like this:
     * t.sleep();   or     t.yield()
     * But those are actually static methods of the Thread class—they don't affect the instance t; instead they are defined to always affect the thread that's currently executing. (This is a good example of why it's a bad idea to use an instance variable to access a static method—it's misleading. There is a method, suspend(), in the Thread class, that lets one thread tell another to suspend, but the suspend() method has been deprecated and won't be on the exam (nor will its counterpart resume()). There is also a stop() method, but
     * it too has been deprecated and we won't even go there. Both suspend() and stop() turned out to be very dangerous, so you shouldn't use them and again, because they're deprecated, they won't appear on the exam. Don't study 'em, don't use 'em. Note also that a thread in a blocked state is still considered to be alive.
     * Dead - A thread is considered dead when its run() method completes. It may still be a viable Thread object, but it is no longer a separate thread of execution. Once a thread is dead, it can never be brought back to life! (The whole "I see dead threads" thing.) If you invoke start() on a dead Thread instance, you'll get a runtime (not compiler) exception. And it probably doesn't take a rocket scientist to tell you that if a thread is dead, it is no longer considered to be alive.
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread() {
            @Override
            public void run() {
                super.run();
            }
        };


        Thread.sleep(5 * 1000);  // Sleep for 5 sec (current thread)

        t.setPriority(Thread.MAX_PRIORITY);
        Thread.yield(); //release priority for other threads, could prevents blocking

        t.join(); //wait for thread execution terminating

//        wait(), notify(), and notifyAll() must be called from within a synchronized context! A thread can't invoke a wait or notify method on an object unless it owns that object's lock.


        synchronized (OldFashionThreads.class) {
//IllegalMonitorStateException (runtime) if is invoked in non static context, InterruptedException if interrupted by another
            t.wait(); // waiting other threads to be notified, so it release block
            // the thread releases the lock and waits
            // To continue, the thread needs the lock,
            // so it may be blocked until it gets it.
            t.wait(100);  // Thread releases the lock and waits for notify
            // only for a maximum of two seconds, then goes back to Runnable // The thread reacquires the lock
            t.notify(); // take the lock back, as soon as it is available
            t.notifyAll(); // Will notify all waiting threads



        }
    }
}

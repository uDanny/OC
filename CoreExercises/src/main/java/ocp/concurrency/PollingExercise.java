package ocp.concurrency;

public class PollingExercise {
}

//polling
class CheckResults {
    private static int counter = 0;

    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < 500; i++) {
                CheckResults.counter++;
                System.out.println("++ " + counter);
            }
        }).start();
        while (CheckResults.counter < 100) {
            System.out.println("Not reached yet " + counter);
        }
        System.out.println("Reached!");
    }
}

class CheckResultsV2 {
    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            for (int i = 0; i < 500; i++) {
                CheckResultsV2.counter++;
                System.out.println("++ " + counter);
            }
        }).start();
        while (CheckResultsV2.counter < 100) {
            System.out.println("Not reached yet " + counter);
            Thread.sleep(1000); // 1 SECOND
        }
        System.out.println("Reached!");
    }
}

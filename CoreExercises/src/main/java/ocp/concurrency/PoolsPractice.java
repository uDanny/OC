package ocp.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class PoolsPractice {
    public static void main(String[] args) {
        ExecutorService executorService;
        ScheduledExecutorService scheduledExecutorService;
        executorService = Executors.newSingleThreadExecutor();
        executorService = Executors.newFixedThreadPool(2);
        executorService = Executors.newCachedThreadPool();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService = Executors.newScheduledThreadPool(20);

        int i = Runtime.getRuntime().availableProcessors();
        System.out.println(i);
    }
}

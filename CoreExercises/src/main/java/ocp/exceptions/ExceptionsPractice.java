package ocp.exceptions;

import java.io.IOException;
import java.util.Scanner;

public class ExceptionsPractice {
    public static void main(String[] args) throws IOException {

        try {
            // do some work
        } catch (RuntimeException e) {
            e = new RuntimeException(); //allowed reassignment
        }


        try {
            try {
                throw new IOException();
            } catch (IOException | RuntimeException e) {
//            e = new RuntimeException(); // DOES NOT COMPILE
                throw e; // can rethrow
            }
        } catch (RuntimeException e) {

        }


        try (Scanner s = new Scanner(System.in)) {
            s.nextLine();
        } catch (Exception e) {
//            s.nextInt(); // DOES NOT COMPILE because it is already closed on closing try bracket
        } finally {
//            s.nextInt(); // DOES NOT COMPILE
        }
    }
}

class JammedTurkeyCage implements AutoCloseable {
    String text;

    public JammedTurkeyCage(String text) {
        this.text = text;
    }

    @Override
    public void close() throws IllegalStateException {
        throw new IllegalStateException("Cage door does not close " + text);
    }

    public static void main(String[] args) {

        System.out.println("ex 1");
        try (JammedTurkeyCage t = new JammedTurkeyCage("1")) {
            throw new IllegalStateException("turkeys ran off");
        } catch (IllegalStateException e) {
            System.out.println("caught: " + e.getMessage()); //caught: turkeys ran off
            for (Throwable t : e.getSuppressed())
                System.out.println(t.getMessage()); //Cage door does not close 1
        }


        System.out.println("ex 2");

        try {
            try (JammedTurkeyCage t = new JammedTurkeyCage("2")) {
                throw new RuntimeException("turkeys ran off");
            } catch (IllegalStateException e) { // Runtime is not caught here
                System.out.println("caught: " + e.getMessage());
            }
        } catch (RuntimeException e) {
            System.out.println("Runtime is caught here: " + e.getMessage()); //Runtime is caught here: turkeys ran off
            for (Throwable t : e.getSuppressed())
                System.out.println(t.getMessage()); // Cage door does not close 2


        }


        System.out.println("ex 3");
        try (
                JammedTurkeyCage t1 = new JammedTurkeyCage("t1");
                JammedTurkeyCage t2 = new JammedTurkeyCage("t2")) {
            System.out.println("turkeys entered cages");
        } catch (IllegalStateException e) { // enter in block from close
            System.out.println("caught: " + e.getMessage()); // caught: Cage door does not close t2
            for (Throwable t : e.getSuppressed())
                System.out.println(t.getMessage());
        }
    }
}

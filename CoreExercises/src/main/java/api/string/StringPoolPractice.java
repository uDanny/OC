package api.string;

import java.lang.reflect.Field;

public class StringPoolPractice {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        String s = "ss";
        String s01 = "ss";
        String s1 = new String("ss"); //It goes to pool as it is a String literal, but returned heap object
        String s2 = new String(s);


        System.out.println(s == s01); // true
        System.out.println(s == s1); // false
        System.out.println(s1 == s2); // false
        System.out.println(s == s2); // false
        System.out.println(s1.intern() == s); // true, intern, manually put from heap to pool
        System.out.println("sssss" == "sss" + "ss"); // true
        System.out.println(s1.intern() == "s".concat("s")); //false (or trim, etc.)
////
        System.out.println("\nStringBufer");
        String string = new String("a");
        String string2 = new StringBuffer("a").toString();
        System.out.println(string == string2); // false, StringBuffer -> new String

        //// cheat on pool
        System.out.println("\nCheatOnPool");
        // add "Hello" to pool
        String hello = new String("Hello");

        // Change value of "Hello" string to "Buy Buy"
        Field value = hello.getClass().getDeclaredField("value");
        value.setAccessible(true);
        value.set(hello.intern(), "Buy buy".toCharArray());

        // Print "Hello" to console
        System.out.println("Hello");
    }
}

package api.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//todo: review tricky StringUtils, in comparation with String
public class StringsPractice {
    public static void main(String[] args) {
        String s = "sdsadsadsa";
        String s1 = "sdsadsadsadadasdsa";

        s.length();
        s.charAt(2);//StringIndexOutOfBoundsException if more then the length

        int s2 = s.indexOf('s'); // -1 if not found
        int s3 = s.indexOf("dsa");
        int s4 = s.indexOf('s', 2);
        int s5 = s.indexOf("dsa", 2);

        String substring = s.substring(3);//3 to end
        String substring1 = s.substring(3, 7);//3 to 7 ( may cause exception );

        //it replaces all
        s.replace('s', 'q');
        s.replace("a", "oo");


        boolean sd = s.startsWith("sd");
        boolean sd1 = s.endsWith("sd");
        boolean s6 = s.contains("s");


        //StringBuilder
        StringBuilder sb = new StringBuilder("aaaasadfssdfs");
        char c = sb.charAt(0);
        int length1 = sb.length();
//        int ss2 = sb.indexOf('a'); //no char param
        int ss2 = sb.indexOf("ss");
        int ss3 = sb.indexOf("ss", 2);
        int length = sb.length();
        String substring2 = sb.substring(0);
        String substring3 = sb.substring(0, 1);
        StringBuilder ss = sb.insert(2, "ss");// insert at index, inclusive last index + 1, push that index to +1
        sb.insert(2, "ss", 0, 1);// insert only one s
        new StringBuilder("xxxx").insert(1, "oo", 0, 1).toString(); //xoxxx
//        StringBuilder ss1 = sb.insert(20, "ss");//StringIndexOutOfBoundsException
        StringBuilder ss1 = sb.replace(1, 3, "a"); //01234 ->1a4
        new StringBuilder("xxxx").append("Aaaaa", 0,1).toString(); //xxxxA


        StringBuilder delete = sb.delete(0, 1);
        StringBuilder stringBuilder = sb.deleteCharAt(1);
        StringBuilder reverse = sb.reverse();
        String string = sb.toString();


        String s7 = "";
        s7 += 2;
        s7 += '1';
        s7 += false;
        s7 += new Object();
        s7 += null;
        System.out.println(s7);


        boolean trUe = "String".replace('g', 'g') == "String"; // replace returns the same object if there is no change.

        stringBuilderPractice();
        StringBuilder stringBuilderLengthExample = new StringBuilder("aa");
        stringBuilder.setLength(1); //a


        StringBuilder sbCapacity = new StringBuilder(100);
        StringBuilder sbCapacity1 = new StringBuilder();
        sbCapacity1.ensureCapacity(100);
        //this value is increased if needed on append

    }

    private static void stringBuilderPractice() {
        StringBuilder s = new StringBuilder("abcde");
        System.out.println(s.substring(0, 1)); // does not include the end index!
        System.out.println(s.substring(0, s.length())); // valid expresion, full list !

        //same approach like in StringBuilder
        List<Integer> list = new ArrayList<>(Arrays.asList(0, 1, 2));
        System.out.println(list.subList(0, 0)); // empty list
        System.out.println(list.subList(0, 1)); // one element, 0th
        System.out.println(list.subList(0, list.size())); // valid expresion, full list! not allowed size() + 1, outOfBound exception


        StringBuilder sb = new StringBuilder("aaa");
        //no removeAll
        sb.delete(0, sb.length() + 1); //just delete all

        s.reverse();
    }
}

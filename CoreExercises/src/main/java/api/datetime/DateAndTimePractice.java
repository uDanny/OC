package api.datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAdjusters;

public class DateAndTimePractice {
    public static void main(String[] args) {
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());


        LocalDate date1 = LocalDate.of(2015, Month.JANUARY, 20);
        LocalDate date2 = LocalDate.of(2015, 1, 20);

        LocalTime time1 = LocalTime.of(6, 15); // hour and minute
        LocalTime time2 = LocalTime.of(6, 15, 30); // + seconds
        LocalTime time3 = LocalTime.of(6, 15, 30, 200); // + nanoseconds

        // constructors min still Min, max still nanoSec. Month = enum/nr
        LocalDateTime dateTime1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6, 15, 30);
        LocalDateTime dateTime2 = LocalDateTime.of(date1, time1);
//        LocalDate.of(2015, Month.JANUARY, 32); // throws DateTimeException

        dateOperations();
        timeOperations();
        dateTimeOperations();

        //Period - for Date / DateTIme
        Period period = Period.ofMonths(1); // create a period
        period = Period.ofYears(1);
        period = Period.ofDays(1);
        period = Period.ofWeeks(1);
        period = Period.of(1, 2, 3);
        date1.plus(period);
        dateTime1.minus(period);
//        time1.plus(period); // UnsupportedTemporalTypeException

        //Duration - for Time / DateTIme
        Duration duration = Duration.ofDays(2);
        duration = Duration.ofHours(2);
        duration = Duration.ofMinutes(2);
        duration = Duration.ofSeconds(2);
        duration = Duration.ofSeconds(2, 3); //nano adjustments
        duration = Duration.ofMillis(3);
        duration = Duration.ofNanos(3);

        time1.plus(duration);
        dateTime1.minus(duration);
//        date1.minus(Duration.ofDays(3)); // UnsupportedTemporalTypeException

        //days has no impact on the time
        LocalTime l1 = time1;
        LocalTime l2 = time1;
        System.out.println(l1);
        l2 = l1.plus(Duration.ofDays(2));
        System.out.println(l2);
        System.out.println(l1.equals(l2)); //true

        displayFormats();
        parseDate();

        LocalDateTime l = LocalDateTime.now();
//        DateTimeFormatter.ISO_ZONED_DATE_TIME.format(l); //exception, localized has no zone


    }

    private static void parseDate() {
        System.out.println("parse date");
        DateTimeFormatter f = DateTimeFormatter.ofPattern("MM dd yyyy");
        LocalDate date = LocalDate.parse("01 02 2015", f);
        LocalTime time = LocalTime.parse("11:22");
        LocalTime time2 = LocalTime.parse("11:22");
        System.out.println(date); // 2015-01-02
        System.out.println(time); // 11:22
    }

    private static void displayFormats() {
        LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time = LocalTime.of(11, 12, 34);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        System.out.println("iso");
        System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(time.format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        //or
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(dateTime));


        ///
        System.out.println("short");
        DateTimeFormatter shortDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        System.out.println(shortDateTime.format(dateTime)); // 1/20/20
        System.out.println(dateTime.format(shortDateTime)); // 1/20/20
//        System.out.println(shortDateTime.format(date)); // UnsupportedTemporalTypeException
//        System.out.println(date.format(shortDateTime)); // UnsupportedTemporalTypeException
//        System.out.println(shortDateTime.format(time)); // UnsupportedTemporalTypeException
//        System.out.println(time.format(shortDateTime)); // UnsupportedTemporalTypeException

        DateTimeFormatter shortDateTime2 =
                DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        System.out.println(dateTime.format(shortDateTime2));
        System.out.println(shortDateTime2.format(dateTime));
        System.out.println(shortDateTime2.format(date));
//        System.out.println(time.format(shortDateTime2)); //UnsupportedTemporalTypeException
//        System.out.println(shortDateTime2.format(time)); //UnsupportedTemporalTypeException
//
        DateTimeFormatter shortDateTime3 =
                DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
        System.out.println(dateTime.format(shortDateTime3));
        System.out.println(shortDateTime3.format(dateTime));
        System.out.println(time.format(shortDateTime3));
        System.out.println(shortDateTime3.format(time));
//        System.out.println(date.format(shortDateTime3)); //UnsupportedTemporalTypeException
//        System.out.println(shortDateTime3.format(date)); //UnsupportedTemporalTypeException

        System.out.println("short, medium");

        System.out.println(date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT))); // 1/20/20
        System.out.println(date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM))); // Jan 20, 2020

        System.out.println(time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))); // 11:12 AM
        System.out.println(time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM))); // 11:12:34 AM


        System.out.println(dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT))); // 1/20/20 11:12 AM
        System.out.println(dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))); //Jan 20, 2020 11:12:34 AM

//
        System.out.println("pattern");
        DateTimeFormatter f = DateTimeFormatter.ofPattern("MMMM dd, yyyy, hh:mm");
        System.out.println(dateTime.format(f));
        System.out.println(f.format(dateTime));
//        System.out.println(date.format(f)); //UnsupportedTemporalTypeException
//        System.out.println(time.format(f)); //UnsupportedTemporalTypeException

        System.out.println(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.TUESDAY)));
        System.out.println(TemporalAdjusters.next(DayOfWeek.TUESDAY).adjustInto(LocalDate.now()));


    }

    public static void timeOperations() {
        LocalTime time = LocalTime.of(5, 15, 22, 34);
        System.out.println(time);
        time = time.plusHours(2);
        time = time.plusMinutes(2);
        time = time.minusSeconds(2);
        time = time.minusNanos(2);
        System.out.println(time);


    }

    private static void dateTimeOperations() {
        //imutable
        LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time = LocalTime.of(5, 15);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        System.out.println(dateTime); // 2020-01-20T5
        dateTime = dateTime.minusDays(1);
        System.out.println(dateTime); // 2020-01-19T5
        dateTime = dateTime.minusHours(10);
        System.out.println(dateTime); // 2020-01-18T5
        dateTime = dateTime.minusSeconds(30);
        System.out.println(dateTime); // 2020-01-18T0
    }

    private static void dateOperations() {
        //imutable
        LocalDate date = LocalDate.of(2014, Month.JANUARY, 20);
        System.out.println(date); // 2014-01-20
        date = date.plusDays(2);
        System.out.println(date); // 2014-01-22
        date = date.plusWeeks(1);
        System.out.println(date); // 2014-01-29
        date = date.plusMonths(1);
        System.out.println(date); // 2014-02-28
        date = date.plusYears(5);
        System.out.println(date);
    }
}

package api.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class ArraysPractice {
    int[] defaultInts = {1, 3};
    int[] defaultInts2 = new int[2]; //[0, 0]
    int[] defaultArray; //null

    public static void main(String[] args) {
        int[] ints;
        int ints2[];

        int[] i = new int[2];
        int[] i2 = new int[]{1, 2};
        int[] i3 = {1, 2};

        int[] ii = i;

        Object[] o = {1, 2, null, 3};
        String[] s = {"a"};

        System.out.println(o); // print reference
        System.out.println(s); // print reference
        System.out.println(Arrays.asList(s)); // print implements print

        int length = i.length;

        int[][] multi = new int[0][1];
        int multi2[][] = new int[0][1];
        int[] multi3[] = new int[0][1];

        int[] multi4[] = {{0, 1}, {0, 2}};
        int[][] multi5 = new int[2][]; // could only init first
//        multi5[0][0] = 1; // nullpointer

        for (int[] ints1 : multi5) {
            System.out.println(ints1); //prints 2 nulls
        }
        multi5[0] = new int[]{1, 2}; // cant use only {}, only on initialization works
//        multi5[1] = {1, 2}; // not compile
        multi5[0] = new int[2];

        int[] multi6[] = {{}, {}};
        multi6[0] = new int[2];
        multi6[1] = new int[]{1, 2};
        multi6[0][0] = 5;

        int[] multi7 = {1, 2};
        method(multi7);
        //multi7 == {1,0};

        int[][] multi8 = {{1, 2}, {4, 5}};
        //multi8 == {{1, 2}, {4, 5}};

        int[] multi9 = new int[2];
//        multi9[0] = null;

        //arrays with behaviour, like objects:
        String[][][] a1 = new String[3][][];
        String[][] a2 = new String[10][];
        a1[0] = a2;
        a2[1] = new String[]{"A"}; //changed a1

        a1.getClass().isArray(); // true

//        int[] primitiveNull = {null};
        Integer[] wrapperNull = {null};

        int[] primitiveNull = new int[2]; //all elements are 0

        ArrayList<String> objects = new ArrayList<>();
        objects.add(0, "a");
        objects.add(0, "b"); //output [b,a]
        objects.add(9, "b"); //IndexOutOfBoundException
    }

    private static void method(int[] m) {
        m[1] = 0;
    }

    private static void method(int[][] m) {
        m[1] = null;
    }


}

class ObjectLike {
    char c;

    public void m1() {
        char[] cA = {'a', 'b'};
        m2(c, cA);
        System.out.println(((int) c) + "," + cA[1]);
    }

    public void m2(char c, char[] cA) {
        c = 'b';
        cA[1] = cA[0] = 'm';
    }

    public static void main(String args[]) {
        new ObjectLike().m1(); // prints 0,m, because for arrays = operator changes state, not reference
    }
}

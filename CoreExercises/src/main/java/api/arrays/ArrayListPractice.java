package api.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayListPractice {
    public static void main(String[] args) {

        ArrayList<String> a = new ArrayList<>(Arrays.asList("ss", "dd", "ff"));

//        ArrayList's remove(Object ) method removes the first occurence of the given element and returns true if found.
        a.remove(2); //remove or index, index has priority if <Integer>, can access objects thru new Integer
        a.set(0, "aa");
        System.out.println(a.size());

        a.clear();
        System.out.println(a);

        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(1, 2, 3));
        integers.add(null);
//        int integer = integers.get(3); // nullPointerException because null, works with autoboxing

        listToArray();
        trickSize();
        immutableList();


        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(3);
        Collections.sort(list);
        int a1 = Collections.binarySearch(list, 2); // -3 =? in sorted list it's index would be 2, because it is absent it is negated +1 value


        List<Double> stringList = new ArrayList<>();
        //ofObject

        stringList.indexOf("ofObject");
        stringList.contains("also of object");
        stringList.contains("also of object");

//        stringList.add(2); // !!! no compile, no autobox


    }

    public static void listToArray() {
        List<String> list = new ArrayList<>();
        list.add("hawk");
        list.add("robin");
        Object[] objectArray = list.toArray();
        System.out.println(objectArray.length);
        String[] stringArray = list.toArray(new String[0]); // if it is not an object array, it needs an init of array type. if array size is smaller then list size, it creates new bigger array
        System.out.println(stringArray.length);

//        String[] objects = (String[]) list.toArray(); //classCastException

    }

    public static void trickSize() {
        List<String> list = new ArrayList<>();
        list.add("a");
//        list.set(5, "f"); //cant set empty value
    }

    public static void immutableList() {
        String[] s = {"a", "b"};
        List<String> list = Arrays.asList(s);
        List<String> list2 = new ArrayList<>(Arrays.asList("a"));
        //list.remove(0); // throw exception because is an array under the hood (fixed size)
        list2.remove(0); //works fine

    }


}

class CollectionsVsArrayCasting {
    public static void main(String args[]) {
        A[] a, a1;
        B[] b;
        a = new A[10];
        a1 = a;
        b = new B[20];
        a = b;  // 1
        b = (B[]) a;
        b = (B[]) a1;


        List<A> aa = new ArrayList<>(), aa1 = new ArrayList<>();
        List<B> bb = new ArrayList<>();
        aa1 = aa;

//        aa = bb;

    }
}

class A {
}

class B extends A {
}

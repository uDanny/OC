package cmd;

/**
 * class could be public or private package
 * in a file, we can have only one public class, and it should has the same name as the file
 * we can compile file with multiple classes, and jvm will create a .class file for each
 * example of using it:
 * javac -cp <path to source> -d <compiler output path> <path to source>.java
 * java -cp <path to other classes> <var args> <var args> <package name>.<classname>
 */
 class CmdRun {
    public static void main(String[] args) {
        System.out.println("cmd RUN");
    }
}
class A{
    public static void main(String[] args) {
        System.out.println("A RUN");
    }
}

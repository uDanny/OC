import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//        remember that you can never access a class that is defined in the default package (i.e. the package with no name) from a class in any other package
class Uncategorized {

    public static void main(String args[]) {

        atributionOrder();

//        + operator doesnt support nulls
//        System.out.println(null + true);
//        System.out.println(true + null);
        System.out.println(true +" " + null);
//        System.out.println(null + null);

//        while (false) { x=3; } - not reachable
//        for( int i = 0; false; i++) x = 3; - not reachable


    }

    private static void atributionOrder() {
        String str1 = "one";
        String str2 = "two";
        System.out.println(str1.equals(str1 = str2)); //false
//        boolean operators have more precedence than =. (In fact, = has least precedence of all operators.)
    }


}


class PP {
    Object ceva() {
        return null;
    }
}

class CC extends PP {
//    @Override
//    int ceva() { // not allowed, primitive is not an obj
//        return null;
//    }
}

//castingOrder
//nocompile
class NoHierarchyClass {

    public void method(Object o) {
        System.out.println("Object Version");
    }

    public void method(String s) {
        System.out.println("String Version");
    }

    public void method(StringBuffer s) {
        System.out.println("StringBuffer Version");
    }

    public static void main(String args[]) {
        NoHierarchyClass tc = new NoHierarchyClass();
//        tc.method(null); no compile because is ambiguous call
    }
}


//java.io.FileNotFoundException Version
class HierarchyCast {
    public void method(Object o) {
        System.out.println("Object Version");
    }

    public void method(java.io.FileNotFoundException s) {
        System.out.println("java.io.FileNotFoundException Version");
    }

    public void method(java.io.IOException s) {
        System.out.println("IOException Version");
    }

    public static void main(String args[]) {
        HierarchyCast tc = new HierarchyCast();
        tc.method(null);
        //call java.io.FileNotFoundException because is most precise (child) class in the hierarchy
    }
}

class PlusPlusOperator {
    static int pfff = 0;

    public static void someMethod(int j) {
        int i = 0;
        System.out.println("for");

        for (i = 0; i < 5; i++) {
            System.out.println(i); // 0 - 4
        }
        System.out.println("-----");
        for (i = 0; i < 5; ++i) {
            System.out.println(i); // 0 - 4
        }
        i = 0;

        System.out.println("doWhile");
        do {
            System.out.println(i); // 0 - 5
        } while (i++ < 5);
        System.out.println("-----");
        i = 0;
        do {
            System.out.println(i); // 0 - 4
        } while (++i < 5);

        i = 0;
        System.out.println("whileDo");
        while (i++ < 5) {
            System.out.println(i); // 1 - 5
        }
        System.out.println("-----");
        i = 0;
        while (++i < 5) {
            System.out.println(i); // 1 - 4

        }
        i = 0;
        System.out.println("switch");

        switch (i++) {
            case 0:
                System.out.println(0); //triggered, but i == 1
                break;
            case 1:
                System.out.println(1);
                break;

        }
        i = 0;
        System.out.println("-----");
        switch (++i) {
            case 0:
                System.out.println(0);
                break;
            case 1:
                System.out.println(1); //triggered, but i == 1
                break;


        }
    }
    static public int some(int i) {
        return i++;
    }
    static public int some2(int i) {
        return ++i;
    }

    public static void main(String args[]) {
        int o = 0;
        someMethod(pfff++); // j = 0, pfff = 0
        someMethod(o++); // j = 0, o = 1

        System.out.println(some(0) == some2(0));//false



    }

}

class WhileRemoveFromList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
            iterator.remove();
        }
    }
}

//

class Super2 {
    static java.lang.String ID = "QBANK";
    static java.lang.String ID_OVERRIDDEN = "ZBANK";
}

class Sub2 extends Super2 {
    static java.lang.String ID_OVERRIDDEN = "ZBANK";

    static {
        System.out.print("In Sub");
    }
}

class Test2 {
    public static void main(String[] args) {
        System.out.println(Sub2.ID);
        System.out.println(Sub2.ID_OVERRIDDEN);
        //prints QBANK In SubZBANK, because static block is invoked on first usage of static variables of object
    }
}




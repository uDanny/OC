package reflection;

import java.lang.reflect.Field;

public class ReflectionValueSetter {
    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {
        Integer a = 2;

        Field valField = a.getClass().getDeclaredField("value");
        valField.setAccessible(true);
        valField.setInt(a, 4);

        Integer b = 2;
        Integer c = 1;

        System.out.println("b+c : " + (b+c) ); // b+c : 4
    }
}

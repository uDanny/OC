package basicVarTypes;

public class PrimitiveNumbersCastingOrder {

    static public void met(byte i) {
    }

    static public void met(int i) {
    } // and other larger types types

    static public void met(Byte i) {
    } //only byte Wrapper, no other boxing larger formats like Integers

    static public void met(Number i) {
    }

    static public void met(Object i) {
    }

    static public void met(byte... i) {
    } // if Byte... and byte... => multiple implementations

    static public void met(Byte... i) {
    } // if Byte... and byte... => multiple implementations, no other boxing larger formats like Integers


    static public void met(int... i) {
    } //all larger primitives

    static public void met(Number... i) {
    }

    static public void met(Object... i) {
    }

    public static void main(String[] args) {
        byte b = 120; //8

        met(b);

    }
}

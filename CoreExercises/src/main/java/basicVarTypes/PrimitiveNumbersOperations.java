package basicVarTypes;

public class PrimitiveNumbersOperations {

    public static void main(String[] args) {

        /**upcasting on operations example*/
        byte a11 = 3;
        byte a12 = 2;
        byte a13 = (byte) (a11 + a12); //operations on byte and short are automatic upcasting to int
        a13 += a11; //automatic casting to byte from int(because of operation)
        a13 += 1; //automatic casting to byte from intlo
        char c = 0;
        c += 2; //char also supporting incrementing

        /**maxvalue primitives*/
        long a = Long.MAX_VALUE;
        float b1 = Float.MAX_VALUE + 1; //b1 == maxValue

        /**operation casting*/
        int iii = 11;
        long l1 = a + iii;//to larger format
        float v = a + b1; //to floating point

        operations();

        int positive = 2;
        int negative = -positive; //-2

        float works = Integer.valueOf(2) + (byte) 2;
    }

    /**
     * tricky operations
     */
    private static void operations() {
        System.out.println(1 / 2); // 0
        System.out.println(50 % 53); // 50

        System.out.println(0 / 1); // 0
//        System.out.println(1 / 0); // ArithmeticException
        System.out.println(0 % 53); // 0
//        System.out.println(53 % 0); //  ArithmeticException
        /** operations order */
        int i = 0;
        i = i++; // i remain 0
        System.out.println(i);

        long ss = '2';
        long op = ss + Long.valueOf("5");

    }


}

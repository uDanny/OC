package basicVarTypes;

import java.util.Arrays;

public class PrimitiveNumbers {
    public static void main(String[] args) {
        byte b = 120; //8 bytes range of byte: -128 and 127
        short s = -20; //16 bytes
        int i = -30; // 32 bytes
        long l = -3L; // 64 bytes, requires L when is bigger than int
        float f = 21.0F; //32 bytes floating, requires F when has .
        double d = 1.1D;

        /**force casting*/
        int ii = (int) 2.3_3; //can ve used _ for visual delimitation, can't be used on start, end, ., L, F
        int iVL = (int) l;
        byte bV = (byte) i; //8 with memory leaks
        int iV = bV; //8
        double VV = f;


        /**byte, octal, hexadecimal formats*/
        System.out.println(0b11); // 3
        System.out.println(017); // 15
        System.out.println(0x1F); // 31
//        float x = 0b10_000f; no f or d allowed, only L
        long xww = 0b10000L; // valid construction
        System.out.println(Integer.valueOf("017")); //17, not (017)->octal 15

        /**return on declaration*/
        int y = 5;
        int x = (y += 10); //return value of y 5 + 10

        /**primitive constants*/
        System.out.println(Long.MAX_VALUE);
        System.out.println(Float.MAX_VALUE);

        /**declarations*/
        byte b2 = 0, b3 = 1;
        byte b22, b33 = 2; //b22 not initialized
        byte b0 = b2 = 8; //b0 = 8 and b2 = 8

        Math.round(0.5); //1
        Math.round(-0.5); //0

        int intValueResult = Integer.valueOf(0).intValue();
        Boolean boolObject = Boolean.FALSE; // Boolean obj
        int max = Integer.MAX_VALUE; // primitive


        boolean primitive = Boolean.parseBoolean("String"); //false
        Boolean object = Boolean.valueOf("String");
        Boolean object2 = Boolean.valueOf(true);

        int a = Byte.valueOf((byte) 2);
        Integer a2 = (int) 2; //no byte no Byte

        bytesOperations();

    }

    private static void forLoopsCasting() {

        int[] i = {0};
        for (Integer ii : i) { //can be long autocasting, no Long
            System.out.println("  to Integer");
        }

        Integer[] I = {0};
        for (long integer : I) { //can be long autocasting, no Long
            System.out.println("  to int");
        }
        for (long integer : Arrays.asList(I)) { //can be long autocasting, no Long
        }
    }


    /**
     * return types casting
     */
    /**
     * autocasting to upper values from primitive
     */
    public int byteToInt() {
        byte i = 2;
        return i; //
    }

    /**
     * autocasting to upper values from wrapper
     */
    public int byteWrapToInt() {
        Byte i = 2;
        return i; // autocasting to upper values
    }

    /**
     * no autocasting, works only for int-Integer
     */
    public Integer intToIntWrap() { // no autocasting to upper values
        int i = 2;
        return i;
    }

    /**
     * attribution, equality, operators
     */
    public static final Short psfS = 2;
    public static final Integer psfI = 2;
    public static final Long psfL = 2L;
    public static final Double psfD = 2.0;
    public static final Character psfC = 'a';

    public static final short psfs = 2;
    public static final int psfi = 2;
    public static final long psfl = 2;
    public static final double psfd = 2.0;
    public static final char psfc = 'a';


    public static void equalityMethod() {
        Short S = 2;
        Integer I = 2;
        Long L = 2L;
        Double D = 2.0;
        Character C = 'a';

        short s = 2;
        int i = 2;
        long l = 2;
        double d = 2;
        char c = 'a';


        System.out.println("attribution");

        Long aLong = new Long("2");
        final int finalInt = 0;
        final long finalLong = 0;
        //finalStatic
        s = finalInt; //works because is final, jvm understand it fits
//        s = finalLong; //works only up to int
        s = psfi; //no psfI, no psfd, no float = psfd
        s = psfc;
//        s = c;
        d = psfc;
//        s = i;
        //autocasting
        s += d; //autocasting

        i = s;
        i = I;
        i = S;
        i = c; // no s = c
        i = C;
//        i = D;
        i += D;
        d = c;

        S = finalInt; // because it is final and it fits
        S = psfi; // no psfI
        I = i; // no I = s;
//        S = i;
        I = I + i;
        //onlyLowerAutocasting
        I -= s;
        I -= S;
        I -= C;
        i -= l;
//        I -= l;
//        I -= L;
        D -= s;


//        I = c;
        c = C; //no I, no i, no b ...
//        c = s;
        //onlyPrimitiveAutocasting
        c += l;
//        C += l;
//        C += s;
        c = psfi; //no long
        c = psfs; //no long
//        c = s;
//        c = i;
        C = psfi;

        //allowed operations:
        {
            i = I;
            i = s;
            i = S;

            i = c;
            i = C;

            s = psfi;
            s = psfc;

            i += l;
            i += L;

            ////
            I = i;

            S = psfi;
            S = psfc;

            I += i;
            I += c;
            I += C;

            //
            c = C;

            c = psfi;

            c += l;
            c += L;

            //
            C = c;
            C = psfi;

        }

        //equality
        System.out.println("operator");
        System.out.println(i == d); //true
        System.out.println(i == s); //true
        System.out.println(i == D); //true
        System.out.println(i == S); //true
        System.out.println(i == c); //true
//        System.out.println(D == S); //false + not compile
        System.out.println("equals");
        System.out.println(I.equals(i)); //true
        System.out.println(I.equals(new Integer(2))); //true
        System.out.println(I.equals(s)); //false
        System.out.println(I.equals(D)); //false
        System.out.println(I.equals(d)); //false

        System.out.println("operations");

        double v = S + D;
        double v2 = s + D;
        Double v3 = s + D;

        //constructor parameters
        Short SS = new Short((short) 9); //only with casting using constructor
        Character CC = new Character((char) 0); //only with casting using constructor
    }


    public static void constructorsMethod(){
        new Integer(1); // int primitive value
        new Integer("1"); //String
        Short s = new Short((short) 1); //casting needed because the parameter is not int
        new Integer(s); // casted automatic
        new Boolean(true);
        new Boolean("true");
        Short.valueOf((short) 1); //string, returns primitive
        Boolean.valueOf(true); //string, returns primitive
        Boolean.valueOf("true"); //string, returns primitive
        Boolean.parseBoolean("true"); //string, returns primitive


    }


    private static void bytesOperations() {
        System.out.println(Integer.toBinaryString(Integer.MIN_VALUE)); //(1)00..0 () = sign flag
        System.out.println(Integer.toBinaryString(Integer.MIN_VALUE + 1)); //(1)00..1 // negatives are starting from minim
        System.out.println(Integer.toBinaryString(Integer.MAX_VALUE)); //()11..1
        System.out.println(Integer.toBinaryString(Integer.MAX_VALUE - 1)); //()11..1
        System.out.println(Integer.toBinaryString(Integer.MIN_VALUE - 1)); //Integer.MAX_VALUE
        System.out.println(Integer.toBinaryString(Integer.MAX_VALUE + 1)); //Integer.MIN_VALUE

        System.out.println(Integer.toBinaryString(1)); //00..1
        System.out.println(Integer.toBinaryString(-2)); // (1)11..0
    }
}

package basicVarTypes;

public class PrimitiveCharCastingOrder {

    static public void met(char i) {
    }

    static public void met(int i) {
    } // and other larger types types (bigger than int)

    static public void met(Character i) { //
    }

    static public void met(Object i) {
    }

    static public void met(char... i) {
    }// if Character... and char... => multiple implementations

    static public void met(Character... i) {
    }// if Character... and char... => multiple implementations

    static public void met(int... i) {
    } //all larger primitives

    static public void met(Object... i) {
    }

    public static void main(String[] args) {
        char b = 120; //8

        met(b);

    }
}

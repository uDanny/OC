package operators;

import java.util.ArrayList;
import java.util.List;

public class VarArgsPractice {
    public static void walk1(int... nums) {
    }

    //    public static void walk1(int start, int... nums) { } // compiles with no ambiguous calls like:
//       walk1(1);
//       walk1(1, 1);
//       walk1(1, 1, 1);
    public static void walk2(int start, int... nums) {
    }
//    public void walk3(int... nums, int start) { } // DOES NOT COMPILE
//    public void walk4(int... start, int... nums) { } // DOES NOT COMPILE

    public static void main(String[] args) {

        walk1(); // nums is not null, but empty array
        walk1(1);
        walk1(1, 1);
        walk1(1, 1, 1);
        walk1(new int[]{1, 2}); //needs using of new int[]


        walk2(1);
        walk2(1, 1);
        walk2(1, 1, 2);
        walk2(1, new int[]{1, 2});
        walk2(1, null); //null array
//        walk2(1, 1,2,null); // not compiles a NullPointerException

    }
}

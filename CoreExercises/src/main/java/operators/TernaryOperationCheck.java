package operators;

public class TernaryOperationCheck {
    public static void main(String[] args) {
//         true ? 2 : "s"; //should return a value, can't be null
        Object o = true ? 2 : "s";
//        Object oo = true ? voidMethod() : "s"; //no compile - need return and same types
        Object o2 = true ? false ? "00" : 77 : "s";
    }
}

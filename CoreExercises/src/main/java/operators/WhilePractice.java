package operators;

public class WhilePractice {
    public static void main(String[] args) {
        int i = 0;
        do {
            i++;
            System.out.println("firstLoop");
        } while (i < 3);
        System.out.println("do-while " + i); // at least once, = 3

        //


        i = 0;
        while (i < 3) {
            i++;
            System.out.println("secondLoop");
        }
        System.out.println("while-do " + i); //  = 3

    }
}

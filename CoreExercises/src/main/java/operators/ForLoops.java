package operators;

import java.util.ArrayList;
import java.util.List;

class ForStatements {
    public static void main(String[] args) {
        int out = 0;
        for (; ; out++) { // only second time executes ++
            break;
        }
        System.out.println(out); // 0

        for (int i = out = 0, o = 2; out < 100; ) {
            break;
        }

        for (String s = "a", l = "l"; out < 100 && true; s.concat("0"), s.concat("1")) {
            break;
        }
        String s = "a";
        String s1 = "a";
//        for (String oo="a",s1="a1"; out<100; ) { if only init, or assignment
        for (s = "a", s1 = "a1"; out < 100; ) {
            break;
        }


    }
}

public class ForLoops {
    public static void main(String[] args) {
        int x = 0;
        int y = 0;
//        for(long y = 0, x = 4; x < 5 && y < 10; x++, y++) { // DOES NOT COMPILE
        for (y = 0, x = 4; x < 5 && y < 10; x++, y++) {
            System.out.print(x + " ");
        }

        //

        for (String xx = "a", ceva = "aaa"; x < 5 && y < 10; x++, y++) {
            System.out.print(x + " ");
        }
        String xx = "a", ceva = "aaa";
        for (xx = "a", ceva = "aaa"; x < 5 && y < 10; x++, y++) {
            System.out.print(x + " ");
        }


//        for(long y = 0, int x = 4; x < 5 && y<10; x++, y++) { // DOES NOT COMPILE
//            System.out.print(x + " ");
//        }

        //FOREACH - accept casting on foreach(+ to primitives
        List<Integer> integers = new ArrayList<>();
        integers.add(0);
        integers.add(1);
        for (double integer : integers) {

        }

    }
}

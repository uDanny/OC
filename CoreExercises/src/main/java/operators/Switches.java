package operators;

public class Switches {

    public static final String S_1 = "1";
    public static final Byte B = 1;
    public static final Integer I = 2;
    public static final byte b = 3;
    public static final int i = 4;
    public static final char c = 'c';
    public static final Character C = 'C';

    public static void main(String[] args) {
        int localI = 7;
        switch (localI) { //int, short, byte(and their wrappers), string, char, enums supported
            case 0:
                System.out.println("1");
                break;
            case b: // not B
                System.out.println("2");
                break;
            case i: // not I
                System.out.println("2");
                break;
            case '7': // false, not working with Character
                System.out.println("2");
                break;
            case c: // false, not working with C
                System.out.println("2");
                break;
            default:
                System.out.println("default");
        }

        Integer integerWrapper = 1;
        switch (integerWrapper) {
            case 0: // cant use null
                System.out.println("1");
                break;
            case i: // not I, not b, not B
                System.out.println("2");
                break;
            default:
                System.out.println("default");
        }


        String s = "1";
        switch (s) {
            case "aa": // cant use null
                System.out.println("unu");
                break;
            case S_1: // only constant expresion required(static + final)
                System.out.println("unu");
                break;
        }

        char localC = '2';
        switch (localC) {
                case '2': // cant use null
                System.out.println("unu");
                break;

                case c: // not C
                System.out.println("casting");
                break;

                case b: //also i, not B or I
                System.out.println("casting");
                break;
                //contiunue; // not compile

//            case S_1.charAt(0): // only constant expresion required
//                System.out.println("unu");
//                break;
        }

        Character charWrapper = '2';
        switch (charWrapper) {
                case '2': // cant use null
                System.out.println("unu");
                break;

                case c: // not C
                System.out.println("casting");
                break;

                case b: //also i, not B or I
                System.out.println("casting");
                break;
        }

        byte b = 0;
        switch (b){
//            case '':
        }

        int i = 0;
        //we can have empty switch, but no code inside
        switch (i) {
//            System.out.println("inside");
        }
    }

    /**
     * declaration trick
     */
    public static void switchCase(){
        String[] args = {"a", "1"};
        switch(Integer.parseInt(args[1]))  //1
        {
            case 0 :
                boolean b = false;
                int ii = 20;
                break;

            case 1 :
                //b = !b; //not valid
                b = true; //valid, same scope with case 0
                ii = 0;
                int i = 0;
                break;
        }

    }


}

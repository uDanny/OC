package operators;

public class LabelsPractice {
    public static void main(String[] args) {
        System.out.println("1 break _________");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println("inner for");
                break;
            }
            System.out.println("outer for");
        }

        System.out.println("2 break label _________");

        for (int i = 0; i < 3; i++) {
            L1:
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    System.out.println("X2 inner for");
                    break L1;
                }
                System.out.println("inner for");

            }
            System.out.println("outer for");
        }

        System.out.println("3 continue _________");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println("inner for");
                continue;
            }
            System.out.println("outer for");
        }

        System.out.println("4 continue label _________");
        L1:
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println("inner for");
                continue L1;
            }
            System.out.println("outer for");
        }

        System.out.println("5 while + for _________");

        LL:
        {
        } //for what?
        LL2:
        if (true) {
            break LL2;
        }


//infinite loops
        L1:
        do {
            L2:
            for (int i = 0; i < 7; i++) {
                L3:
                if (false) {
                    break L1;
                } else {
                    L4:
                    switch ("a") {
                        case "a":
                            System.out.println("inside switch");
                            break L1;
                        default:
                            continue L1;
                    }
//                    continue L2;
                }
            }
        } while (true);

        //try with isContinue = true, or false
        System.out.println("6 while + for _________");
        for (int i = 0; i < 2; i++) {
            System.out.println("i starts");
            LABEL:
            for (int j = 0; j < 2; j++) {
                System.out.println(" j starts");
                for (int k = 0; k < 2; k++) {
                    System.out.println("  k starts");
                    boolean isContinue = false;
                    if (isContinue) {
                        continue LABEL;
                    } else if (!isContinue) {
                        break LABEL;
                    }
                    System.out.println("  k ends");
                }
                System.out.println(" j ends");
            }
            System.out.println("i ends");
        }
        System.out.println("end");
    }

    private void visibility() {
        int c = 0;
        JACK:
        while (c < 8) {
            JILL:
            System.out.println(c);
//        if (c > 3) break JILL; //no allowed
//        else c++;
        }
    }


}

class Bonus {
    public static void main(String[] args) {
        //                FLAG: not allowed before declaration
        int oo = 0;
        L0:
        {
            L000:
            System.out.println("---");
            L000V2:
            if (true) {
                L00:
                if (true) {
                    break L0; // jumps to iii
                } else if (false) {
                    break L00; // jumps to i
                } else {
//                    break L000; // not allowed
                    break L000V2; // jumps to ii

                }
                int i = 0;
            }
            int ii = 0;

        }
        int iii = 0;

    }
}

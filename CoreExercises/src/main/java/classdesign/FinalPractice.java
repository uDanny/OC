package classdesign;

public class FinalPractice {

    public static void main(String[] args) {

    }
}

final class A {

}

//class B extends A {} cant inherit
class AA {

    final int i; //should be instantiated, at list in constructor, + just once per
    static final int ii = 0; //should be instantiated, before construct, explicit or in static block, + just once

//    {i = 2;} // if no constructor, might be initialized in block

    AA(int i) {this.i = 5;}

    AA() {this.i = 6;} // permitted

    //
    final void methdod(){}
    final static void methdod2(){}

    private final void privateMethdod(){}
    private final static void privateMethdod2(){}

    public static void main(String[] args) {
//        i = 5; //prohibited
    }
}

class BB extends AA{
    final int i = 2; //can hide final instance var
    static final int ii = 0; //hide upper even if it is final, it is reused

//    final void methdod(){} //prohibited because final
//    final void methdod2(){} //prohibited because final

    //it is redefine method, because upper methods are private
    private final void privateMethdod(){}
    private final static void privateMethdod2(){}

}

package classdesign;

//cant be final
interface InterfacePractice {
//        {} //no init blocks
//        static {} //no static block

//cant be private / protected / with no value
public static final int psfVar = 0; // default is public static final

//cant be final / private / protected / default / abstract in combination with static
public static void c() { //automatic is public
}

//cant be private, protected, final
abstract public void a(); // public by default

//cant be private / protected / static / final / abstract for defaults
public default void defMethod() { //automatic is public
}
}

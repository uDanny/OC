package classdesign;

class Blocks {
    static int ii = 0;
    int i = 0;
//        static {
//            throw new NullPointerException(); //nocompile
//        }
    static {System.out.println("bloc initial static");}

    {
        System.out.println("bloc initial");
        i = 2;
        ii = 3;
    }

    public Blocks() {
        System.out.println("constructor");
    }


    public static void main(String[] args) {
        System.out.println("voidMethod");
        System.out.println(new Blocks().i);
        System.out.println(ii);


    }

}

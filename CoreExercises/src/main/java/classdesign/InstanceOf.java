package classdesign;

public class InstanceOf {

    interface A{}
    class AA implements A{}
    class B{}
    class BB extends B{}

    class C extends B implements A{}


    public static void main(String[] args) {
        InstanceOf rr = new InstanceOf();
        rr.methodName();
    }

    private void methodName() {
        AA a1 = new AA();
        A a2 = new AA();
        A a3 = null;
        System.out.println(a1 instanceof AA); // true
        System.out.println(a1 instanceof A); // true
        System.out.println(a2 instanceof AA); // true
        System.out.println(a2 instanceof A); // true
        System.out.println(a3 instanceof A); // false
//        System.out.println(a3 instanceof Integer); // no compile, check interface
        System.out.println(a3 instanceof Iterable); // false, on interfaces, no checks on compile, interfaces checked on runtime

        //

        BB b1 = new BB();
        B b2 = new BB();
        System.out.println(b1 instanceof BB); // true
        System.out.println(b1 instanceof B); // true
        System.out.println(b2 instanceof BB); // true
        System.out.println(b2 instanceof B); // true

        //
        C c1 = new C();
        B c2 = new C();
        A c3 = new C();

        System.out.println(c1 instanceof C); // true
        System.out.println(c1 instanceof B); // true
        System.out.println(c1 instanceof A); // true

        System.out.println(c2 instanceof C); // true
        System.out.println(c2 instanceof B); // true
        System.out.println(c2 instanceof A); // true

        System.out.println(c3 instanceof C); // true
        System.out.println(c3 instanceof B); // true
        System.out.println(c3 instanceof A); // true

        int i = 0;
        //System.out.println(c3 instanceof Integer); // false + not compile




    }
}

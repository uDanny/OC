package classdesign;

public class ModifiersBlocks {
    final int i = 3;
    final int ii; // final require init

    {
//        i = 2; //only once can declare it
        ii = 2;
//        ii = 3; //only once can declare it
        System.out.println("init block");
    }

    final int iii;

    public ModifiersBlocks(int iii) {
        this.iii = iii;
//        this.ii = ii; //can declare only once
        System.out.println("constructor");
    }

    ///
    static final int f = 2;
    static final int ff;

    static {
        ff = 2;
        System.out.println("static block");
    }

    public static void main(String[] args) {
        System.out.println("Main method");
        ModifiersBlocks m = new ModifiersBlocks(1);
        System.out.println("--");
        ModifiersBlocks m2 = new ModifiersBlocks(1);
//order:
//        static block
//        Main method
//        init block
//        constructor
//        --
//        init block
//        constructor

    }
}

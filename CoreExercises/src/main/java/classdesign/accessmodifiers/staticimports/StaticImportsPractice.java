package classdesign.accessmodifiers.staticimports;
/**
//import classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource;
//import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.method;
//import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.i;

//import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.i;
//import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.method;
*/
import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.i; //needs to be explicit because anotherPackage2.Static.* - also includes i
import static classdesign.accessmodifiers.staticimports.anotherPackage2.StaticResource.*;
/**
//import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.i;
//import static classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource.method;
*/
import classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource;

public class StaticImportsPractice {
//        'this' reference is not available within a static method.

    public static void main(String[] args) {
        //       StaticResource.method(); //allowed, needs an usual import class of another package

        method(); //from anotherPackage2
        i = 3; //from anotherPackage

        //needs apart import, even if static is been imported :import classdesign.accessmodifiers.staticimports.anotherPackage.StaticResource;
        StaticResource s = new StaticResource();

        //explicit import
        classdesign.accessmodifiers.staticimports.anotherPackage2.StaticResource.i = 3;
    }
}

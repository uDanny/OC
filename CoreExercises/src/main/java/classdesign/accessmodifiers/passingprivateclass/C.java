package classdesign.accessmodifiers.passingprivateclass;

import classdesign.accessmodifiers.passingprivateclass.a.A;

public class C {
    public static void main(String[] args) {
        new C().main();
    }

    private void main() {
        A a = new A();
        a.getB();
//        B b = a.getB(); //access error
//        a.getB().doB(); //access error
    }
}

package classdesign.accessmodifiers.classModifiers.zone2;
import classdesign.accessmodifiers.classModifiers.zone1.A;

public class X {
    public static void main(String[] args) {
        A a = new A();
//        B b = new B(); not compile because is not public and in another package
//        C c = new C(); not compile because is not public and in another package
    }
}

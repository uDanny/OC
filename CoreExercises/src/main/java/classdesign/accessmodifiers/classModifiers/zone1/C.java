package classdesign.accessmodifiers.classModifiers.zone1;

class B {
    public static void main(String[] args) {
        A a = new A();
    }

}
class C{}

//public class D{}  - compile error(maximum one class per file to be public and it should be the same name as the file)


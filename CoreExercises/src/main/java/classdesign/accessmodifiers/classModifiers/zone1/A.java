package classdesign.accessmodifiers.classModifiers.zone1;

import classdesign.accessmodifiers.classModifiers.zone2.*;
//import classdesign.accessmodifiers.ex1.zone3.x; //if need to use same name, one of imports must be explicit

public class A {
    public static void main(String[] args) {
        //can access without import and no need to be public because same package
        new C();
        new B();
        //needs import
        new X();
    }
}

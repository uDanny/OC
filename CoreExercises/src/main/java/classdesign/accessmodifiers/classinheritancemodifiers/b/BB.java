package classdesign.accessmodifiers.classinheritancemodifiers.b;

public class BB {
    public static void main(String[] args) {
        new AA().nonStaticMethod();

//        new AA().i;
//        new AA().methodName();
//        new A().i;
//        new A().methodName();
    }
}

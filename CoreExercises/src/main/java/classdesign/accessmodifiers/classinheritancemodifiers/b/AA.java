package classdesign.accessmodifiers.classinheritancemodifiers.b;

import classdesign.accessmodifiers.classinheritancemodifiers.A;

public class AA extends A {
    public static void main(String[] args) {
        new AA().nonStaticMethod();
    }

    public void nonStaticMethod() {

        int i1 = this.i; // protected
//        i1 = this.i2; //package-private
        methodName(); // protected
//        methodName2(); //package-private

        //even on protected fields/ method, can't get access through instances of parent class
        A a = new A();
//        int i = a.i;
//        int ii = a.i2;
//        a.methodName();
//        a.methodName2();

        A a2 = new AA();
        int i2 = ((AA) a2).i; //only with casting
//        int ii2 = a2.i2;
        ((AA) a2).methodName(); //only with casting
//        a2.methodName2();

        AA a3 = new AA();
        int i3 = a3.i; // can access fields from this class by instance
//        int ii3 = a3.i2;
        a3.methodName(); // can access methods from this class by instance
//        a3.methodName2();
    }
}

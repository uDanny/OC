package classdesign.accessmodifiers.classinheritancemodifiers;

public class AA extends A {
    public static void main(String[] args) {
        new AA().nonStaticMethod();
    }

    private void nonStaticMethod() {
        int i1 = this.i;
        i1 = this.i2;
        methodName();
        methodName2();

        //can access protected and private-package from instance of upper obj
        A a = new A();
        int i = a.i;
        int ii = a.i2;
        a.methodName();
        a.methodName2();

        //can access protected and private-package from child instance inside, wrapped into child obj
        A a2 = new AA();
        int i2 = a2.i;
        int ii2 = a2.i2;
        a2.methodName();
        a2.methodName2();

        //can access protected and private-package from child instance inside
        AA a3 = new AA();
        int i3 = a3.i;
        int ii3 = a3.i2;
        a3.methodName();
        a3.methodName2();
    }

//    final static void fs(){}

}

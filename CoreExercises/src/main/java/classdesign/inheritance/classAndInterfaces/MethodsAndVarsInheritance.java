package classdesign.inheritance.classAndInterfaces;

public class MethodsAndVarsInheritance {

    public static void main(String[] args) {
        Bear.sneeze(); //Bear is sneezing
        Panda.sneeze(); //Panda is sneezing

        Bear bear = new Bear();
        bear.sneeze(); //Bear is sneezing
        Bear.sneeze(); //equivalent as upper

        Bear bearPanda = new Panda();
        bearPanda.sneeze(); //Bear is sneezing, because it's reference to hidden method
        Bear.sneeze(); //equivalent as upper

        Panda panda = new Panda();
        panda.sneeze(); //Panda is sneezing
        Panda.sneeze(); //equivalent as upper

        //vasrs

        System.out.println(bear.i);
        System.out.println(bearPanda.i); // same as bear.i, hidden var
        System.out.println(panda.i);

        System.out.println(bear.j);
        System.out.println(bearPanda.j); // same as bear.j, hidden var
        System.out.println(panda.j);

    }
}

class UpperClass {}

class DownClass extends UpperClass {}

class Bear {

    final int i = 0; //could be also final, it's about value not hiding
    final static int j = 0;

    public static void sneeze() {
        System.out.println("Bear is sneezing");
    }
    private final void weirdMethod(){} //final is redundant
    final private void weirdMethod2(){} //final is redundant

    public void hibernate() {
        System.out.println("Bear is hibernating");
    }

    private void privateMethod() {
        System.out.println("privateMethod bear");
    }

    public final void finalMethod() {
        System.out.println("finalMethod bear");
    }

    public static final void finalStaticMethod() {
        System.out.println("finalStaticMethod bear");
    }

    private static final void privateFinalStaticMethod() {
        System.out.println("privateFinalStaticMethod bear");
    }

    UpperClass covariantMethod() {return null;}
    long covariantPrimitivesMethod() {return 0;}
    Long covariantPrimitiveWrappersMethod() {return null;}

}

class Panda extends Bear {

    int i = 1;
    final static int j = 1;

    public static void sneeze() {
        System.out.println("Panda is sneezing");
    }
    //    public void sneeze() { // superclass method is static, no compile
    //        System.out.println("Panda bear sneezes quietly");
    //    }
    private final void weirdMethod(){} //final is redundant
    //    public static void hibernate() { // superclass method is not static, no compile
    //        System.out.println("Panda bear is going to sleep");
    //    }
    private void privateMethod() { //is just redeclaring
        System.out.println("privateMethod panda");
    }

    //    public final void finalMethod() { //can't override final, compile error
    //        System.out.println("finalMethod panda");
    //    }
    //    public static final void finalStaticMethod() { //can't override final, compile error
    //        System.out.println("finalStaticMethod panda");
    //    }
    private static final void privateFinalStaticMethod() { //is just redeclaring
        System.out.println("privateFinalStaticMethod panda");
    }

    @Override
    final DownClass covariantMethod() { // could be final also
        UpperClass upperClass = super.covariantMethod();
        return null;
    }

    //doesnt works like autoCast or covariants
//    @Override
//    int covariantPrimitivesMethod() {
//        return super.covariantPrimitivesMethod();
//    }
//    @Override
//    Integer covariantPrimitiveWrappersMethod() {
//        return super.covariantPrimitiveWrappersMethod();
//    }
}

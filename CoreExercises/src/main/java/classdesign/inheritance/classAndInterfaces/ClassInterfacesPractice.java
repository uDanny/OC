package classdesign.inheritance.classAndInterfaces;

public class ClassInterfacesPractice {
}


interface IParent {
    //    static {}
    //{}
    default void someMethod() {
    }
}

interface IChild extends IParent {
    void someMethod();
}

interface IGrandChild extends IParent, IChild {

}

class Implementor implements IGrandChild {

    @Override //must be implemented because nonabstract method
    public void someMethod() {

    }
}


interface CParent {
    default void someMethod() {
    }
}

abstract class CChild implements CParent {
    public abstract void someMethod();
}


class CImplementor extends CChild implements CParent {

    @Override //must be implemented
    public void someMethod() {

    }
}

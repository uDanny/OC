package classdesign.inheritance.classAndInterfaces;

public class ClassInterfaceInsanceMethods {
}

class ConcreteClass {
    public void method() {
        System.out.printf(this.getClass().toString());
    }
    int ai =0;
}

class ConcreteClass2 {
    public void method() {
        System.out.printf(this.getClass().toString());
    }
}

abstract class AbstractClass {
    public abstract void method();
}

abstract class AbstractClass2 {
    public abstract void method();
}

interface AbstractInterface {
    int ai =0;
    void method();
}

interface AbstractInterface2 {
    void method();
}

interface ConcreteInterface {
    int ai =0;

    default void method() {
        System.out.printf(this.getClass().toString());
    }
}

interface ConcreteInterface2 {
    default void method() {
        System.out.printf(this.getClass().toString());
    }
}

/**
 * usage
 */
//interfaces
interface A extends AbstractInterface, AbstractInterface2 {
}

interface A2 extends AbstractInterface, ConcreteInterface {

    @Override
    default void method() { //not autoresolved from default
//        AbstractInterface.super.method();
        boolean a = AbstractInterface.ai<0;
        boolean b = ConcreteInterface.ai<0;
        ConcreteInterface.super.method();
    }
}

interface A3 extends ConcreteInterface, ConcreteInterface2 {
    @Override
    default void method() {
        ConcreteInterface.super.method();
    }
}


//-----
//abstract - interfaces
abstract class B implements AbstractInterface, AbstractInterface2 {

}

abstract class B2 implements AbstractInterface, ConcreteInterface {

    @Override
    public void method() {
        ConcreteInterface.super.method();
    }
}

abstract class B3 implements ConcreteInterface, ConcreteInterface2 {

    @Override
    public void method() {

    }
}

//------
//class - combinations
class C extends ConcreteClass implements AbstractInterface {
    //autoresolved, used from ConcreteClass
}

class C2 extends ConcreteClass implements ConcreteInterface {
    //autoresolved, used from ConcreteClass

}
class C22 extends ConcreteClass implements ConcreteInterface {
    //autoresolved, used from ConcreteClass

    @Override
    public void method() {

        System.out.println("methods override");
        System.out.println(ConcreteInterface.ai);
        System.out.println(super.ai);
        super.method();
        ConcreteInterface.super.method();

//        int ai = new C22().ai;
    }
    public void method222() {
        ConcreteInterface.super.method();
    }


    }

class C222 extends AbstractClass implements ConcreteInterface {
    //not autoresolved from interface
    @Override
    public void method() {

    }
}

//-----
abstract class C3 extends AbstractClass implements AbstractInterface {
    //autoresolved, expected to be implemented in concrete
}

abstract class C4 extends AbstractClass implements ConcreteInterface {
    //autoresolved, expected to be implemented in concrete
}

//---
class C5 implements ConcreteInterface {
    //autoresolved, uses interface's default
}


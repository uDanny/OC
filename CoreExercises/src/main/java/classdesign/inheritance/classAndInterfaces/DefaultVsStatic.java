package classdesign.inheritance.classAndInterfaces;

//cant be final
class DefaultVsStatic {
    static void main(String[] args) {
        ParentClass.parentClassStatic();
        new ParentClass().parentClassStatic();

        ChildClass.parentClassStatic();
        new ChildClass().parentClassStatic();

        ParentInterface.staticInterfaceMethod();
        ParentInterface.staticInterfaceHidden(); //called from interface
//         ParentInterface.defaultMethod();

//        ChildClass.staticInterfaceMethod();
//        new ChildClass().staticInterfaceMethod();

        ChildClass.staticInterfaceHidden();
        new ChildClass().staticInterfaceHidden();

//         ChildClass.defaultMethod();
        new ChildClass().defaultMethod();

        //statics available from parent classes instances, not from interfaces impl instances
        //defaults are available as class methods from instance


    }
}


class ParentClass {
    static void parentClassStatic() {
    }
//    //to-delete
//    static void staticInterfaceMethod() {
//    }
}

class ChildClass extends ParentClass implements ParentInterface {
    static void staticInterfaceHidden() {
    } //upper hidden

    @Override
    public void abstractMethod() {
//        //to-delete
//        staticInterfaceMethod();
    }


}

interface ParentInterface {
    static void staticInterfaceMethod() {
    }

    static void staticInterfaceHidden() {
    }

    default void defaultMethod() {
        abstractMethod();
    }

    void abstractMethod();

}

//An interface can redeclare a default method and also make it abstract. (as well as abstract methods)
interface ChildInterface extends ParentInterface {

    static void staticInterfaceHidden() {
    }

    ; //upper hidden

    @Override
    default void defaultMethod() {
    }

    ;
}

//oneMoreExample
interface Pi {
    default void a() {
    }
}

interface Pi2 {
    static void a() {
    }
}

interface Ci extends Pi, Pi2 {
}

class Pc {
    static void a() {
    }
}

class Cc implements Ci {
    public static void main(String[] args) {
        Cc c = new Cc();
        c.a();
//        ((Pi2)c).a();
        Pi2.a();
    }
}

//class Cc2 extends Pc implements Ci { //ide bug, it is not allowed, conflict static methods
//    public static void main(String[] args) {
//        Cc2 c = new Cc2();
//        c.a(); //from static class, default is inaccessible from instance
//        ((Pc) c).a(); //static from class
//        ((Ci) c).a(); //default
//        ((Pi) c).a(); //default
////        ((Pi2) c).a(); //no compile because is static from interfaces, not accessible through instance
//
//        Pi2.a();
////        Ci.a();
////        Pi2.super.a();
////        Ci.super.a();
//    }
//}

class SuperClass{
    int i;
    static int si;
}
interface SuperInterface{
    int i =0;
    int si =0;

}
class ImplClass extends SuperClass implements SuperInterface{

    void mainInstance(){
        System.out.println(super.i); //needs super to access from class
        System.out.println(super.si); //needs super to access from class
    }
}



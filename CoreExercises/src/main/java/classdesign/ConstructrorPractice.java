package classdesign;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ConstructrorPractice {

    public static void main(String[] args) {
        new ConstructrorPractice().method();
    }

    void method() {
        B b = new B();
    }

    class A {

        int i;

        public A() {
            System.out.println("empty constructor invoked by default");
        }

        public A(int i) {
            this.i = i;
        }
    }

    class B extends A {
    }

    class C extends A {

        int j;
        int k;

        public C(int i) {
            // int a = 0; //upper constructor should be on the first Line
            super(i); // call upper constructor, but it in this line absence, it calls the default constructor
            callMe(this); // can do everything after calling super
        }

        public C(int i, int j) {
            this(i); // calling same C constructor
            this.j = j;
        }

        private void callMe(C c) { //handle this
//            this(2); //can use only in constructors
        }

        private void callMe(A c) { //handle this
        }

        private C getObject() { //handle this
            return this;
        }
    }

}

//exceptions
class AA1 {
    public AA1() throws IOException {
        throw new IOException();
    }

    void m() throws IOException {
    }
}

class BB1 extends AA1 {
    //IOException is valid here, but FileNotFoundException is invalid
    public BB1() throws IOException { // or Exception
    }
//    public BB1(int i)  { // or Exception
//        try {
//            super(); //must be first statement
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    // FileNotFoundException is valid here, but Exception is invalid
    void m() throws FileNotFoundException {
    }
}

//instantiation order example
class Parent {
    public Parent() {


        System.out.println(i);
        System.out.println(j);

        System.out.println(getI());
        System.out.println(getJ());
    }

    int i = 1;

    int getI() {
        return i;
    }

    static int j = 1;

    static int getJ() {
        return j;
    }


}

class Child extends Parent {
    int i = 2;

    @Override
    int getI() {
        return i;
    }

    static int j = 2;

    static int getJ() {
        return j;
    }

    public static void main(String[] args) {
        new Child(); // 1 1 0 1, 0 -> because on Parent constructor, all child's params are initialized with default value
    }

}

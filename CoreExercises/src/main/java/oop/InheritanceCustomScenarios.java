package oop;

import java.util.ArrayList;
import java.util.Arrays;

public class InheritanceCustomScenarios {

    //methods invoking
    public static void main(String[] args) {
        a(new ChildClass());

        ChildClass[] children = {new ChildClass()};
        for (ParentClass child : children) {

        }
        for (ParentClass child : Arrays.asList(children)) {

        }

    }

    static ParentClass a(ParentClass a) {
        return new ChildClass();
    }

}

/**
 * order of instantiated values in oop
 */
class A {
    A() {
        print();
    }

    void print() {
        System.out.println("A");
    }
}

class B extends A {
    int i = 4;

    public static void main(String[] args) {
        A a = new B();
        a.print();
    }

    void print() {
        System.out.println(i);
    } //will be printed: 0, 4, because on super constructor call, all child variables have default values
}


class ParentClass {

}

class ChildClass extends ParentClass {
    void childMethod(){}
    void nonFinalMethod(){}
}
abstract class NewChildClass extends ChildClass {
    @Override
     abstract void childMethod();

    @Override
     final void nonFinalMethod(){};
}

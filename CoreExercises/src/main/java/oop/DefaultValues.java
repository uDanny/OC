package oop;

public class DefaultValues {
    static String s; // null
    static boolean b; // false
    static float f; // 0.0f
    static char c; //0, or '\0', or '\u0000'

    public static void main(String[] args) {
        /** char default value**/
        char cc = '0';
        char ccc = 0;
        char cccc = '\0';
        char ccccc = '\u0000';
        System.out.println(c == cc); //false
        System.out.println(c == ccc); //true
        System.out.println(c == cccc); //true
        System.out.println(c == ccccc); //true
    }
}

package lambdas;

public class LambdasEmpty {
    public static void main(String[] args) {
        run(() -> voidMethod()); //will invoke run(Runner ) method.
        run(() -> intMethod()); //will invoke run(Runner2 ) method.  
    }

    public static void run(Runner x) {
        System.out.println("In runner");
        x.run();
    }

    public static void run(Runner2 x) {
        System.out.println("In runner2");
        x.run();
    }

    public static void voidMethod() {
        System.out.println("voidMethod");
    }

    public static int intMethod() {
        System.out.println("intMethod");
        return 0;
    }
}


interface Runner {

    public void run();
}

interface Runner2 {

    public int run();
}

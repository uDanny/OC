package lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LambdaPractice {

    public static void main(String[] args) {
        new LambdaPractice().mainLogic();
    }

    private void mainLogic() {

        List<Animal> animals = new ArrayList<>(); // list of animals
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

//        print(animals, new CheckIfHopper()); // pass class that does check
        print(animals, a -> a.canHop()); //doesnt matter hierarchy of Checker obj
        print(animals, a -> !a.canHop());
        print(animals, Animal::canHop);

//        print(new ArrayList<String>(), (String a) -> a.isEmpty());

//        print((null) -> true); // 0 parameters
//        print(a -> a.startsWith("test")); // 1 parameter
//        print((String a) -> a.startsWith("test")); // 1 parameter
//        print((a, b) -> a.startsWith("test")); // 2 parameters
//        print((String a, String b) -> a.startsWith("test"));

//        print(a, b -> a.startsWith("test")); // DOES NOT COMPILE
//        print(a -> { a.startsWith("test"); }); // DOES NOT COMPILE
//        print(a -> { return a.startsWith("test") }); // DOES NOT COMPILE
    }

    public interface ParrentChecks {
        boolean test(Animal a);
    }

    public interface Check extends ParrentChecks {
    }

    public interface Check2 extends ParrentChecks {
    }

    public interface CustomCheck {
//        int test(boolean a); //cant use another method then boolean
        boolean test(boolean a);

    }


    private void print(List<Animal> animals, ParrentChecks checker) {
        for (Animal animal : animals) {
            if (checker.test(animal)) // the general check
                System.out.print(animal + " ");
        }
        System.out.println();
    }

    private void print(ArrayList<String> strings, CustomCheck checker) {
        for (String string : strings) {
            if (checker.test(true))
                System.out.print(string + " ");
        }
        System.out.println();
    }

    public class Animal {
        private String species;
        private boolean canHop;
        private boolean canSwim;

        public Animal(String speciesName, boolean hopper, boolean swimmer) {
            species = speciesName;
            canHop = hopper;
            canSwim = swimmer;
        }

        public boolean canHop() {
            return canHop;
        }

        public boolean canSwim() {
            return canSwim;
        }

        public String toString() {
            return species;
        }
    }
}

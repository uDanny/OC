package lambdas;

import java.util.ArrayList;
import java.util.List;

public class LambdaPracticeNonBoolean {

    public static void main(String[] args) {
        new LambdaPracticeNonBoolean().mainLogic();
    }

    private void mainLogic() {

        List<Animal> animals = new ArrayList<>(); // list of animals
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        print(animals, a -> a - 3);
    }


    public interface CustomCheck {
        int test(int a);

    }

    private void print(List<Animal> animals, CustomCheck customCheck) {
        for (Animal animal : animals) {
            System.out.print(customCheck.test(animal.getLifes()));
        }
    }


    public class Animal {
        private String species;
        private boolean canHop;
        private boolean canSwim;
        private int lifes = 5;

        public Animal(String speciesName, boolean hopper, boolean swimmer) {
            species = speciesName;
            canHop = hopper;
            canSwim = swimmer;
        }

        public boolean canHop() {
            return canHop;
        }

        public boolean canSwim() {
            return canSwim;
        }

        public int getLifes() {
            return lifes;
        }

        public String toString() {
            return species;
        }
    }
}
